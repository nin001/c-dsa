/*
 Take rows from user
 aBc
 dEf
 gHi
 */
#include<stdio.h>

void main(){
	int row;
	printf("Enter a number : ");
	scanf("%d",&row);
	char ch1 = 'A',ch2 = 'a';
	for(int i = 1 ; i<=row ; i++){
		for(int j = 1 ; j<=row ; j++){
			if(j%2==0){
				printf("%c\t",ch1);
				ch1++;ch2++;
			}else{
				printf("%c\t",ch2);
				ch1++;ch2++;
			}
		}
		printf("\n");
	}
}
