//WAP to find input is character or digit or alphabet
#include<stdio.h>

void main(){
	char input;
	printf("Enter input character : ");
	scanf("%c",&input);
	if(input>=48&&input<=57){
		printf("You entered a number\n");
	}else if(input>='A'&&input<='Z'){
		printf("You entered a Upper case alphabet\n");
	}else if(input>='a'&&input<='z'){
		printf("You entered a Lower case alphabet\n");
	}else{
		printf("You entered a speacial character\n");
	}
}

