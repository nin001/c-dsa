/*
 1 3 5 
 5 7 9 
 9 11 13
 */
#include<stdio.h>

void main(){
	int row;
	printf("Enter number of row : ");
	scanf("%d",&row);
	int num = 1;
	for(int i = 1 ; i<=row ; i++){
		for(int j = 1 ; j<=row ; j++){
			printf("%d\t",num);
			num = num + 2;
		}printf("\n");
		num = num - 2;
	}
}
