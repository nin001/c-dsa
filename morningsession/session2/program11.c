/*
1234
abcd
####
1234
abcd
####
*/
#include<stdio.h>

void main(){
	int row;
	printf("Enter row : ");
	scanf("%d",&row);
	
	int num = 1;
	char ch = 'a';
	
	for(int i = 1 ; i<=row ; i++){
		for(int j = 1 ; j<=4 ; j++){
			if(i%3==1){
				printf("%d",num++);
			}else if(i%3==0){
				printf("#");
				
			}else{
				printf("%c",ch++);
			}
		}printf("\n");
	}
}
