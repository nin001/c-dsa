/*
 * AbC
 * dEf
 * GhI
 */
#include<stdio.h>

void main(){
	int row;
	printf("Enter row : ");
	scanf("%d",&row);
	char ch = 'A';
	for(int i = 1 ; i<=row ; i++){
		for(int j = 1 ; j<=row ; j++){
			if(ch<='Z'&&ch>='A'){
				printf("%c\t",ch);
				ch = ch + 33;
			}else{	
				printf("%c\t",ch);
				ch = ch - 31;
			}
		}printf("\n");
	}
}
