//WAP to print odd numbers as it is and even numbers cube take range from user
#include<stdio.h>

void main(){
	int x,y;
	printf("Enter first number : ");
	scanf("%d",&x);
	printf("Enter second number : ");
	scanf("%d",&y);

	for(int i = x ; i<=y ; i++){
		if(i%2==0){
			printf("%d ",i*i*i);
		}else{
			printf("%d ",i);
		}
	}
	printf("\n");
}
