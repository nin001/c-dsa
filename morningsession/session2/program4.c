/*
 take rows from user 
 d d d d
 c c c c
 b b b b
 a a a a
 */
#include<stdio.h>

void main(){
	int row;
	printf("Enter row : ");
	scanf("%d",&row);
	char ch = 'a'+(row-1);
	for(int i = row ; i>=1 ; i--){
		for(int j = 1 ; j<=row ; j++){
			printf("%c\t",ch);
		}
		printf("\n");
		ch--;
	}
}
