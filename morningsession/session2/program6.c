/*
 take input from user rows;
 123
 234
 345
 */
#include<stdio.h>

void main(){
	int row;
	printf("Enter rows : ");
	scanf("%d",&row);
	int num = 1;
	for(int i = 1 ; i<=row ; i++){
		for(int j = 1 ; j<=row ; j++){
			printf("%d\t",num);
			num++;
		}
		printf("\n");
		num = num-2;
	}
}
