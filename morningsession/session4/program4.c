//take value from user 
//1 2 3 4
//5 6 7
//8 9
//10
//
#include<stdio.h>

void main(){
	int row; 
	printf("Enter rows : ");
	scanf("%d",&row);
	int num = 1;
	for(int i = 1 ; i<=row ; i++){
		for(int j = i ; j<=row ; j++){
			printf("%d\t",num++);
		}
		printf("\n");
	}
}
