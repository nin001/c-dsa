//take num of rows from user
//4 3 2 1
//C B A 
//2 1
//A
#include<stdio.h>

void main(){
	int row;
	printf("Enter no of rows : ");
	scanf("%d",&row);

	int num = row;
	char ch = 64+(row-1);
			   
	for(int i = 1 ; i<=row ; i++){
		for(int j = i ; j<=row ; j++){
			if(i%2 == 0){
				printf("%c ",ch);
				ch--;
			}else{
				printf("%d ",num);
				num--;
			}
		}
		printf("\n");
		if(i%2==0)
			ch = ch + 2;
		else
			num = num+2;
		
	}
}	
