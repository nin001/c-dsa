/*
 Take input from user and print factorial of the number*/
#include<stdio.h>

void main(){
	int input;
	printf("Enter number : ");
	scanf("%d",&input);
	int fact = 1;
	while(input!=1){
		fact = input*fact;
		input--;
	}
	printf("Factorial of %d is %d\n",input,fact);
}

