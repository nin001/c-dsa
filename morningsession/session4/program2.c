/*
Input from user : rows
1 2 3 4 
1 2 3
1 2
1
*/
#include<stdio.h>

void main(){
	int row;
	printf("Enter rows : ");
	scanf("%d",&row);
	
	for(int i = 1 ; i<=row ; i++){
		int num = 1;
		for(int j = i ; j<=row ; j++){
			printf("%d ",num++);
		}
		printf("\n");
	}
}
//Second logic for this code is that we can run the column loop reverse and give j value of row and j>=i 

