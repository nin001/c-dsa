//print factorial of range taken input from user
#include<stdio.h>

void main(){
	int x,y;
	printf("Enter first number : ");
	scanf("%d",&x);

	printf("Enter second number : ");
	scanf("%d",&y);

	if(y<x){
		int temp = y;
		y = x;
		x = temp;
	}
	for(int i = x ; i<=y ; i++){
		int fact = 1;
		for(int j = 1 ; j<=i ; j++){
			fact = fact*j;
		}
		printf("Factorial of %d is %d\n",i,fact);
	}
}


