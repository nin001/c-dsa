//1 2 3 4
//4 5 6
//6 7
//7
#include<stdio.h>

void main(){
	int row;
	printf("Enter row : ");
	scanf("%d",&row);

	int num = 1;
	for(int i = 1 ; i<=row ; i++){
		for(int j = i  ; j<=row ; j++){
			printf("%d ",num++);
		}
		printf("\n");
		num--;
	}
}
