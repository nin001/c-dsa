//4 3 2 1
//3 2 1
//2 1 
//1
#include<stdio.h>

void main(){
	int row;
	printf("Enter rows : ");
	scanf("%d",&row);

	int num = row;

	for(int i = 1 ; i<=row ; i++){
		int num2 = num;
		for(int j = i ; j<=row ; j++){
			printf("%d ",num2--);
		}
		printf("\n");
		num--;
	}
}

