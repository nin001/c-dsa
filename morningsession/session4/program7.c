//1 2 3 4 5
//2 3 4 5
//3 4 5
//4 5
//5
//4
#include<stdio.h>

void main(){
	int row;
	printf("Enter row : ");
	scanf("%d",&row);

	for(int i = 1 ; i<=row ; i++){
		for(int j = i ; j<=row ; j++){
			printf("%d ",j);
		}
		printf("\n");
	}
}
