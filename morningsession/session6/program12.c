//AbCd
// eGi
//  Kn
//   q
//
#include<stdio.h>

void main(){
	int row;
	printf("Enter row : ");
	scanf("%d",&row);

	char ch = 'A';
	for(int i = 1 ; i<=row ; i++){
		for(int sp = 1 ; sp<i ; sp++){
			printf("  ");
		}
		for(int j = i ; j<=row ; j++){
			if(j%2==0)
				printf("%c ",ch+32);
			else
				printf("%c ",ch);
			ch = ch+i;
		}
		printf("\n");
	}
}
