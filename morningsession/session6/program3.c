//1234
// 123
//  12
//   1
//
#include<stdio.h>

void main(){
	int row;
	printf("Enter row : ");
	scanf("%d",&row);

	for(int i = 1 ; i<=row ; i++){
		for(int sp = 1 ; sp<i ; sp++){
			printf("  ");
		}
		for(int j = 1 ; j<=row+1-i ; j++){
			printf("%d ",j);
		}
		printf("\n");
	}
}
