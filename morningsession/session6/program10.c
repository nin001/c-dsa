//a B c D
//  e F g 
//    H i
//      J

#include<stdio.h>

void main(){
	int row;
	printf("Enter row : ");
	scanf("%d",&row);

	char ch = 97;

	for(int i = 1 ; i<=row ; i++){
		for(int sp = 1 ; sp<i ; sp++){
			printf("  ");
		}
		for(int j = i ; j<=row ; j++){
			if(ch%2!=0)
				printf("%c ",ch);
			else
				printf("%c ",ch-32);
			ch++;
		}
		printf("\n");
	}
}
