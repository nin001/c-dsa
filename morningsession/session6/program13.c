//take range input and print number whose factorial is even within given range
//
//NOTE : except 1 each factorial contains *2
//therefore each fact is positive except 1and0

#include<stdio.h>

void main(){
	int x,y;
	printf("Enter 1st no : ");
	scanf("%d",&x);

	printf("Enter 2nd no : ");
	scanf("%d",&y);

	if(y<x){
		int temp = y;
		y = x;
		x = temp;
	}
	printf("Numbers who have even factorials : ");
	for(int i = x ; i<=y ; i++){
		if(i==1||i==-1||i==0){
			continue;
		}
		printf("%d ",i);
	}
	printf("\n");
}



