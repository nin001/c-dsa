//****
// ***
//  **
//   *
//
#include<stdio.h>

void main(){
	int row;
	printf("Enter row : ");
	scanf("%d",&row);

	for(int i = 1 ; i<=row ; i++){
		for(int sp = 1 ; sp<i ; sp++){
			printf(" ");
		}
		for(int j = i ; j<=row ; j++){
			printf("*");
		}
		printf("\n");
	}
}
