//100 9 64 7
//   36 5  16
//      3  4
//         1
//
#include<stdio.h>

void main(){
	int row;
	printf("Enter row : ");
	scanf("%d",&row);

	int num = 0;
	for(int i = 1 ; i<=row ; i++){
		for(int j = i ; j<=row ; j++){
			num++;
		}
	}

	for(int i = 1 ; i<=row ; i++){
		for(int sp = 1 ; sp<i ; sp++){
			printf("\t");
		}
		for(int j = i ; j<=row ; j++){
			if(num%2==0)
				printf("%d\t",num*num);
			else
				printf("%d\t",num);
			num--;
		}
		printf("\n");

	}	
}	
