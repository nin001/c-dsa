//1234
// 567
//  89
//   10
//
#include<stdio.h>

void main(){
	int row;
	printf("Enter row : ");
	scanf("%d",&row);

	int num = 1;

	for(int i = 1 ; i<=row ; i++){
		for(int sp = 1 ; sp<i ; sp++){
			printf("  ");
		}
		for(int j = i ; j<=row ; j++){
			printf("%d ",num++);
		}
		printf("\n");
	}
}
