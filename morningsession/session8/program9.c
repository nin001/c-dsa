//A0 B1 C2 D3 E4 F5 G6
//   H2 I3 J4 K5 L6
//      M4 N5 O6
//         P6

#include<stdio.h>

void main(){
	int row;
	printf("Enter row : ");
	scanf("%d",&row);

	char ch = 'A';
	int count = 0;
	for(int i = 1 ; i<=row ; i++){
		int num = 0 + count;

		for(int sp = 1 ; sp<i ; sp++){
			printf("\t");
		}
		for(int j = i ; j<=2*row-i ; j++){
			printf("%c%d\t",ch,num);
			ch++;
			num++;
		}
		printf("\n");
		count = count+2;
	}
}	
