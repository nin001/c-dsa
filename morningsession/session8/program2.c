//1 2 3 4 5
//  6 7 8
//    9

#include<stdio.h>

void main(){
	int row;
	printf("Enter row : ");
	scanf("%d",&row);
	
	int num = 1;

	for(int i = 1 ; i<=row ; i++){
		for(int sp = 1 ; sp<i ; sp++){
			printf("\t");
		}
		for(int j = i ; j<=row*2-i ; j++){
			printf("%d\t",num++);
		}
		printf("\n");
	}
}
