//take number of rows from user
//*******
// *****
//  ***
//   *
#include<stdio.h>

void main(){
	int row;
	printf("Enter rows : ");
	scanf("%d",&row);

	for(int i = 1 ; i<=row ; i++){
		for(int sp = 1 ; sp<i ; sp++){
			printf(" ");
		}
		for(int j = i ; j<=row*2-i ; j++){
			printf("*");
		}
		printf("\n");
	}
}
