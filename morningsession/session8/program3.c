//1234567
// 12345
//  123
//   1

#include<stdio.h>

void main(){
	int row;
	printf("Enter row : ");
	scanf("%d",&row);

	for(int i = 1 ; i<=row ; i++){
		int num = 1;
		for(int sp = 1 ; sp<i ; sp++){
			printf("  ");
		}
		for(int j = i ; j<=2*row-i ; j++){
			printf("%d ",num++);
		}
		printf("\n");
	}
}
