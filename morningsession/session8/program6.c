//1234321
// 12321
//  121
//   1

#include<stdio.h>

void main(){
	int row;
	printf("Enter row : ");
	scanf("%d",&row);

	for(int i = 1 ; i<=row ; i++){
		int num = 0;
		for(int sp = 1 ; sp<i ; sp++){
			printf("  ");
		}
		for(int j = i ; j<=2*row-i ; j++){
			if(j<=(2*row)/2)
				printf("%d ",++num);
			else
				printf("%d ",--num);
		}
		printf("\n");
	}
}
