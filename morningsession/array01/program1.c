// count the number of elements that are greater than itself

#include<stdio.h>

int greaterNoCount(int arr[] , int size ){
	int count =  0;
	for(int i = 0 ; i<size ; i++){
		for(int j = 0 ; j<size ; j++){
			if(arr[i]<arr[j]){
				count++;
				break;
			}
		}
	}
	return count;
}

void main(){
	int size;
	printf("Enter size : ");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array elements :\n");
	for(int i = 0 ; i<size ; i++)
		scanf("%d",(arr+i));

	printf("Number of elements that have at least 1 element greater : %d\n",greaterNoCount(arr,size));

}
