// Array rotation

#include<stdio.h>

int* rotateArr(int arr[] , int size , int k){

	while(k!=0){
		int temp1,temp2;
		for(int i = size-1 ; i>=0 ; i--){
			if(i==size-1){
				temp1 = arr[i];
				arr[i] = arr[0];
			}else{
				temp2 = arr[i];
				arr[i] = temp1;
				temp1 = temp2;
			}
		}
		k--;
	}

	return arr;
}
void main(){
        int size;
        printf("Enter size : ");
        scanf("%d",&size);

        int arr[size];

        printf("Enter array elements :\n");
        for(int i = 0 ; i<size ; i++)
 		scanf("%d",(arr+i));

	int k;
	printf("Enter number of rotation to be needed in array : ");
	scanf("%d",&k);
	
	rotateArr(arr,size,k);

        printf("Array elements after rotating :\n");
        for(int i = 0 ; i<size ; i++)
 		printf("%d ",*(arr+i));
	printf("\n");
}
