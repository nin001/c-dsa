// reverse in given range
#include<stdio.h>

int* revArrRange(int arr[] , int a , int b){
	while(a<=b){
		int temp = arr[a];
		arr[a] = arr[b];
		arr[b] = temp;

		a++;
		b--;
	}
	return arr;
}

void main(){
	int size;
	printf("Enter size : ");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array elements :\n");
	for(int i = 0 ; i<size ; i++)
		scanf("%d",(arr+i));

	int a,b;
	printf("Enter num1 of range : ");
	scanf("%d",&a);
	
	printf("Enter num2 of range : ");
	scanf("%d",&b);

	if(b<size && a>=0 && a<b){
		revArrRange(arr,a,b);
		printf("newArray :\n");
		for(int i = 0 ; i<size ; i++)
			printf("%d ",*(arr+i));
		printf("\n");
	}else{
		printf("Invalid numbers given for range\n");
	}
}
