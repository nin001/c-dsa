// Good pair
// given array arr and number b
// condition for good pair
// (i,j) is good pair if(i!=j) and (arr[i]+arr[j] == b)
#include<stdio.h>

int goodPair(int arr[] , int size , int b){
	for(int i = 0 ; i<size ; i++){
		for(int j = 0 ; j<size ; j++){
			if(i!=j && (arr[i]+arr[j] == b))
				return 1;
		}
	}
	return 0;
}

void main(){
	int size;
	printf("Enter size : ");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array elements :\n");
	for(int i = 0 ; i<size ; i++)
		scanf("%d",(arr+i));

	int num;
	printf("Enter number : ");
	scanf("%d",&num);

	int ret = goodPair(arr,size,num);
	if(ret == 1)
		printf("Good pair present\n");
	else
		printf("Good pair not present\n");

}
