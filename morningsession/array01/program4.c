// Reverse array and return in another array
#include<stdio.h>

int* revArr(int arr[] , int size , int retArr[]){
	int index = 0;
	for(int i = size-1 ; i>=0 ; i--)
		retArr[index++] = arr[i];

	return retArr;
}

void main(){
        int size;
        printf("Enter size : ");
        scanf("%d",&size);

        int arr[size];

        printf("Enter array elements :\n");
        for(int i = 0 ; i<size ; i++)
                scanf("%d",(arr+i));

	int returnarr[size];

	revArr(arr,size,returnarr);

        printf("Reversed array :\n");
        for(int i = 0 ; i<size ; i++)
                printf("%d ",*(returnarr+i));
	printf("\n");	
}
