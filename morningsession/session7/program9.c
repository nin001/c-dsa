//    5
//   565
//  54345
// 5678765
//543212345
//
#include<stdio.h>

void main(){
	int row;
	printf("Enter row : ");
	scanf("%d",&row);
	int num = row+1;
	for(int i = 1 ; i<=row ; i++){
		for(int sp = i ; sp<row ; sp++){
			printf("  ");
		}
		for(int j = 1 ; j<2*i ; j++){
			if(j<=2*i/2){
				if(i%2==0){
					printf("%d ",++num);
				}else{
					printf("%d ",--num);
				}
			}else{
				if(i%2==0){
					printf("%d ",--num);
				}else{
					printf("%d ",++num);
				}
			}
		}
		printf("\n");
		if(i%2!=0)
			num--;
		else
			num++;


	}
}
