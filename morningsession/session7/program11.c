//   1
//  AbA
// 12321
//AbCdCbA
//
#include<stdio.h>

void main(){
	int row;
	printf("Enter row : ");
	scanf("%d",&row);

	for(int i = 1 ; i<=row ; i++){
		char ch = 'A'-1;
		int num = 0;
		for(int sp = i ; sp < row ; sp++){
			printf("  ");
		}

		for(int j = 1 ; j<2*i ; j++){
			if(j<=2*i/2){
				if(i%2 != 0){
					printf("%d ",++num);
				}else{
					if(ch%2 == 0){
						printf("%c ",++ch);
					}else{
						ch++;
						printf("%c ",ch+32);
					}
				}
				
			}else{
				if(i%2 != 0){
					printf("%d ",--num);
				}else{
					if(ch%2 == 0){
						printf("%c ",--ch);
					}else{
						ch--;
						printf("%c ",ch);
					}
				}
			}
		}
		printf("\n");
	}

}
					
