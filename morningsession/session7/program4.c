//   4
//  434
// 43234
//4321234

#include<stdio.h>

void main(){
	int row;
	printf("Enter row : ");
	scanf("%d",&row);

	for(int i = 1 ; i<=row ; i++){
		int num = row+1;
		for(int sp = i ; sp<row ; sp++){
			printf("  ");
		}
		for(int j = 1 ; j<2*i ; j++){
			if(j<=(2*i/2))
				printf("%d ",--num);
			else
				printf("%d ",++num);
		}
		printf("\n");
	}
}	
