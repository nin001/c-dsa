//   4
//  333
// 22222
//1111111

#include<stdio.h>

void main(){
	int row;
	printf("Enter row : ");
	scanf("%d",&row);

	int num = row;

	for(int i = 1 ; i<=row ; i++){
		for(int sp = i ; sp<row ; sp++){
			printf("  ");
		}
		for(int j = 1 ; j<2*i ; j++){
			printf("%d ",num);
		}
		printf("\n");
		num--;
	}
}
