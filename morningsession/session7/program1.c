//   *
//  ***
// *****
//*******

#include<stdio.h>

void main(){
	int row;
	printf("Enter row : ");
	scanf("%d",&row);

	for(int i = 1 ; i<=row ; i++){
		for(int sp = i ; sp<row	; sp++){
			printf("  ");
		}
		for(int j = 1 ; j<2*i ; j++){
			printf("* ");
		}
		printf("\n");
	}
}
