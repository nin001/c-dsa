//			1
//		4	7	4
//	7	10	13	10	7
//10	13	16	19	16	13	10
//
#include<stdio.h>

void main(){
	int row;
	printf("Enter row : ");
	scanf("%d",&row);
	
	int num = -2;

	for(int i = 1 ; i<=row ; i++){
		for(int sp = i ; sp<row ; sp++){
			printf("\t");
		}
		for(int j = 1 ; j<2*i ; j++){
			if(j<=(2*i/2)){
				num = num + 3;
				printf("%d\t",num);
				
			}else{
				num = num - 3;
				printf("%d\t",num);
			}
		}
		
		printf("\n");
	}
}

