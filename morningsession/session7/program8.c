//   D
//  cDc
// BcdcB
//aBcDcBa
//
#include<stdio.h>

void main(){
	int row;
	printf("Enter row : ");
	scanf("%d",&row);
	
	char ch1 = 63+row;

	for(int i = 1 ; i<=row ; i++){
		char ch = ch1;
		for(int sp = i ; sp<row ; sp++){
			printf("  ");
		}
		for(int j = 1 ; j<2*i ; j++){
			if(j<=2*i/2){
				if(ch%2==0){
					ch++;
					printf("%c ",ch+32);
				}else{
					ch++;
					printf("%c ",ch);
				}
			}else{
				if(ch%2==0){
					ch--;
					printf("%c ",ch+32);
				}else{
					ch--;
					printf("%c ",ch);

				}
			}
		}
		printf("\n");
		ch1--;
	}
}

