//   4
//  363
// 24642
//1234321

#include<stdio.h>

void main(){
	int row;
	printf("Enter row : ");
	scanf("%d",&row);
	
	int num = row;
	for(int i = 1 ; i<=row ; i++){
		int div = i-1;
		for(int sp = i ; sp<row ; sp++){
			printf("  ");
		}
		for(int j = 1 ; j<2*i ; j++){
			if(j<=2*i/2){
				printf("%d ",num*j);
			}else{
				printf("%d ",num*div);
				div--;
			}
		}
		printf("\n");
		num--;		
	}
}

