//1234
//abcd
//5678
//efgh
#include<stdio.h>

void main(){
	int row;
	printf("Enter row : ");
	scanf("%d",&row);
	int x = 1;
	char ch = 'a';
	for(int i = 1 ; i<=row ; i++){
		for(int j = 1 ; j<=row ; j++){
			if(i%2!=0){
				printf("%d\t",x);
				x++;
			}else{
				printf("%c\t",ch);
				ch++;
			}
		}
		printf("\n");
	}
}
