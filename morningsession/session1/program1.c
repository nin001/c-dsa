//print
//***
//***
//***
#include<stdio.h>

void main(){
	int row;
	printf("Enter row : ");
	scanf("%d",&row);
	for(int i = 1 ; i<=row ; i++){
		for(int j = 1 ; j<=row ; j++){
			printf("*");
		}
		printf("\n");
	}
}
/*
 * DRY RUN
 * row = 3
 i = 1 i<=3 j = 1 j<=3 print(*) j++ "\n" i++
       1<=3   1   1<=3    *      2
                  2<=3    *      3
		  3<=3    *      4
		  4<=3
		  false              \n   2
       2<=3   1   1<=3    *      2
       		  2<=3    *      3
		  3<=3    *     4
		  4<=3
		  false              \n   3
       3<=3   1   1<=3    *     2
                  2<=3    *     3
		  3<=3    *     4
		  4<=3 
		  false              \n   4
	4<=3
	false
*/
