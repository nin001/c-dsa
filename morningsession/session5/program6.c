//    5
//   56
//  543
// 5678
//54321
//
#include<stdio.h>

void main(){
	int row;
	printf("Enter row : ");
	scanf("%d",&row);

	for(int i = 1 ; i<=row ; i++){
		int num = row;
		for(int sp = i ; sp<row ; sp++){
			printf("  ");
		}
		for(int j = 1 ; j<=i ; j++){
			if(i%2==0)
				printf("%d ",num++);
			else
				printf("%d ",num--);
		}
		printf("\n");
	}
}
