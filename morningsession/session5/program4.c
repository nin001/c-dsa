//take rows from user 
//      1
//   4  7
//10 130 16
//row = 3
//
#include<stdio.h>

void main(){
	int row;
	printf("Enter row : ");
	scanf("%d",&row);

	int num = 1;
	for(int i = 1 ; i<=row ; i++){
		for(int sp = i ; sp<row ; sp++){
			printf("\t");
		}
		for(int j = 1 ; j<=i ; j++){
			printf("%d\t",num);
			num = num + row;
		}
		printf("\n");
	}
}
