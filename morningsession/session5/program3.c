//      d
//    c c
//  b b b
//a a a a
#include<stdio.h>

void main(){
	int row;
	printf("Enter row : ");
	scanf("%d",&row);

	char ch = 96+row;
	for(int i = 1 ; i<=row ; i++){
		for(int sp = i ; sp<row ; sp++){
			printf("  ");
		}
		for(int j = 1 ; j<=i ; j++){
			printf("%c ",ch);
		}
		printf("\n");
		ch--;
	}
}
