//
//   1
//  Ab
// 123
//AbCd
//
#include<stdio.h>

void main(){
	int row;
	printf("Enter row : ");
	scanf("%d",&row);

	for(int i = 1 ; i<=row ; i++){
		char ch = 'A';
		for(int sp = i ; sp<row ; sp++){
			printf("  ");
		}
		for(int j = 1 ; j<=i ; j++){
			if(i%2!=0)
				printf("%d ",j);
			else{
				if(j%2==0)
					printf("%c ",ch+32);
				else
					printf("%c ",ch);
			}
			ch++;
		}
		printf("\n");
	}
}
