//   1
//  12
// 123
//1234
#include<stdio.h>

void main(){
	int row;
	printf("Enter row :");
	scanf("%d",&row);

	for(int i = 1 ; i<=row ; i++){
		for(int sp = i ; sp<row ; sp++){
			printf("\t");
		}	
		for(int j = 1 ; j<=i ; j++){
			printf("%d\t",j);
		}
		printf("\n");
	}
}
