//    A
//   ba
//  CeG
// dcba
//EGIKM

#include<stdio.h>

void main(){
	int row;
	printf("Enter row : ");
	scanf("%d",&row);
	char ch1 = 'A';
	for(int i = 1 ; i<=row ; i++){
		char ch2 = ch1;
		for(int sp = i ; sp<row ; sp++){
			printf("  ");
		}
		for(int j = 1 ; j<=i ; j++){
			if(i%2==0){
				printf("%c ",ch2+32);
				ch2--;
			}else{
				printf("%c ",ch2);
				ch2 = ch2+2;
			}
		}
		printf("\n");
		ch1++;	
	}
}
