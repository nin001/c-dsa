/*
 Multi threading is possible in c language 
 we can create new threads (user threads) and assign them a function of desire and make them execute simultaneously
 multi threading is possible using a library called ptherad which is based on POSIX standardization
 POSIX - portable operating system interface
 */

#include<stdio.h>
#include<unistd.h>
#include<pthread.h>

// to check that if thread present or not 
// there is atleast one thread that is present in a process which is not created by user 

void fun(){
	printf("in fun\n");

	printf("Fun thread id : %ld\n",pthread_self());

	printf("end fun\n");

}

void main(){

	printf("in main\n");

	printf("thread id : %ld\n",pthread_self());
	
	fun();

	printf("End main\n");

}

/*
 * output
 in main
thread id : 139940614522688		//the id inside fun and main is the same because there is one thread 
in fun					//present in the process.
Fun thread id : 139940614522688
end fun
End main

*/
