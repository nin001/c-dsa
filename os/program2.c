// Fork system call 
// Fork system call is a call which helps creating a process 
// This is a system call
/*pid_t fork(void); 
 * this is prototype of the fork function */
/*fork() creates a new process by duplicating the calling process.  The new process is referred to as the child process.  The calling process is referred to
       as the parent process.*/

#include<stdio.h>
#include<unistd.h>   // header file consisting fork system call

// Fork system call creates a child process which consists the context of its parent(code of parent)(Parent is the process which has called fork() sys call

void main(){

	int ret = fork();

	if(ret == 0){
		printf("Child Pid : %d\n",getpid());
		printf("Childs parent (a.out) : %d\n",getppid());
	}else{
		printf("A.out Pid : %d\n",getpid());
		printf("A.out parent (Shell) : %d\n",getppid());

	}
}

// Fork system call returns the pid of itself and 0 where 0 is for successful fork call and -1 for unsuccessful
// here it doesnt get into recursion because the ret variable when child process calls fork gets 0 and no further process is generated

/*output
2802
2660
2803
1430


A.out Pid : 2815
A.out parent (Shell) : 2660
Child Pid : 2816
Childs parent (a.out) : 1430
*/


