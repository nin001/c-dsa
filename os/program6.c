//fork system call code assigning child a file to execute
#include<stdio.h>
#include<unistd.h>

void main(){
	int pid = fork();

	if(pid != 0){
		printf("Parent pid : %d\n",getpid());
		printf("Ajoba pid : %d\n",getpid());
	}else{
		printf("In child\n");
		execlp("/home/niraj/c/os/code","nin",NULL);
	}
}
//
//output
/*
Parent pid : 3220
Ajoba pid : 3220
In child
niraj@niraj-IdeaPad:~/c/os$ Sum = 55   -- this shows that new process was created that executed target file's
					code which was compiled as the name of "code"
*/

