// creating multiple threads in a single process
#include<stdio.h>
#include<pthread.h>

void* gun(){
	printf("gun - %ld\n",pthread_self());
	for(int i = 0 ; i<3 ; i++)
		printf("gun -> i : %d\n",i);
}

void* fun(){
	printf("fun - %ld\n",pthread_self());
	for(int i = 0 ; i<3 ; i++)
		printf("fun -> i : %d\n",i);

	pthread_t tid;
	pthread_create(&tid,NULL,gun,NULL);
	printf("End fun\n");
}

void main(){
	pthread_t tid;
	printf("main - %ld\n",pthread_self());

	pthread_create(&tid,NULL,fun,NULL);

	pthread_join(tid,NULL);		// pthread_join is a function which makes main thread wait untill the
					// child thread has returned(NULL in out case) mentioned in the second 
					// parameter
	printf("End main\n");

}

/*
 * output
 main - 139879760516928
fun - 139879760512576
fun -> i : 0
fun -> i : 1
fun -> i : 2
End fun
End main
gun - 139879752119872
gun - 139879752119872
0
gun -> i : 1
gun -> i : 2
gun -> i : 2
*/
