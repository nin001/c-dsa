//WAP which explains the concept of array of pointers

#include<stdio.h>

void main(){
	int a,b,c,d;
	a = 10; b = 20 ; c = 30 ; d = 40 ;

	int*(arr[4]) = {&a,&b,&c,&d};

/* Array of pointers : 
	 * Array which has all the elements as pointers 
	*/
	
	printf("Values in array of pointers (dereferenced):\n");
	for(int i = 0 ; i<4 ; i++){
		printf("%d\n",*arr[i]);
	}
	printf("Addresses :\n");
	for(int i = 0 ; i<4 ; i++){
		printf("%p\n",arr[i]);
	}
}
