// WAP to create array of size n and sort and print in ascending order

#include<stdio.h>

void main(){
	int size;
	printf("Enter size : ");
	scanf("%d",&size);

	int arr[size];
	printf("Enter array elements :\n");
	for(int i = 0 ; i<size ; i++){
		scanf("%d",&arr[i]);
	}

	for(int i = 0 ; i<size ; i++){
		for(int j = 0 ; j<size ; j++){
			if(arr[i]<arr[j]){
				int temp;
				temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
			}
		}
	}
	printf("Sorted array :\n");
	for(int i = 0 ; i<size ; i++){
		printf("%d\n",arr[i]);
	}
}
