//WAp to create array of size n given by user and take values from user and print
//using pointer
//
#include<stdio.h>

void main(){
	int size;
	printf("Enter size : ");
	scanf("%d",&size);

	int arr[size];
	
	printf("Enter array elements :\n");
	for(int i = 0 ; i<size ; i++){
		scanf("%d",&(*(arr+i)));
	}
	printf("Array elements :\n");
	for(int i = 0 ; i<size ; i++){
		printf("%d\n",*(arr+i));
	}
}
