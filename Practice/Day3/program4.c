//WAP to show concept of array of pointers to an array
//
/*Concept : It is a array of pointers which are pointing to the array*/

#include<stdio.h>

void main(){
	int arr1[] = {1,2,3,4,5};
	int arr2[] = {6,7,8,9,10};
	int arr3[] = {11,12,13,14,15};

	int (*ptr[5])[] = {&arr1,&arr2,&arr3}; //Array of pointers pointing to arrays

	for(int i = 0 ; i<3 ; i++){
		printf("%d : ",i+1);
		for(int j = 0 ; j<5 ; j++){
			printf("%d ",*(*(ptr[i])+j));
		}
		printf("\n");
	}

}

