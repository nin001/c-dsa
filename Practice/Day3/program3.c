//WAP that shows the concept of pointer to an array

/*Pointer to an array : A pointer to an array is a pointer which is pointing
 * 			towards the whole array
*/
#include<stdio.h>

void main(){
	int arr1[5] = {1,2,3,4,5};
	int arr2[5] = {10,20,30,40,50};
	int arr3[5] = {11,21,31,41,51};
	int arr4[5] = {12,22,32,42,52};




	int (*ptr)[5] = &arr1;
	//as it is pointing towards the whole array
/*	
	for(int i = 1 ; i<=3 ; i++){
		printf("array %d :\n",i);
		for(int j = 0 ; j<5 ; j++){
			printf("%d\n",*(*(ptr)+j));
		}
		ptr++;
	}*/
	printf("%p\n",ptr);
	printf("%d\n",**ptr);
	ptr++;
	printf("%d\n",**ptr);
}
