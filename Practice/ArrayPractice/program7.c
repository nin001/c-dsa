// sum of all elements

#include <stdio.h>

void main() {
	int n;
	printf("Enter size : ");
	scanf("%d",&n);

	int sum = 0;

	int arr[n];
	printf("Enter array elements\n");
	for(int i = 0 ; i<n ; i++) {
		scanf("%d",(arr+i));
		sum += *(arr+i);
	}

	printf("Sum of array : %d\n" , sum);
}
