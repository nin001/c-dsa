// take input 2 arrays and print the common elements in the array

#include <stdio.h>

void main() {
	int n;
	printf("Enter size of First array: ");
	scanf("%d",&n);

	int arr1[n];

	int m;
	printf("Enter size of Second array : ");
	scanf("%d",&m);

	int arr2[m];

	printf("Enter elements in first array : ");
	for(int i = 0 ; i<n ; i++) {
		scanf("%d",(arr1+i));
	}

	printf("Enter elements in second array : ");
	for(int i = 0 ; i<m ; i++) {
		scanf("%d",(arr2+i));
	}
	
	int mergedArr[m+n];
	for(int i = 0 ; i<m+n ; i++) {
		if(i<n) {
			mergedArr[i] = arr1[i];
			printf("%d ",mergedArr[i]);
		}else {
			mergedArr[i] = arr2[i-n];
			printf("%d ",mergedArr[i]);
		}
	}
	printf("\n");
}
