//sort array in ascending order

#include <stdio.h>

void main() {
	int size;
	printf("Enter size : ");
	scanf("%d" , &size);

	int arr[size];
	printf("Enter array elements\n");
	for(int i = 0 ; i<size ; i++) {
		scanf("%d",(arr+i));
	}

	printf("Sorting array\n");

	for(int i = 1 ; i<size ; i++) {
		int minIndex = i-1;
		for(int j = i ; j<size ; j++) {
			if(arr[j]<arr[minIndex])
				minIndex = j;
		}

		int temp = arr[i-1];
		arr[i-1] = arr[minIndex];
		arr[minIndex] = temp;
	}
	
	printf("Sorted Array elements\n");
	for(int i = 0 ; i<size ; i++) {
		printf("%d ",*(arr+i));
	}
	printf("\n");
}
