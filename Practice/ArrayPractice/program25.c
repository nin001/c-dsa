// subarray with given sum
//
// naive approch

#include <stdio.h>

void subarraySum(int *arr , int size , int sum) {
	for(int i = 0 ; i<size ; i++) {
		int temp = *(arr+i);
		for(int j = i+1 ; j<size ; j++) {
			temp += *(arr+j);
			if(temp == sum) { 
				printf("Subarray found : from %d to %d index\n",i,j);
				return;
			}
			if(temp>sum)
				break;
		}
	}
	printf("No subarray with sum %d\n",sum);
	return;
}

void main() {

	int size;
	printf("Enter size : ");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array elements : ");
	for(int i = 0 ; i<size ; i++) {
		scanf("%d",(arr+i));
	}

	int sum;
	printf("Enter subarray sum : ");
	scanf("%d",&sum);

	subarraySum(arr,size,sum);
}
