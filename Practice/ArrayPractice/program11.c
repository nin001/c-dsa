// take a character array input and print only alphabets not speacial characters

#include <stdio.h>

void main() {
	int n;
	printf("enter size of character array : ");
	scanf("%d",&n);

	getchar();

	char carr[n];
	printf("Enter character array : ");
	fgets(carr,n,stdin);

	printf("Character array : ");
	for(int i = 0 ; i<n ; i++) {
		if((*(carr+i) >= 65 && *(carr+i)<=90) || (*(carr+i)>=97 && *(carr+i)<=120))
		       printf("%c",*(carr+i));
	}
	printf("\n");
}
