// program to remove specific element from the array 
#include <stdio.h>
#include <stdbool.h>

bool removeEle(int *ptr , int *size , int ele) {
	for(int i = 0 ; i<*size ; i++) {
		if(*(ptr+i) == ele) {
			if(i == (*size-1)) {
				*(ptr+i) = 0;
			}else {
				for(int j = i+1 ; j<*size ; j++) {
					*(ptr+(j-1)) = *(ptr+j);
				}
			}
			*size = *size-1;
			return true;
		}
	}
	
	return false;
}

void main() {
	int n;
	printf("Enter size ; ");
	scanf("%d",&n);

	int arr[n];
	printf("Enter array elements : ");
	for(int i = 0 ; i<n ; i++) {
		scanf("%d",(arr+i));
	}

	int ele;
	printf("Enter element to remove : ");
	scanf("%d",&ele);

	if(removeEle(arr,&n,ele)) {
		for(int i = 0 ; i<n ; i++) {
			printf("%d ",*(arr+i));
		}
		printf("\n");
	}else {
		printf("Element not found in the array");
	}
}
