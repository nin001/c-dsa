// Given array with all distinct elements find the largest three elements

#include <stdio.h>
#include <limits.h>

void main() {
	int n;
	printf("Enter size : ");
	scanf("%d",&n);

	int arr[n];

	printf("Enter array size : ");
	for(int i = 0 ; i<n ; i++) {
		scanf("%d",&(arr[i]));
	}

	int first = INT_MIN;
	int second = INT_MIN;
	int third = INT_MIN;

	for(int i = 0 ; i<n ; i++) {
		if(arr[i]>first) {
			third = second;
			second = first;
			first = arr[i];
		}else if(arr[i]>second && arr[i]!=first) {
			third = second;
			second = arr[i];
		}else if(arr[i]>third && arr[i]!=second) {
			third = arr[i];
		}
	}

	printf("%d %d %d\n",first,second,third);
}
