// create array for 5 integer element from user and print

#include <stdio.h>

void main() {

	int size = 5;

	int arr[size];

	printf("Enter array elements :\n");
	for(int i = 0 ; i<size ; i++) 
		scanf("%d",(arr+i));

	printf("Array : ");
	for(int i = 0 ; i<size ; i++)
		printf("%d ", *(arr+i));

	printf("\n");
}
