// take array from user and print the minimum

#include <stdio.h>

int minArr(int *ptr , int size) {

	int min = *(ptr+0);
	for(int i = 0 ; i<size ; i++) {
		if(*(ptr+i) < min)
			min = *(ptr+i);
	}
	return min;

}
void main() {
	int n;
	printf("Enter size :");
	scanf("%d",&n);

	int arr[n];
	printf("Enter array elements\n");
	for(int i = 0 ; i<n ; i++)
		scanf("%d",(arr+i));

	printf("Minimum in array : %d\n",minArr(arr,n));
}
