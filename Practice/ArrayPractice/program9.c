// find strong number in the array 

#include <stdio.h>

int strongInArray(int *ptr , int n) {
	for(int i = 0 ; i<n ; i++) {
		int ele = *(ptr+i);

		int digCount = 0;
		while(ele != 0) {
			digCount++;
			ele /= 10;
		}

		ele = *(ptr+i);

		int strong = 0;

		while(ele != 0 ) {
			int store = 1;
			for(int i = 0 ; i<digCount ; i++)
				store *= ele%10;
			strong+=store;
		}

		if(strong == *(ptr+i))
			return *(ptr+i);
	}

	return -1;
}


void main() {
	int n;
	printf("Enter array size : ");
	scanf("%d",&n);

	int arr[n];
	printf("Enter array elements\n");
	for(int i = 0 ; i<n ; i++) 
		scanf("%d",(arr+i));
	
	printf(".");
	int ret = strongInArray(arr,n);
	
	if(ret != -1)
		printf("Strong in array : %d\n",ret);
	else
		printf("Strong number not present in array\n");
	
}
