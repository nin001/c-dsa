// Find sum of even and odd nums in array

#include <stdio.h>

void main() {
	int n;
	printf("Enter size : ");
	scanf("%d",&n);

	int evenSum = 0;
	int oddSum =  0;

	int arr[n];
	printf("enter array elements\n");
	for(int i = 0 ; i<n ; i++) {
		scanf("%d",(arr+i));
		if(*(arr+i)%2 == 0)
			evenSum += *(arr+i);
		else
			oddSum += *(arr+i);
	}
	
	printf("sum of even numbers : %d\n" , evenSum);
	printf("sum of odd numbers : %d\n" , oddSum);
}

