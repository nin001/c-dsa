// Given array of 0s 1s and 2s Sort the given array

#include <stdio.h>

void swap(int *ptr1 , int *ptr2) {
	int temp = *ptr1;
	*ptr1 = *ptr2;
	*ptr2 = temp;
}

void sortArray(int *arr , int size) {
	int low = 0;
	int mid = 0;
	int high = size-1;

	while(mid<=high) {
		if(*(arr+mid) == 0) {
			swap((arr+mid),(arr+low));
			mid++;
			low++;
		}else if(*(arr+mid) == 1) {
			mid++;
		}else if(*(arr+mid) == 2) {
			swap((arr+mid),(arr+high));
			high--;
		}
	}
}

void main() {
	int size;
	printf("Enter size : ");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array elements : ");
	for(int i = 0 ; i<size ; i++) {
		scanf("%d",(arr+i));
	}

	sortArray(arr,size);

	printf("Sorted Array");
	for(int i = 0 ; i<size ; i++) {
		printf("%d ",*(arr+i));
	}
	printf("\n");
}
