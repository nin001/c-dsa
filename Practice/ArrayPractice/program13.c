// Find no of even and odd nums in array

#include <stdio.h>

void main() {
	int n;
	printf("Enter size : ");
	scanf("%d",&n);

	int evenCount = 0;
	int oddCount =  0;

	int arr[n];
	printf("enter array elements\n");
	for(int i = 0 ; i<n ; i++) {
		scanf("%d",(arr+i));
		if(*(arr+i)%2 == 0)
			evenCount++;
		else
			oddCount++;
	}
	
	printf("No of even numbers : %d\n" , evenCount);
	printf("No of odd numbers : %d\n" , oddCount);
}

