//wap to create array of 5 integer element and print the array

#include <stdio.h>

void printArray(int *ptr , int size) {
	for(int i = 0 ; i<size ; i++) {
		printf("%d ",*(ptr+i));
	}
	printf("\n");
}

void main() {
	int arr[] = {1,2,3,4,5,6};

	int size = sizeof(arr)/sizeof(arr[0]);
	printArray(arr,size);
}
