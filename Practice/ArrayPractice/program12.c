// take input 2 arrays and print the common elements in the array

#include <stdio.h>

void commonElements(int *ptr1 , int s1 , int *ptr2 , int s2) {
	for(int i = 0 ; i<s1 ; i++) {
		for(int j = 0 ; j<s2 ; j++) {
			if(*(ptr1+i) == *(ptr2+j)) {
				printf("%d ",*(ptr1+i));
				break;
			}
		}
	}
	printf("\n");
}

void main() {
	int n;
	printf("Enter size of First array: ");
	scanf("%d",&n);

	int arr1[n];

	int m;
	printf("Enter size of Second array : ");
	scanf("%d",&m);

	int arr2[m];

	printf("Enter elements in first array : ");
	for(int i = 0 ; i<n ; i++) {
		scanf("%d",(arr1+i));
	}

	printf("Enter elements in second array : ");
	for(int i = 0 ; i<m ; i++) {
		scanf("%d",(arr2+i));
	}

	commonElements(arr1,n,arr2,m);
}
