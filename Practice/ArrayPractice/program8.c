// frequency of elements in the array

#include <stdio.h>

void freqElementsArray(int *ptr , int size) {
	for(int i = 0 ; i<size ; i++) {
		int freq = 0;
		int flag = 0;
		for(int j = 0 ; j<size ; j++) {
			if(j<i && *(ptr+i) == *(ptr+j)) {
				flag = 1;
				break;
			}else if(*(ptr+i) == *(ptr+j))
				freq++;
		}
		if(flag == 0)
			printf("Frequency of %d is %d\n",*(ptr+i),freq);
	}
}

void main() {
	int n;
	printf("Enter size : ");
	scanf("%d",&n);

	int arr[n];
	printf("Enter array elements\n");
	for(int i = 0 ; i<n ; i++) 
		scanf("%d",(arr+i));

	freqElementsArray(arr,n);
}
