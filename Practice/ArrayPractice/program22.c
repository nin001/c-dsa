// program to reverse array

#include <stdio.h>

void reverseArray(int *arr , int size ) {
	int index1 = 0;
	int index2 = size-1;
	while(index1<index2) {
		int temp = *(arr+index1);
		*(arr+index1) = *(arr+index2);
		*(arr+index2) = temp;

		index1++;
		index2--;
	}
}

void printArray(int *arr , int size) {
	for(int i = 0 ; i<size ; i++) 
		printf("%d ",*(arr+i));
	printf("\n");
}

void main() {

	int size;
	printf("Size : ");
	scanf("%d",&size);

	int arr[size];
	printf("Elements : ");
	for(int i = 0 ; i<size ; i++)
		scanf("%d",(arr+i));
	printArray(arr,size);

	reverseArray(arr,size);
	printArray(arr,size);
}
