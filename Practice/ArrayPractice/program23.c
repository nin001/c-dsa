// Kth largest and Kth smallest 
//
// smallest code 

#include <stdio.h>

void sort(int *arr, int size) {
	for(int i = 0 ; i<size-1 ; i++) {
		int min = i;
		for(int j = i+1 ; j<size ; j++) {
			if(*(arr+min) > *(arr+j))
				min = j;
		}

		int temp = *(arr+min);
		*(arr+min) = *(arr+i);
		*(arr+i) = temp;
	}
}

int KthSmallest(int *arr , int size , int k) {
	sort(arr,size);		//sorts array in ascending order
	return *(arr+(k-1));
}

void main() {
	int size;
	scanf("%d",&size);

	int arr[size];
	for(int i = 0 ; i<size ; i++)
		scanf("%d",(arr+i));
	int k;
	scanf("%d",&k);
	int ret = KthSmallest(arr,size,k);
	printf("%d\n",ret);


}
