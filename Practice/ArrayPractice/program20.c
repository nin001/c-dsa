// Return index where number is greater than the neighbours

#include <stdio.h>

int number(int *arr , int size) {
	for(int i = 0 ; i<size ; i++) {
		if(i == 0) {
			if(*(arr+i) > *(arr+(i+1))) {
				return *(arr+i);
			}
		}else if(i == size-1) {
			if(*(arr+i) > *(arr+(i-1))) {
				return *(arr+i);
			}
		}else if(*(arr+i) > *(arr+(i+1)) && *(arr+i) > *(arr+(i-1))) 
			return *(arr+i);
	}
	return -1;
}

void main() {
	int size;
	printf("Enter size : ");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array : ");
	for(int i = 0 ; i<size ; i++) 
		scanf("%d",(arr+i));

	int ret = number(arr,size);
	if(ret!=-1) {
		printf("Number greater than its neighbours : %d\n",ret);
	}else {
		printf("Number not found\n");
	}


}
