#include <stdio.h>

void sortArray(int *arr, int size) {
    for (int i = 0; i < size - 1; i++) {
        int min = i;
        for (int j = i + 1; j < size; j++) {
            if (arr[j] < arr[min]) {
                min = j;
            }
        }
        int temp = arr[min];
        arr[min] = arr[i];
        arr[i] = temp;
    }
}

void removeDuplicate(int *arr, int *n) {
    if (*n == 1)
        return;

    sortArray(arr, *n);

    int temp = 0;
    int itr = temp + 1;
    while (itr < *n) {
        if (arr[temp] == arr[itr]) {
            for (int i = itr + 1; i < *n; i++) {
                arr[i - 1] = arr[i];
            }
            (*n)--;
        } else {
            itr++;
            temp++;
        }
    }
}

int main() {
    int size;
    printf("Enter size: ");
    scanf("%d", &size);

    int arr[size];
    printf("Enter array elements: ");
    for (int i = 0; i < size; i++)
        scanf("%d", &arr[i]);

    removeDuplicate(arr, &size);

    printf("New size: %d\n", size);

    return 0;
}

