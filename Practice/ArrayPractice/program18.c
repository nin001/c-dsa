// Armstrong number in the array

#include <stdio.h>

void armstrongNumbers(int *arr , int n) {
	for(int i = 0 ; i<n ; i++) {
		int temp = *(arr+i);
		int digCount = 0;
		while(temp != 0){
			digCount++;
			temp /= 10;
		}

		temp = *(arr+i);
		int armS = 0;

		while(temp!=0) {
			int mult = 1;
			for(int i = 0 ; i<digCount ; i++) {
				mult *= temp%10;
			}
			armS += mult;
			temp /=10;
		}

		if(armS == *(arr+i))
			printf("%d\n",armS);
	}
}		

void main() {
	int size;
	printf("Enter array size : ");
	scanf("%d",&size);

	int arr[size];
	printf("Enter array elements : ");
	for(int i = 0 ; i<size ; i++) 
		scanf("%d",(arr+i));

	armstrongNumbers(arr,size);
}
