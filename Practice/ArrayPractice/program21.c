// Find minimum and maximum element in array

#include <stdio.h>

void minAndMax(int *arr , int size , int *ret) {
	int min = 0;
	int max = 0;
	for(int i = 1 ; i<size ; i++) {
		if(*(arr+i) < *(arr+min))
			min = i;
		if(*(arr+i) > *(arr+max))
			max = i;
	}

	*(ret+0) = *(arr+min);
	*(ret+1) = *(arr+max);
}


void main() {

	int size;
	printf("Enter size : ");
	scanf("%d",&size);

	int arr[size];
	printf("Enter elements : ");
	for(int i = 0 ; i<size ; i++) {
		scanf("%d",(arr+i));
	}

	int ret[] = {-1,-1};
	// Array where 0th index contains min and 1th contain max initialized -1
	
	minAndMax(arr,size,ret);

	printf("Min : %d\nMax : %d\n",*(ret+0),*(ret+1));
}
