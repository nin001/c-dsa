// Move all negative numbers to one side of the array

#include <stdio.h>

void swap(int *ptr1 , int *ptr2) {
	int temp = *ptr1;
	*ptr1 = *ptr2;
	*ptr2 = temp;
}

void moveNegToSide(int *arr , int size) {
	int itr = 0;
	int negItr = 0;
	while(itr != size) {
		if(negItr == itr && *(arr+itr) < 0) {
			negItr++;
			itr++;
		}else if(*(arr+itr) < 0) {
			swap((arr+itr),(arr+negItr));
			itr++;
			negItr++;
		}else {
			itr++;
		}
	}
}

void printArr(int *arr , int size) {
	for(int i = 0 ; i<size ; i++) 
		printf("%d ",*(arr+i));
	printf("\n");
}

void main() {

	int size;
	printf("Enter size : ");
	scanf("%d",&size);

	int arr[size];
	printf("Enter elements : ");
	for(int i = 0 ; i<size ; i++)
		scanf("%d",(arr+i));

	moveNegToSide(arr,size);
	printArr(arr,size);
}
