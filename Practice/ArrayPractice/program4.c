// take array from user and print even numbers
#include<stdio.h>

void main() {
	int n;
	printf("Enter size :");
	scanf("%d",&n);

	int arr[n];
	printf("Enter array elements\n");
	for(int i = 0 ; i<n ; i++) 
		scanf("%d",(arr+i));

	printf("Even elements present in array are :\n");
	for(int i = 0 ; i<n ; i++) {
		if(*(arr+i)%2 == 0)
			printf("%d ",*(arr+i));
	}
	printf("\n");
}
