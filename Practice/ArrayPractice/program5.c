// take array from user and print max number of the array
#include <stdio.h>

int maxArr(int *ptr , int size) {
	int max = *(ptr+0);
	for(int i = 1 ; i<size ; i++)
		if(*(ptr+i)>max)
			max = *(ptr+i);
	return max;
}

void main() {
	int n;
	printf("Enter size :");
	scanf("%d",&n);

	int arr[n];
	printf("Enter array elements\n");
	for(int i = 0 ; i<n ; i++)
		scanf("%d",(arr+i));

	printf("Max in array : %d\n",maxArr(arr,n));
}
