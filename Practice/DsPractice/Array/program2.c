// reverse in range array

#include <stdio.h>

void reverseRange(int *arr , int n , int a , int b) {
	while(a<b) {
		int temp = *(arr+a);
		*(arr+a) = *(arr+b);
		*(arr+b) = temp;

		a++;
		b--;
	}
}

void main() {
	int n;
	printf("Enter size : ");
	scanf("%d",&n);

	int arr[n];
	printf("Enter array elements : ");
	for(int i = 0 ; i<n ; i++) {
		scanf("%d",(arr+i));
	}

	int a;
	printf("Enter first value for range : ");
	scanf("%d",&a);

	int b;
	printf("Enter second value for range : ");
	scanf("%d",&b);

	if(a<0 || a>=n || b<a || b>=n)
		printf("Invalid range");
	else
		reverseRange(arr,n,a,b);

	for(int i = 0 ; i<n ; i++) 
		printf("%d ",*(arr+i));
	printf("\n");
}
