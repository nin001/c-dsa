// Count of element
// Count the element in array , which has atleast one element greater than itself

#include <stdio.h>

void sort(int arr[] , int n) {
	for(int i = 0 ; i<n-1 ; i++) {
		int min = i;
		for(int j = i+1 ; j<n ; j++) {
			if(arr[min]>arr[j])
				min = j;
		}
		int temp = arr[min];
		arr[min] = arr[i];
		arr[i] = temp;
	}
}

int countElement(int *arr , int n) {
	sort(arr,n);
	int count = 0;
	for(int i = 0 ; i<n-1 ; i++) {
		if(arr[i] != arr[i+1])
			count++;
	}
	return count;
}

void main() {
	int n;
	printf("Enter size : ");
	scanf("%d",&n);

	int arr[n];
	printf("Enter array elements : ");
	for(int i = 0 ; i<n ; i++) {
		scanf("%d",(arr+i));
	}
	
	int ret = countElement(arr,n);
	printf("Count of elements which has atleast 1 element greater than itself : %d\n",ret);

}
