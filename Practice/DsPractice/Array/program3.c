// array rotation
// last element will become first other elements will shift forward
// do this for b times

#include <stdio.h>

void rotateArray(int *arr , int n , int b) {
	if(b>n)
		b = b%n;
	for(int i = 0 ; i<b ; i++) {
		int temp = *(arr+(n-1));
		for(int i = n-2 ; i>=0 ; i--) {
			*(arr+(i+1)) = *(arr+i);
		}
		*(arr+0) = temp;
	}
}


void main() {
	int n;
	printf("Enter size : ");
	scanf("%d",&n);

	int arr[n];
	printf("Enter array elements : ");
	for(int i = 0 ; i<n ; i++) {
		scanf("%d",(arr+i));
	}

	int b;
	printf("How many times to rotate the array : ");
	scanf("%d",&b);

	rotateArray(arr,n,b);
	printf("rotated array : ");
	for(int i = 0 ; i<n ; i++) {
		printf("%d ",*(arr+i));
	}
	printf("\n");
}
