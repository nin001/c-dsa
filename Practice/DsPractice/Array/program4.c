// 2d array
//
// column sum

#include <stdio.h>

void colSum(int row , int col , int (*arr)[row] , int *ret) {

	for(int i = 0 ; i<col ; i++) {
		for(int j = 0 ; j<row ; j++) {
			*(ret+i) += *(*(arr+i)+j);
		}
	}
}

void main() {
	int row;
	printf("Enter row : ");
	scanf("%d",&row);

	int col;
	printf("Enter col : ");
	scanf("%d",&col);

	int arr[row][col];
	printf("Enter array elements :\n");
	for(int i = 0 ; i<row ; i++) {
		printf("Enter elements in row %d\n",i+1);
		for(int j = 0 ; j<col ; j++) {
			scanf("%d",(*(arr+i)+j));
		}
	}

	int ret[col];
	for(int i = 0 ; i<col ; i++) {
		ret[i] = 0;
	}
	colSum(row,col,arr,ret);
	
	for(int i = 0 ; i<col ; i++) {
		printf("Column %d sum : %d\n",(i+1),*(ret+i));
	}

}
