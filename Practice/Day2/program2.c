//WAP to show the concept of void pointer and access data using void pointer 
#include<stdio.h>

void main(){
	int x = 10;
	char ch = 'N';
	float f = 10.65f;
	double d = 5864.547893;

	void *iptr = &x;
	void *cptr = &ch;
	void *fptr = &f;
	void *dptr = &d;

	printf("Accessing data through void pointer\n");
	printf("x = %d\n",*(int*)iptr);
	printf("ch = %f\n",*(float*)fptr);
	printf("f = %c\n",*(char*)cptr);
	printf("d = %lf\n",*(double*)dptr);
}
