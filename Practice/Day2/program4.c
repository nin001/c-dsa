//WAP of every type of pointer 
//
#include<stdio.h>

/*Pointer types 
 * Wild pointer - The pointer which is not given proper address and has garbage value during initialisation
 * null pointer - Pointer which is initialized with NULL value that means pointer pointing to nothing
 * dangling pointer - pointer which is pointing towards something that is gone
 * void pointer - Generic pointer that can store any data type of objects(variables) address 
 * 		- void pointer cannot be dereferenced ,can be dereferenced only after typecasting
 *
*/
int a = 10;
int *dptr = NULL;  //null pointer

int dan(){
	int d = 25;
	printf("%d\n",d);

	dptr = &d;
	printf("%p\n%d\n",dptr,*dptr);
}

void main(){
	int x = 10;
	int *wptr;  //wild pointer
	void *vptr = &x;  //Void pointer
	dan();

	printf("%p\n%d\n",dptr,*dptr); //dan() stack is already popped but dptr still pointing to x
	//pritnf("%d\n",*wptr);    //segmentation fault because of accessing garbage address probably out of process
	
	printf("%d\n",*(int*)vptr);

}
	
	
