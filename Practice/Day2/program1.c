#include<stdio.h>

void main(){
	int x = 10;
	printf("%d\n",x);

	int *ptr1 = &x;
	int *ptr2 = ptr1;
	int **ptr3 = &ptr1;
	int ***ptr4 = &ptr3;

	printf("%d\n%d\n%d\n%d\n",*ptr1,*ptr2,**ptr3,***ptr4);
}
