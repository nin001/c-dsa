//take an array of void pointers and store data of multiple datatype of element(int,char,float)&Print the array;

#include<stdio.h>

void main(){
	int x = 10;
	char ch = 'N';
	float f = 15.35f;
	double d = 78945.6123;

	void *(ptrarr[]) = {&x,&ch,&f,&d};
	
	printf("Array elements\n");
	printf("%d\n",*(int*)ptrarr[0]);
	printf("%c\n",*(char*)ptrarr[1]);
	printf("%f\n",*(float*)ptrarr[2]);
	printf("%lf\n",*(double*)ptrarr[3]);
}
