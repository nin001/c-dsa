//write a function which returns the addition of two numbers and print the ans in main fun
//
#include<stdio.h>

int add(int,int);

void main(){
	int x,y;
	printf("Enter first number : ");
	scanf("%d",&x);
	printf("Enter second number : ");
	scanf("%d",&y);

	printf("Addition : %d\n",add(x,y));
}

int add(int x,int y){
	return x+y;
}
