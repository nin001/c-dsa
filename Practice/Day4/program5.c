//Write a function that returns reverse of the number 

#include<stdio.h>

int rev(int);

void main(){
	int num;
	printf("Enter number : ");
	scanf("%d",&num);

	int revnum = rev(num);

	printf("Reverse Number is %d\n",revnum);
	//printf("Reverse number is %d\n",rev(num)); 
	
}
int rev(int num){
	int rev = 0;
	int rem;
	while(num!=0){
		rem = num%10;
		rev = rev*10+rem;
		num = num/10;
	}
	return rev;
}
		
