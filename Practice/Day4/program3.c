//WAP of swapping two numbers using call by address

#include<stdio.h>

void swap(int*,int*);

void main(){
	int x;
	printf("Enter first number : ");
	scanf("%d",&x);
	int y;
	printf("Enter second number : ");
	scanf("%d",&y);
	printf("Before swapping\n");
	printf("x = %d\ny = %d\n",x,y);
	
	swap(&x,&y);

	printf("After swapping\n");
	printf("x = %d\ny = %d\n",x,y);
}

void swap(int* ptr1,int* ptr2){
	int temp;
	temp = *ptr1;
	*ptr1 = *ptr2;
	*ptr2 = temp;
}
