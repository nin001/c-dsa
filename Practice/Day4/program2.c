//WAP of swapping two numbers using call by value

#include<stdio.h>

void swap(int,int);

void main(){
	int x;
	printf("Enter first number : ");
	scanf("%d",&x);

	int y;
	printf("Enter second number : ");
	scanf("%d",&y);
	printf("Before swapping\n");
	printf("x = %d\ny = %d\n",x,y);

	printf("After swapping\n");
	swap(x,y);


}

void swap(int x,int y){
	int temp = x;
	x = y;
	y = temp;
	printf("x = %d\ny = %d\n",x,y);
}


