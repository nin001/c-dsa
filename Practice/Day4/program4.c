//WAP a function that checks the given number is prime or not

#include<stdio.h>

int checkPrime(int);

void main(){
	int num;
	printf("Enter a number : ");
	scanf("%d",&num);

	int check = checkPrime(num);
	if(check==1)
		printf("%d is a prime number\n",num);
	else
		printf("%d is not a prime number\n",num);
}

int checkPrime(int num){
	if(num>0){
		int count = 0;
		for(int i = 2 ; i<num ; i++){
			if(num%i==0){
				count = 1;
				break;
			}
		}
		if(count==1)
			return 0;
		else
			return 1;
	}else
		return 0;
}

