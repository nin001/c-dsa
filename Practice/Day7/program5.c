// Write user defined mystrupr and mystrlwr functions for strings

#include<stdio.h>

char* mystrlwr(char*);

char* mystrupr(char*);


void main(){
	char str1[20];
	printf("Enter string : ");
	gets(str1);

	mystrlwr(str1);
	printf("Lower string  : ");
	puts(str1);

	mystrupr(str1);
	printf("Upper string : ");
	puts(str1);

}

char* mystrlwr(char* str){
	while(*str!='\0'){
		if(*str>=65 && *str<=90)
			*str = *str + 32;
		
		str++;
	}
	return str;
}

char* mystrupr(char* str){
	while(*str!='\0'){
		if(*str>=97 && *str<=122)
			*str = *str - 32;
		str++;
	}
	return str;
}
