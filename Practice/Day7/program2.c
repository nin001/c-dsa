//WAP which shows the concept of command line argument

#include<stdio.h>

void main(int argc,char*argv[]){
	for(int i = 0 ; i<argc ; i++){
		printf("Argc = %d\n",i+1);
		printf("Argv = %s\n",argv[i]);
	}
}
