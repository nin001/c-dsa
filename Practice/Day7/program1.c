//WAP which shows the example of array or strings and print the array;

#include<stdio.h>
#include<string.h>


void main(){
	int row;
	printf("How many strings you want to enter : ");
	scanf("%d",&row);
	printf("Max string length is 20\n");
	
	char arr[row][20];
	
	for(int i = 0 ; i<row ; i++){
		printf("Enter string : ");
		gets(arr[i]);
	}

	for(int i = 0 ; i<row ; i++){
		puts(arr[i]);
	}
}

