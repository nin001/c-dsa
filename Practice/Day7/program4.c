//WAP in which make array of character pointers storing addresses of
//string and print the strings

#include<stdio.h>

void main(){
	char str1[5] = {"Niraj"};
	char str2[5] = {"Raj"};
	char str3[5] = {"Nin"};

	char *string[3] = {str1,str2,str3};
	for(int i = 0 ; i<3 ; i++){
		puts(string[i]);
		printf("Address : %p\n",string[i]);
	}
}
