//WAP to take two strings from user and check weather anargam or not

#include<stdio.h>

int mystrlen(char* str){
	int count = 0;
	while(*str!='\0'){
		str++;
		count++;
	}
	return count;
}


void main(){
	char str1[20],str2[20];
	printf("Enter first string : ");
	gets(str1);
	printf("Enter second string : ");
	gets(str2);

	if(mystrlen(str1) == mystrlen(str2)){
		int size = mystrlen(str1);
		for(int i = 0 ; i<size ; i++){
			for(int j = 0 ; j<size ; j++){
				if(str1[i] > str1[j]){
					char temp = str1[i];
					str1[i] = str1[j];
					str1[j] = temp;
				}
				
				if(str2[i] > str2[j]){
					char temp = str2[i];
					str2[i] = str2[j];
					str2[j] = temp;
				}

			}
		}
		
		int flag = 1;
		for(int i = 0 ; i<size ; i++){
			if(str1[i]!=str2[i]){
				flag = 0;
				break;
			}
		}
		if(flag == 1)
			printf("Strings are anagram\n");
		else
			printf("Strings are not anagram\n");
	}else
		printf("Strings are not anagram\n");
}



	
