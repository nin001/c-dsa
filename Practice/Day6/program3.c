//WAP to your own string copy function and concate, print copied and concated string

#include<stdio.h>
#include<string.h>

void mystrcpy(char*,char*);

void mystrcat(char*,char*);

void main(){
	char* str1 = "Niraj";
	char str2[15];
 	char str3[25] = {"Programmer "};

	mystrcpy(str2,str1);
	puts(str1);
	puts(str2);	

	mystrcat(str3,str1);
	puts(str1);
	puts(str3);
}	

void mystrcpy(char*dest,char*src){
	while(*src!='\0'){
		*dest = *src;
		dest++;
		src++;
	}
	*dest = '\0';
}

void mystrcat(char*dest,char*src){
	while(*dest!='\0'){
		dest++;
	}
	while(*src!='\0'){
		*dest = *src;
		dest++;
		src++;
	}
	*dest = '\0';
}
