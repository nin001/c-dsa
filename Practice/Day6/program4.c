//Write a function to reverse string and see that reversed string and original string are same or not

#include<stdio.h>
#include<string.h>
void mystrrev(char*);

void main(){
	char str1[20];
	gets(str1);
	char str2[20];
	strcpy(str2,str1);
	mystrrev(str1);	
	printf("Reversed string is : ");
	puts(str1);	
	
	int flag = 1;
	for(int i = 0 ; i<20 ; i++){
		if(str1[i] == '\0')
			break;
		else if(str1[i] != str2[i]){
			flag = 0;
			break;
		}
	}
	
	if(flag == 1)
		printf("Reverse string and original are the same\n");
	else
		printf("Reverse string and original are not the same\n");
}

void mystrrev(char* ptr){
	char* temp = ptr;
	
	while(*temp!='\0'){
		temp++;
	}temp--;
	
	char x;
	while(ptr<temp){
		x = *temp;
		*temp = *ptr;
		*ptr = x;
		temp--;
		ptr++;
	}

}



