// WAP to initialize a character array or string in a multiple way and print
// (from user and hardcoded);
#include<stdio.h>
#include<string.h>

void main(){
	//Method 1 Character array
	char arr1[10] = {'N','i','r','a','j','\0'};  //can give initializer list with seperated character sets but then we have to give '\0' at the end
	char arr2[10] = {"Vishal"};                 //can give initializer list as "string" ,default '\0' at the end
	//from user;
	char arr3[15];
	gets(arr3);
	
	
	//printing 
	
	puts(arr1);
	puts(arr2);
	puts(arr3);
	//	using for loop
	int size = sizeof(arr1)/sizeof(char);
	for(int i = 0 ; i<size ; i++){
		if(arr1[i] == '\0')
			break;
		printf("%c",arr1[i]);
	}
	printf("\n");

	/*Note
	 * Character array is modifiable and mutable
	 * It gets memory in stack
	 * gets and puts are string functions that are used to "get" input from user and "put" display the string 
	 */

	//Method 2 Character pointer 
	char* ptr1 = "NirajRandhir";
	
	puts(ptr1);
		
	/*Note
	 * Character pointer gets space in RO Data that is read only data
	 * Modification or change in RO Data is not allowed
	 */
	

}
