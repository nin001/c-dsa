//WAP to use predefined length function and your own length function for array
//(take array from user)

#include<stdio.h>
#include<string.h>

int mystrlen(char*);

void main(){
	char str1[] = "nirajrandhir1111@gmail.com";

	printf("Using library strlen() function\n");
	printf("Length of character array is %ld\n",strlen(str1));
	printf("Using mystrlen() function\n");
	printf("Length of character array is %d\n",mystrlen(str1));

}

int mystrlen(char* ptr){
	int count = 0;
	while(*ptr!='\0'){
		count++;
		ptr++;
	}
	return count;
}
	
