//Write a function for reverse number, even or odd , and prime number and call function using
//array of functions

#include<stdio.h>

void rev(int);

void evenodd(int);

void prime(int);

void main(){
	void(*ptr[])(int) = {rev,evenodd,prime};

	int x;
	printf("Enter a number : ");
	scanf("%d",&x);
	int size = sizeof(ptr)/sizeof(int*);
	printf("%d\n",size);
	for(int i = 0 ; i<size ; i++){
		ptr[i](x);
	}
}

void rev(int x){
	if(x>9 || x<-9){
		int rev = 0 ;
		int rem;
		while(x!=0){
			rem = x%10;
			rev = rev*10+rem;
			x = x/10;
		}
		printf("Reversed number : %d\n",rev);
	}else{
		printf("Reverse number : %d\n",x);
	}
}

void evenodd(int x){
	if(x%2==0)
		printf("%d is even\n",x);
	else
		printf("%d is odd\n",x);
}

void prime(int x){
	int flag = 0;
	for(int i = 2 ; i<x ; i++){
		if(x%i==0){
			flag = 1;
			break;
		}
	}
	if(flag == 0)
		printf("%d is prime number\n",x);
	else
		printf("%d is not a prime number\n",x);
}




