//WAP to print multiplication of two numbers using function pointer

#include<stdio.h>

int mult(int x,int y);

void main(){
	int x,y;
	printf("Enter x : ");
	scanf("%d",&x);
	printf("Enter y : ");
	scanf("%d",&y);

	int (*ptr)(int,int) = mult;

	printf("Multiplication of %d and %d is %d\n",x,y,ptr(x,y));
}

int mult(int x , int y){
	return x*y;
}

