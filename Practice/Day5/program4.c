//WAP for concept of passing function to function

#include<stdio.h>
//checking given input numbers reverse is prime or not
int prime(int (*ptr)(int));

int rev(int);

void main(){
	prime(rev);

}

int rev(int x){
	int rev = 0;
	int rem;
	while(x!=0){
		rem = x%10;
		rev = rev*10+rem;
		x = x/10;
	}
	return rev;
}

int prime(int (*ptr)(int)){
	
	int num;
	printf("Enter number : ");
	scanf("%d",&num);

	num = ptr(num);

	printf("Reverse number is %d\n",num);

	int flag = 0;
	for(int i = 2 ; i<num ; i++){
		if(num%i == 0){
			flag = 1;
			break;
		}
	}
	if(flag == 0)
		printf("Reverse number is prime\n");
	else
		printf("Reverse number is not prime\n");
}
