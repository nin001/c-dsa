// WAP to take number from the user in retVal() function and return the first and the last digit
// of that number in main function

#include<stdio.h>

void retVal(int* fptr,int* lptr){
	int input;
	printf("Enter Number : ");
	scanf("%d",&input);

	*lptr = input%10;

	int rem;
	while(input!=0){
		*fptr = input%10;
		input/=10;
	}
}

void main(){
	int lastdig,firstdig;
	retVal(&firstdig,&lastdig);

	printf("First digit of input is %d and last digit of input is %d\n",firstdig,lastdig);

}
