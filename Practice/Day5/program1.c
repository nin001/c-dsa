//Wap to take size and elements from user in main function and print even elements in evenArr()
//passing array to function
//
#include<stdio.h>

void evenArr(int*ptr , int size);

void main(){
	int size;
	printf("Enter array size : ");
	scanf("%d",&size);

	int arr[size];
	printf("Enter array elements :\n");
	for(int i = 0 ; i<size ; i++){
		scanf("%d",&arr[i]);
	}
	size = sizeof(arr)/sizeof(int);

	evenArr(arr,size);

}

void evenArr(int *ptr , int size){
	printf("Even elements in array :\n");
	for(int i = 0 ; i<size ; i++){
		if(*(ptr+i)%2==0){
			printf("%d\n",*(ptr+i));
		}
	}
}
