//5.WAP to take array from user and print second largest number
//
#include<stdio.h>

void main(){
	int size;
	printf("Enter array size : ");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array elements :\n");
	for(int i = 0 ; i<size ; i++){
		scanf("%d",&arr[i]);
	}

	for(int i = 0 ; i<size ; i++){
		for(int j = 0 ; j<size ; j++){
			if(arr[i]<arr[j]){
				int temp;
				temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
			}
		}
	}
	
	printf("Second largest element in array is %d\n",arr[size-2]);
}

