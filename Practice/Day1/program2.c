//2.WAP to create 2D array of size given from user,take values from user
//assign that array to another array and print second array
//
#include<stdio.h>

void main(){
	int row,col;

	printf("Enter row size of array : ");
	scanf("%d",&row);

	printf("Enter col size of array : ");
	scanf("%d",&col);

	int arr[row][col];
	printf("Enter array elements :\n");
	for(int i = 0 ; i<row ; i++){
		for(int j = 0 ; j<col ; j++){
			scanf("%d",&arr[i][j]);
		}
	}
	printf("Assigning array to another array...");
	int arr2[row][col];
	for(int i = 0 ; i<row ; i++){
		for(int j = 0 ; j<col ; j++){
			arr2[i][j] = arr[i][j];
		}
	}
	printf("Elements of second array :\n");
	for(int i = 0 ; i<row ; i++){
		for(int j = 0 ; j<col ; j++){
			printf("%d\n",arr2[i][j]);
		}
	}
}
