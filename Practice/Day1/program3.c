//3.WAp to create a 3D array from user and print the sum of left diagonal elements

#include<stdio.h>

void main(){
	int plane,row,col;
	int sum = 0;
	printf("Enter number of planes in array : ");
	scanf("%d",&plane);
	printf("Enter number of rows in array : ");
	scanf("%d",&row);
	printf("Enter number of cols in array : ");
	scanf("%d",&col);

	int arr[plane][row][col];
	printf("Enter array elements :\n");
	for(int i = 0 ; i<plane ; i++){
		for(int j = 0 ; j<row ; j++){
			for(int k = 0 ; k<col ; k++){
				scanf("%d",&arr[i][j][k]);
				if(j==k)
					sum = sum + arr[i][j][k];
			}
		}
	}
	printf("Sum of diagonal elements is %d\n",sum);
}
	
