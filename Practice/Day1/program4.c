//4.WAP to compare 2 arrays and print "Equal" if same and "Not Equal" if not same
//
#include<stdio.h>

void main(){
	int s1,s2;

	printf("Enter 1st array size : ");
	scanf("%d",&s1);

	printf("Enter 2nd array size : ");
	scanf("%d",&s2);

	int arr1[s1];
	int arr2[s2];

	printf("Enter Array1 elements :\n");
	for(int i = 0 ; i<s1 ; i++){
		scanf("%d",&arr1[i]);
	}
	printf("Enter Array2 elements :\n");
	for(int i = 0 ; i<s2 ; i++){
		scanf("%d",&arr2[i]);
	}
	
	if(s1==s2){
		int flag = 0;
		for(int i = 0 ; i<s1 ; i++){
			if(arr1[i]==arr2[i]){
				flag = 1;
			}else
				flag = 0;
		}
		if(flag == 1)
			printf("Equal\n");
		else
			printf("Not Equal\n");
	}else
		printf("Not Equal\n");
}

