#include<stdio.h>

void main(){
	/*
	 * If statements 
	 * syntax:
	 * if(boolean value){
	 * }
	 * If statements are used as a condition check if the boolean value of the
	 * condition is 0 then if statement is skipped and other than 0 is considered
	 * as true (most probably 1)
	 */
	char user_char;
	printf("Enter the value of character : ");
	scanf("%c",&user_char);
	if (user_char>='A'&&user_char<='Z'){
		printf("You Entered a Capital letter \n");
	}
	if (user_char>='a'&&user_char<='z'){
		printf("You Entered a small letter \n");
	}
}


