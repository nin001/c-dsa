#include<stdio.h>

void main(){
	/*Switch is the alternative method which is used instead of if else if ladder
	 * when coding large numbers of conditions using if else ladder it becomes out of control
	 * for a coder to code in it as the number of conditions increase it becomes difficult and
	 * difficult to manage
	 *
	 * syntax :
	 * switch("integer and character"){   ----in real there is only integer value character 
	 * 	case "condition":                 value is converted in ascii integer value
	 * 		do this
	 * 	and further cases
	 * 	default:
	 * 		"else"
	 * 		it is the else of the switch 
	 * 		if none of the cases matches then block under default is executed
	 * }
	 *the values that are inserted in the switch as parameter are only int values the character
	 value are converted to its ascii value
	 
	 there are certain cases in switch
	 1) taking character as input by user
	 in this it takes the character as a character and doesnt convert in its ascii values
	 2) taking character as parameter
	 in this it takes the character value and converts it into its ascii value
	 */
	int num;
	printf("Enter the number : ");
	scanf("%d",&num);
	switch(num){
		case 2:
			printf("2\n");
			break;
		case 5:
			printf("5\n");
			break;
		case 7:
			printf("7\n");
			break;
		default:
			printf("wrong input\n");
			
	}
}	

