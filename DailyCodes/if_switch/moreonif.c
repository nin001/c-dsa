#include<stdio.h>

void main(){
	/*
	 * We have mistaken "if" as it takes a condition but reality is it needs a boolean value that is a true value of a false value 
	 *false value in boolean is 0 as we know and true value in boolean anything which is not a zero 
	point to note is that if takes the char values as well and takes is ascii value as a number that is if we pass a it takes as97*/
	// first block of condition (passing a int)
	int a = 0;
	int b = 48;
	if(a){
		printf("In the first if block\n");
	}
	if(b){
		printf("In the second if block\n");
	}
	if(a&&b){
		printf("In the third if block\n");
	}
	if(a||b){
		printf("In the fourth if block\n");
	}
	// second block of condition
	// third block of condition
} 
