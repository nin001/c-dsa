//Subtraction of pointers
//Note : Subtraction of two pointer can be done in c
//reason : As pointers consist address of variable and when we subtract
//the address will be within the process(most probably)
#include<stdio.h>

void main(){
	/*	NOTE
	 Subtraction formula : ptr1-ptr2 = (ptr1 - ptr2)/size of data type of pointer
	 Pointer subtraction is meaningfull for array it gives that one element is how many houses far from another element*/
	int arr[] = {1,2,3,4,5};
	int *ptr1 = &(arr[0]);
	int *ptr2 = &(arr[3]);

	printf("%d\n",ptr2-ptr1);
	printf("%d\n",ptr1-ptr2);
}
