//take input from user make 2D array and print using pointer
#include<stdio.h>

void main(){
	int row,col;
	printf("Enter number of rows in 2D arry : ");
	scanf("%d",&row);
	printf("Enter number of col in 2D array : ");
	scanf("%d",&col);
	int arr[row][col];
	printf("Enter elements in 2D array :\n");
	for(int i = 0 ; i<row ; i++){
		for(int j = 0 ; j<col ; j++){
			scanf("%d",&arr[i][j]);
		}
	}
	printf("Array elements are :\n" );
	for(int i = 0 ; i<row ; i++){
		for(int j = 0 ; j<col ; j++){
			printf("%d\t",*(*(arr+i)+j));
		}
		printf("\n");
	}
}

