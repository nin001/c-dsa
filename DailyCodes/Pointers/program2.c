//Pointers 
//  Pointers are variables that can store address of any variable function etc
//  There are two important operators in pointers "&" Address of operators and also called referencing operator and "*" value at and also called dereferencing operator
#include<stdio.h>

void main(){
	int var;
	printf("Enter Number : ");
	scanf("%d",&var);
	
	int *ptr = &var;
	printf("Accessing values and address by using pointer...\n");
	printf("Address of variable : %p\n",ptr);
	printf("Value of variable : %d\n",*ptr);
}
