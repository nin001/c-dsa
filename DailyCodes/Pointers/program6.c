//Addition of pointers and integer
#include<stdio.h>

void main(){
	/*	NOTE
	 Pointers addition with integer can be done 
formula : ptr+1 = ptr(address) + 1*(Size of Data type of pointer )
		  100+1*(4)->integer pointer with data type size 4 bytes
		  104*/
	int x = 10;
	int *ptr = &x;

	printf("%d\n",*(ptr+1));
	printf("%p\n",ptr+1);
}

