// Void pointer
// Defination : A pointer which can store any type of variables addresses 
// 		It cannot be dereferenced 
// 		Void pointer can be used for any data type variables int char float or double
// 		To dereference the void pointer we need to type cast it first that means that it is no longer void pointer
//		It is also called as Generic Pointer And is USED IN A VERY IMPORTANT OS FUNCTION MALLOC
#include<stdio.h>

void main(){
	char ch = 'a';
	int x = 10;
	float f = 1.5;
	double d = 5.2645687;
	void *vptr1 = &ch;
	void *vptr2 = &x;
	void *vptr3 = &f;
	void *vptr4 = &d;
	printf("Addresses by void pointer without warning\n");
	printf("%p\n",vptr1);
	printf("%p\n",vptr2);
	printf("%p\n",vptr3);
	printf("%p\n",vptr4);

	printf("Accessing values of variables from typecasting\n");	//type casting can be done by putting *("datatype" *) before void ptr
	printf("%c\n",*(char *)vptr1);
	printf("%d\n",*(int *)vptr2);
	printf("%f\n",*(float *)vptr3);
	printf("%lf\n",*(double *)vptr4);
	
}
