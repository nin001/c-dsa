//Addition of pointers 
//NOTE : Two pointers cannot be added 
//reason : Because pointers consists addresses of the variables and if we do
//addition operation on pointers there is 0% possibility that the address after
//addition is out of our process and accessing that address will kill our process
//this is the reason addition of pointers in not allowed
#include<stdio.h>

void main(){
	int x = 10;
	int y = 20;
	int *ptr1 = &x;
	int *ptr2 = &y;

	//printf("Addition of two pointers : %p\n",ptr1+ptr2);    //invalid operand with binary + (int* and int*)
	printf("Addition after dereferencing : %d\n",*ptr1+*ptr2); //this gives addition of values of x and y (dereferencing)
}
