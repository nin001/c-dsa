//multidimensional array 
//arr[row][col] = {initialization list};
#include<stdio.h>

void main(){
	int arr[3][3] = {1,2,3,4,5,6,7,8,9};
	//here in multidimentional array other than first field all are mandatory
	for(int i = 0 ; i<3 ; i++){
		for(int j = 0 ; j<3 ; j++){
			printf("%d\t",*(*(arr+i)+j));
		}
		printf("\n");
	}
	printf("\n");
	for(int i = 0 ; i<9 ; i++){
		printf("%d\n",arr[i]);
	}
	/*Relation between pointer and 2d array
	  arr[i][j] = *(*(arr+i)+j)
	  	    = *(*(arr+(i*size of array))+j*(size of data type of pointer))
		    this is how arr[][] goes internally

 	*/
	/*some points to note in 2d array

	 *arr[row][col] the subscripts represent row and col as given 
	 in which col field is mandatory which we cannot left empty

	 *arr[i][j] internally goes as pointer as follows *(*(arr + i*size of array) + j*size of DTP)
	 
	 *in 2D array arr[1] represents first rows address arr[2] represents second rows address and as follows this address is of whole rows address
	 
	 * In 2d array  */
}
