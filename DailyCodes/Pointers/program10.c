// Wild pointer and null pointer 
//Wild pointer : It is a type of pointer which is just declared and has no inisialization and no address is in it 
//Ha pointer kona kadech pahat nasto tr haa harmful ahe hela access kartana segmentation fault yenar hechi 99.99% gaurentee with warentee mi deto
//Null Pointer : It is a pointer which is initialized with a null or 0 value accessing null pointer also gives segmentation fault 
//Null pointer is used to be sure that out pointer is not pointing towards any unnecessary and inappropriate variables or addresses
#include<stdio.h>

void main(){
	int x = 10;
	int *ptr;	//Wild pointer
	printf("%p\n",ptr);	//Address	//nil 
	//printf("%d\n",*ptr);	//Accessing value	//segmentation fault(core dumped)
	int *nPtr = 0;
	int *nPtr2 = NULL;	//two ways to inisialize Null pointer
        printf("%p %p\n",nPtr,nPtr2);	//segmentation fault for both statements
	printf("%d %d\n",*nPtr,*nPtr2);
}
