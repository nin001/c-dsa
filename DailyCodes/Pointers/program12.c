//Relationship between array and pointer
/*There is one relation which shows that array internally goes as pointer
 * arr[1] in array gives the element present at 1 index of array "arr"
 * internally it goes like this:
 * 	arr[1] = *(arr + 1*size of DTP(array))
 * 		=*("address of first element" + 1*4)->(let array be of int type)
 * 		=*("address of first element" + 4);
 * 		=*(goes one box forward)
 * 		
 *	*/
#include<stdio.h>

void main(){
	int size;
	printf("Enter Array size : ");
	scanf("%d",&size);
	int arr[size];
	for(int i = 0 ; i<size ; i++){
		printf("Enter elements :");
		scanf("%d",&arr[i]);
	}
	printf("Array elements :\n");
	for(int i = 0 ; i<size ; i++){
		printf("%d\n",arr[i]);
	}
	printf("Array elements using pointers :\n");
	for(int i = 0 ; i<size ; i++){
		printf("%d\n",*( arr+i ));
	}
}
