// Array of pointers 
// This in its name itself gives a defination
// That a array which has elements that are of pointer type
// we can write this types of pointers like this
// int* arr[3] = {initializer list};
// type    name with 
// of ptrs  subscripts	
#include<stdio.h>

void main(){
	int x = 10;
	int y = 20;
	int z = 30;
	
	int* ptr1 = &x;
	int* ptr2 = &y;
	int* ptr3 = &z;
	
	int* arr1[3] = {&x,&y,&z}; // this is a way of initialising array of pointers which has direct addresses 

	int* arr2[3] = {ptr1,ptr2,ptr3}; //in this we gave pointers as a element in initializer list
	printf("%ld\n",sizeof(arr1)); 
 	printf("%ld\n",sizeof(arr2)); //if we construct a array of pointers each element gets 8 byte of space in main stack(ram)

	//accessing x , y , z with help of array of pointer
	for(int i = 0 ; i<3 ; i++){
		printf("%d\n",*(arr2+i));
	}
	//printing the addresses where the x , y , z is stored
	
	for(int i = 0 ; i<3 ; i++){
		printf("%d\n",*(arr2+i));
	for(int i = 0 ; i<3 ; i++){
		printf("%d\n",*(arr2+i));
	}
	//printing the addresses where the x , y , z is stored
	}
	//printing the addresses where the x , y , z is stored
}
