#include<stdio.h>

void main(){
	int x = 10;
	printf("%d\n",x);
	printf("%p\n",&x);
	/*Pointers are the variables which stores the address of anyone in it 
	 * it gets 8 bytes of space in ram in process 
	 * pointer should be declared same as data type as the address of variable we are storing
	 * pointer when declared before its name * is present
	 * and we assign it addresses of variables  to pointers with "&" address of operator
	 * to print the address we direcly print the ptr without "&" with format specifier %p*/
	int *ptr = &x;
	printf("%p\n",ptr);

}
