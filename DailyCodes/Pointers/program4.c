//to make a pointer of variable we must make the pointer of same data type of varibale
#include<stdio.h>

void main(){
	int x = 10;
	char ch = 'a';
	double d = 25.5555555;
	float f = 5.5;

	int *iptr = &x;
	char *cptr = &ch;
	double *dptr = &d;
	float *fptr = &f;
	printf("Values accessing from variables : ");
	printf("%d %c %lf %f\n",x,ch,d,f);
	printf("Addresses of variable in pointers : \n" );
	printf("x : %p\nch : %p\nd : %p\nf : %p\n",iptr,cptr,dptr,fptr);
}
