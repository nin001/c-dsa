//Dangling pointer 
//Dangling pointer is a pointer which has the address of the variable which is been vanished after the stackframe is popped
//It is declared as a global variable(Accessible to every one) 
#include<stdio.h>
int a = 10;
int b;
int *ptr = 0;

void user(){
	int x = 30;
	printf("IN USER \n");
	printf("%d\n",a);
	printf("%d\n",b);

	ptr = &x;
	printf("%p\n",ptr);
	printf("%d\n",*ptr);
	printf("OUT USER\n");
}


void main(){
	printf("In Main Function\n");
	printf("%d\n",a);
	printf("%d\n",b);
	user();
	
	printf("%p\n",ptr);	//this address and address of x will be same 
	printf("%d\n",*ptr);	//this will give output the value of x
	/*Explaination
	 As we saw that user stackframe was popped after its function was over still ptr is
	 looking towards that address and also accessing its value(x)
	 This is the part1 concept of dangling pointer*/

}
