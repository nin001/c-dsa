// code

#include<stdio.h>

int x = 5;

void fun(int x){
	printf("Hello\n");
	if(x!=1)
		fun(--x);
	printf("Bye\n");
}

void main(){
	fun(x);
}

/* output
Hello
Hello
Hello
Hello
Hello
Bye
Bye
Bye
Bye
Bye
*/
