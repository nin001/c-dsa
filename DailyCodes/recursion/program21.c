// palindrome string find using recursion
#include<stdio.h>
#include<stdbool.h>

bool isPalindrome(char *carr,int start,int end){
	if(start<end){
		if(*(carr+start) == *(carr+end))
			return isPalindrome(carr,start+1,end-1);
		else
			return false;
	}else{
		return true;
	}
}

void main(){
	int size;
	printf("Enter size : ");
	scanf("%d",&size);
getchar();
	char carr[size];
	fgets(carr,size,stdin);

	if(isPalindrome(carr,0,size-1)){
		printf("Palindrome string\n");
	}else{
		printf("not Palindrome\n");
	}
}
