// n compare with recursion
#include<stdio.h>
#include<stdbool.h>
int mystrlen(char *cptr){
        int count = 0;
        while(*cptr != '\0'){
                count++;
                cptr++;
        }
        return count;
}

bool mystrncmp(char *cptr1 , char *cptr2,int n){
        if(n==0)
                return true;

        if(*cptr1==*cptr2 && mystrncmp(++cptr1,++cptr2,--n)){
                return true;
        }else{
                return false;
        }
}

void main(){
        char carr1[20],carr2[20];

        printf("Enter string: ");
        fgets(carr1,15,stdin);

        printf("Enter sub string: ");
        fgets(carr2,15,stdin);

	int n;
	printf("Enter value of n : ");
	scanf("%d",&n);

	if(mystrlen(carr2)<mystrlen(carr1)){
	        if(mystrncmp(carr1,carr2,n)){
			printf("Substrig found\n");
	       	}else{
	   		printf("Subsring not found\n");
		}
	}else{
		printf("Substring greater than string\n");
	}
}
