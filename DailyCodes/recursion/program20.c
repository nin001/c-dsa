// search elements in character array using recursion
#include<stdio.h>

int searchEle(char *str , char ch , int size){
	if(size == 0 )
		return -1;

	if(*(str+(size-1)) == ch){
		return size-1;
	}

	return searchEle(str,ch,size-1);
}
	

void main(){
	char str[] = "Niraj";
	printf("%s\n",str);

	char ch;
	printf("Enter element to search : ");
	scanf("%c",&ch);

	int index = searchEle(str,ch,5);

	if(index != -1){
		printf("Found at index : %d\n",index);
	}else{
		printf("Not found\n");
	}
}
