// count step program 
// given a number 
// if even
//   divide it by 2 
//   and the number found after operation is the new number
// if odd 
//   subtract by 1
// 
// do this untill found 0
// total steps to reach to 0 is the output
#include<stdio.h>

int countStep(int num){
	int step = 0;
	while(num){
		if(num%2 == 0){
			num /= 2;
		}else{
			num--;
		}
		step++;
	}
	return step;
}

void main(){
	int num;
	printf("Enter number : ");
	scanf("%d",&num);

	printf("Number of steps : %d\n",countStep(num));
}
