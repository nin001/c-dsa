// stack count when there exist global variable declaration (no parameters)
#include<stdio.h>

int x = 5;

void fun(){
	printf("%d\n",x);
	fun(--x);
}

void main(){
	fun();
}

