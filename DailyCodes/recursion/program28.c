// array
// if 2 even in array return true
// else
// false
#include<stdio.h>
#include<stdbool.h>

int count = 0;

bool isVerified(int *ptr , int size){
	if(size < 0){
		return false;
	}

	if(*(ptr+(size-1))%2 == 0)
		count++;

	if(count == 2){
		return true;
	}
	return isVerified(ptr,--size);
}

void main(){

	int size;
	printf("Enter size : ");
	scanf("%d",&size);

	int arr[size];
	
	printf("Enter array elements :\n");

	for(int i = 0 ; i<size ; i++)
		scanf("%d",(arr+i));


	if(isVerified(arr,size)){
		printf("verified array\n");
	}else{
		printf("Not verified array\n");
	}
}
