// count zero 
// Taken int input from user and returning the total number of zeros in it
#include<stdio.h>

int countZero(int n){
	if(n == 0)
		return 1;
	int count = 0;
	while(n){
		if(n%10 == 0)
			count++;
		n = n/10;
	}
	return count;
}

void main(){
	int num;
	printf("Enter number : ");
	scanf("%d",&num);

	printf("NUMBER OF ZEROS : %d\n",countZero(num));
}
