// types of recursion
// direct recursion - recursion in which a function calls itself 
// indirect recursion - recursion in which a function calls other function and the other function calls the one who called (to form a cycle)
// 			is indirect function
//
// tail function - a recursion where a recursive call is the last instruction
// non tail function - a recurstio where a recursive call is not a last call and some intruction or operation is left is non tail recursion

// tail recursion
#include<stdio.h>

int fun(int x){
	if(x == 1)
		return 1;
	return fun(--x);	// last operation
}

void main(){
	printf("%d\n",fun(3));
}
