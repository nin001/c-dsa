// rev no using recursion
#include<stdio.h>

int rev(int n , int var){
	if(n == 0){
		return var;
	}

	var = var*10+n%10;

	return rev(n/10,var);
}

	

void main(){
	int num;
	printf("Enter number : ");
	scanf("%d",&num);

	printf("Reversed number : %d\n",rev(num,0));
}
