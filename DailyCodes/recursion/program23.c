// countzero using recursion
#include<stdio.h>
 
int countZero(int num){

	static int count = 0;
	if(num == 0)
		return 0;
	if(num%10 == 0)
		return 1+countZero(num/10);

	return countZero(num/10);
}	

void main(){
	int num;
	printf("Enter number : ");
	scanf("%d",&num);

	if(num != 0){
		printf("NUMBER OF ZEROS : %d\n",countZero(num));
	}else{
		printf("NUMBER OF ZEROS : %d\n",1);
	}

}
