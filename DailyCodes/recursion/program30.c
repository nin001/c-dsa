// compare character array using recursion
#include<stdio.h>
#include<stdbool.h>

int mystrlen(char *cptr){
	int count = 0;
	while(*cptr != '\0'){
		count++;
		cptr++;
	}
	return count;
}

bool mystrcmp(char *cptr1 , char *cptr2){
	if(*cptr1 == '\0')
		return true;

	if(*cptr1==*cptr2 && mystrcmp(++cptr1,++cptr2)){
		return true;
	}else{
		return false;
	}
}

void main(){
	char carr1[20],carr2[20];

	printf("Enter string 1 : ");
	fgets(carr1,15,stdin);
	
	printf("Enter string 2 : ");
	fgets(carr2,15,stdin);

	if(mystrlen(carr1) == mystrlen(carr2)){
		if(mystrcmp(carr1,carr2)){
			printf("Same\n");
		}else{
			printf("not same\n");
		}

	}else{
		printf("Not same\n");
	}
}
