// reverse a number 
#include<stdio.h>

int revN(int n){
	int var = 0;
	while(n){
		var = var*10+n%10;
		n /=10;
	}
	return var;
}

void main(){
	int num;
	printf("Enter number to reverse : ");
	scanf("%d",&num);

	printf("Reversed number is : %d\n",revN(num));
}
