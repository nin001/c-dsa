// Print array elements using recursion

#include<stdio.h>


void printArray(int arr[],int size);

void main(){
	int arr[] = {10,20,30,40,50};

	printArray(arr,5);

}

void printArray(int arr[],int size){
	static int x = 0;

	printf("%d ",*(arr+x));
	if(size != 1){
		x++;
		printArray(arr,--size);
	}else{
		printf("\n");
	}
}
		
