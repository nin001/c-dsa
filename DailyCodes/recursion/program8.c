// Factorial using recursion (without return)
#include<stdio.h>

int factorial(int x){
	static int fact = 1;

	fact = fact*x;
	if(x!=1)
		factorial(--x);
	else
		printf("Factorial : %d\n",fact);
}

void main(){
	int num;
	printf("Enter positive number : ");
	scanf("%d",&num);

	factorial(num);

}
