// fibonacci series 
// input  0 1 2 3 4 5 ...
// series 0 1 1 2 3 5 ...

//Without recursion
#include<stdio.h>

int fibo(int n){
	int prev1 = 0;
	int prev2 = 1;
	for(int i = 1 ; i < n ; i++){
		int temp = prev1;
		prev1 = prev2;
		prev2 = temp + prev1;
	}
	if(n == 0)
		return prev1;
	return prev2;
}

void main(){
	int num;
	printf("enter number to find value of fibonacci series : ");
	scanf("%d",&num);

	int ret = fibo(num);

	printf("Output : %d\n",ret);
}
