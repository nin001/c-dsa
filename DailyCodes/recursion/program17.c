// fibonacci series using recursion
#include<stdio.h>

int fibo(int n){
	if(n==1)
		return 1;
	if(n==0)
		return 0;

	return fibo(n-1)+fibo(n-2);
}

void main(){
	int num;
	printf("Enter number to find the finonacci series : ");
	scanf("%d",&num);

	int ret = fibo(num);

	printf("Output : %d\n",ret);
}

