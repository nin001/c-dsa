// factorial in recursion
#include<stdio.h>

int fact(int num){
	if(num <= 1){
		return 1;
	}

	return fact(num-1)*num;
}

void main(){
	int num;
	printf("Enter number : ");
	scanf("%d",&num);

	printf("Factorial : %d\n",fact(num));

}
