// character array recursion
// base code

// search element in character array without recursion

#include<stdio.h>

int searchEle(char *carr , char ch , int size){
	for(int i = 0 ; i<size ; i++){
		if(*(carr+i) == ch){
			return i;
		}
	}
	return -1;
}

void main(){
	char carr[5] = "Niraj";
	
	printf("%s\n",carr);
	char ch;
	printf("Enter element to search : ");
	scanf("%c",&ch);

	int index = searchEle(carr,ch,5);
	if(index != -1){
		printf("Found at Index : %d\n",index);
	}else{
		printf("Not found\n");
	}
}
