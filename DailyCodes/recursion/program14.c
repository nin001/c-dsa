// multiple recursion 
// multiple recursion in one return line the solving or evaluation is done from the left side

#include<stdio.h>

/*void*/int fun(int n){
	if(n<=1)
		return 1;
	int x = fun(n-1);
	int y = fun(n-2);

	printf("%d %d\n",x,y);
}

void main(){
	fun(4);
}
