// muliple recursion practice code
#include<stdio.h>

int fun(int n){
	if(n<=1)
		return 1;
	return n+fun(n-1)+fun(n-2);
}

void main(){
	printf("Return value of fun() : %d\n",fun(4));
}
