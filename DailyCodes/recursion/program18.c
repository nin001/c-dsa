// recursion in array
// sumofArray using recursion
#include<stdio.h>

int sumArray(int *arr, int size){

	if(size == 1)
		return arr[size-1];

	return sumArray(arr,size-1)+arr[size-1];	
}

void main(){
	int size;
	printf("Enter Array size  : ");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array elements : \n");
	for(int i = 0 ; i<size ; i++)
		scanf("%d",(arr+i));

	int ret = sumArray(arr,size);

	printf("Sum of array is %d\n",ret);
}

