// check if array sorted
// if yes return true
// else false
#include<stdio.h>
#include<stdbool.h>

bool isVer(int *ptr , int size){
	if(size == 1)
		return true;

	if(*(ptr+(size-1))>*(ptr+(size-2)) && isVer(ptr,--size)){
		return true;
	}else{
		return false;
	}
}

void main(){
	int size;
	printf("Enter size : ");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array elements :\n");
	for(int i = 0 ; i<size ; i++)
		scanf("%d",(arr+i));

	if(isVer(arr,size)){
		printf("Sorted array\n");
	}else{
		printf("Not sorted array\n");
	}
}
