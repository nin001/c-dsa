// print 10 to 1 with recursion
#include<stdio.h>

void fun(int x){
	printf("%d\n",x);
	if(x!=1)
		fun(--x);
}

void main(){
	unsigned int num;
	printf("Enter number : ");
	scanf("%d",&num);
	fun(num);
}
