// sum of n natural numbers using recursion
#include<stdio.h>

int sumN(int n){
	static int sum = 0;
	if(n>0){
		sum = sum + n;
		sumN(--n);
	}
	return sum;
}

void main(){
	int num;
	printf("Enter value of N : ");
	scanf("%d",&num);

	int ret = sumN(num);

	printf("Sum of first %d numbers is %d\n",num,ret);
}
