// sum of user input till 1
#include<stdio.h>

void fun(int x){

	static int sum = 0;
	sum = sum + x;	
	if(x != 1){
		fun(--x);
	}else{
		printf("Sum : %d\n",sum);
	}
}

void main(){
	int num;
	printf("Enter positive number : ");
	scanf("%d",&num);

	fun(num);
}
