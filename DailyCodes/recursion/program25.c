// count step problem using recursion
#include<stdio.h>

int countStep(int num){
	if(num == 0)
		return 0;
	if(num%2 == 0)
		return 1+countStep(num/2);
	else
		return 1+countStep(--num);
}

void main(){
	int num;
	printf("Enter number : ");
	scanf("%d",&num);

	printf("Number of steps : %d\n",countStep(num));
}
