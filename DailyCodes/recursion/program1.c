/*
 * Recursion
 * - Recursion is a function calling a function calling itself within the function 
 *   
 * Two types if recursion 
 * tell recursion
 *  - this is a recursion where the call to a function is a last instruction
 * non-tell recursion
 *  - this is a recursion where there exist some instructions after recursive call
 */

#include<stdio.h>

void fun(){
	static int x = 5;
	printf("%d\n",x);
	fun(--x);
}

void main(){
	fun();
}


