// Factorial using recursion
#include<stdio.h>

int factorial(int x){
	static int fact = 1;

	fact = fact*x;
	if(x!=1)
		factorial(--x);
	return fact;
}

void main(){
	int num;
	printf("Enter positive number : ");
	scanf("%d",&num);

	printf("Factorial of %d is %d\n",num,factorial(num));

}
