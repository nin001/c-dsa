//take size assign elements in array and search element
#include<stdio.h>

void main(){
	int size;
	printf("Enter array size : ");
	scanf("%d",&size);
	int flag = 0;
	int search;
	int arr[size];
	
	for(int i = 0 ; i<size ; i++){
		printf("Enter element : ");
		scanf("%d",&arr[i]);
	}
	
	printf("Enter element to be searched : ");
	scanf("%d",&search);
	for(int i = 0 ; i<size ; i++){
		if(search == arr[i]){
			flag = 1;
		}
	}
	if(flag == 1){
		printf("Element found\n");
	}else{
		printf("Element not found\n");
	}

}
