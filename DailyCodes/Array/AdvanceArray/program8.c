// sum of all subarray
#include<stdio.h>

int subarray(int *arr , int size){
	int sum = 0;
	for(int i = 0 ; i<size ; i++)
		for(int j = i ; j<size ; j++)
			sum+=arr[j];
	return sum;
}


void main(){
	int size;
	printf("Enter size : ");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array elements\n");
	for(int i = 0 ; i<size ; i++){
		scanf("%d",&arr[i]);
	}

	int sum = subarray(arr,size);
	
	printf("sum : %d\n",sum);

}
