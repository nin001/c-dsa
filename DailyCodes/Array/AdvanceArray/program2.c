// Carry forward algorithm
// Left Max
// it is a array where we insert the element which is largest in left side (subarray)

//Brute force approch
#include<stdio.h>

int* leftMaxFun(int arr[] , int size , int leftMax[]){
	leftMax[0] = arr[0];

	for(int i = 1 ; i<size ; i++){
		int lmax = arr[i];
		for(int j = 0 ; j<i ; j++){
			if(lmax<arr[j])
				lmax = arr[j];
		}
		leftMax[i] = lmax;
	}

	return leftMax;
}

void main(){
        int size;
        printf("Enter size : ");
        scanf("%d",&size);

        int arr[size];
        printf("Enter array elements\n");
        for(int i = 0 ; i<size ; i++){
                scanf("%d",(arr+i));
        }

	int leftMax[size];

	leftMaxFun(arr,size,leftMax);

	printf("leftMax array\n");
	for(int i = 0 ; i<size ; i++)
		printf("%d ",leftMax[i]);
	printf("\n");

}

