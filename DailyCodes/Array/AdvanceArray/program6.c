// subarray problem 
// print the sum of every single subarray
#include<stdio.h>

void subarraySum(int arr[] , int size){
	for(int i = 0 ; i<size ; i++){
		int sum = 0;
		for(int j = i ; j<size ; j++){
			sum += arr[j];
			printf("%d\n",sum);
		}
		printf("\n");

	}
}

void main(){
	int size;
	printf("Enter array size : ");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array elements\n");
	for(int i = 0 ; i<size ; i++){
		scanf("%d",&arr[i]);
	}

	subarraySum(arr,size);
}
