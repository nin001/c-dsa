// kadanes algorithm 
// This algorithm is used to calculate maximum sum of subarray

#include<stdio.h>

int kadanesAlgo(int *arr , int size){
	int max = arr[0];
	int sum = 0;
	for(int i = 0 ; i<size ; i++){
		sum += arr[i];

		if(sum<0)
			sum = 0;

		if(max<sum)
			max = sum;
	}

	return max;
}

void main(){
	int size;
	printf("Enter array size : ");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array elements\n");

	for(int i = 0 ; i<size ; i++){
		scanf("%d",&arr[i]);
	}

	int ret = kadanesAlgo(arr,size);
	printf("maximum subarray sum : %d\n",ret);
}
