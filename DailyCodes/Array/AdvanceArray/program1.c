// Prefix array
// -It is a carry forward algorithm based technique
// -It generates a prefix array which contains array of addition to the consicutive elements till the index

//use case :
// Prefix array is used in sum problems where user is going to hit multiple queires to add array in particular
// range
#include<stdio.h>

void PrefixArray(int arr[] , int size , int q){
	int pSum[size];

	pSum[0] = arr[0];

	for(int i = 1 ; i<size ; i++){
		pSum[i] = pSum[i-1]+arr[i];
	}

	for(int i = 0 ; i<q ; i++){
		int start,end;

		printf("Enter start value of range : ");
		scanf("%d",&start);

		printf("Enter end value of range : ");
		scanf("%d",&end);
		if(start>=0 && start<size && end>=0 && end<size && start<=end){
			if(start == 0)
				printf("SUM : %d\n",pSum[end]);
			else
				printf("SUM : %d\n",pSum[end] - pSum[start-1]);
		}else{
			printf("Invalid Query\n");
		}
	}
}


void main(){
	int size;
	printf("Enter size : ");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array elements\n");
	for(int i = 0 ; i<size ; i++){
		scanf("%d",(arr+i));
	}

	int q;
	printf("Enter Number of Queries : ");
	scanf("%d",&q);
	
	PrefixArray(arr,size,q);
}
