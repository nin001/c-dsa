// left max optimized
#include<stdio.h>

int* leftMaxFun(int arr[] , int size , int leftMax[]){
        leftMax[0] = arr[0];

        for(int i = 1 ; i<size ; i++){
		if(leftMax[i-1]<arr[i]){
			leftMax[i] = arr[i];
		}else{
			leftMax[i] = leftMax[i-1];
		}
	}

        return leftMax;
}

void main(){
        int size;
        printf("Enter size : ");
        scanf("%d",&size);

        int arr[size];
        printf("Enter array elements\n");
        for(int i = 0 ; i<size ; i++){
                scanf("%d",(arr+i));
        }

        int leftMax[size];

        leftMaxFun(arr,size,leftMax);

        printf("leftMax array\n");
        for(int i = 0 ; i<size ; i++)
                printf("%d ",leftMax[i]);
        printf("\n");

}

