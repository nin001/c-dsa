// subarray problem
// program to print all the existing subarray in the array
#include<stdio.h>

void subarray(int arr[] , int size){
	for(int i = 0 ; i<size ; i++){
		for(int j = i ; j<size ; j++){
			for(int k = i ; k<=j ; k++){
				printf("%d ",arr[k]);
			}
			printf("\n");
		}
	}
}


void main(){
	int size;
	printf("Enter size : ");
	scanf("%d",&size);

	int arr[size];
	
	printf("Enter array elements\n");
	for(int i = 0 ; i<size ; i++){
		scanf("%d",(arr+i));
	}

	subarray(arr,size);
}
