// Right max
// It is carry forward algorithm which returns array where insertion of elements largest in the right sub array
// is inserted through back side

#include<stdio.h>

int* rightMaxFun(int arr[] , int size , int rightMax[]){
        rightMax[size-1] = arr[size-1];

        for(int i = size-2 ; i>=0 ; i--){
                int rmax = arr[i];
                for(int j = i+1 ; j<size ; j++){
                        if(arr[j]>rmax)
				rmax = arr[j];
                }
                
		rightMax[i] = rmax;
        }

        return rightMax;
}

void main(){
        int size;
        printf("Enter size : ");
        scanf("%d",&size);

        int arr[size];
        printf("Enter array elements\n");
        for(int i = 0 ; i<size ; i++){
                scanf("%d",(arr+i));
        }

        int rightMax[size];

        rightMaxFun(arr,size,rightMax);

        printf("rightMax array\n");
        for(int i = 0 ; i<size ; i++)
                printf("%d ",rightMax[i]);
        printf("\n");

}

