//array initialization;
//character array , integer array , float array
#include<stdio.h>

void main(){
	int iarr[3] = {1,2,3};

	char carr[3] = {'a','b','c'};

	float farr[3];
	farr[0] = 1.23;
	farr[1] = 4.12;
	farr[2] = 1.002;

	for(int i = 0 ; i<3 ; i++){
		printf("iarr : %d\n",iarr[i]);
		printf("carr : %c\n",carr[i]);
		printf("farr : %f\n",farr[i]);
	}

}
