// Arrays in c 
// Array is a data structure which stores data in sequential manner and array has following things
// 1. Index which starts from 0 and soo on 
// 2. Array stores multiple datas of same data type under single variable name

//array declaration and initialization ways
#include<stdio.h>

void main(){
	int arr[5] = {12,45,55,20,5};
	//array is declared using int arr[size of array];
	printf("Elements in array are : ");
	for(int i = 0 ; i<5 ; i++){
		printf("%d ",arr[i]);
	}
	printf("\n");
}
	
	
