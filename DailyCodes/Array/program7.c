//take size from user and insert elements in array and print the elements divisible by
//4 & 5 
#include<stdio.h>

void main(){
	int size;
	printf("Enter size of Array : ");
	scanf("%d",&size);

	int arr[size];

	for(int i = 0 ; i<size ; i++){
		printf("Enter element : " );
		scanf("%d",&arr[i]);
	}
	for(int i = 0 ; i < size ; i++){
		if(arr[i]%4==0 && arr[i]%5==0){
			printf("Element %d at index %d is divisible by 4 and 5\n",arr[i],i);
		}
	}
}
