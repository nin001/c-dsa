//printing array with %d and %p without []
#include<stdio.h>

void main(){
	int iarr[3] = {1,2,3};
	//printf("%d\n",iarr); //exepts format specifier int but got int * 
	printf("%p\n",iarr);
}

/*
   array name that is variable name points towards the first elements address 
   i.e. if we are using iarr then we are talking address of first element
   this is the reason it throws error while using iarr in %d format specifier
   and %p is a format specifier for addresses and pointers 
   this proves that iarr alone without [] is a name pointing to the address of the first element
*/
