//take size from user and take input in array and print array elements
#include<stdio.h>

void main(){
	int size;
	printf("Enter array size : ");
	scanf("%d",&size);
	int arr[size];
	for(int i = 0 ; i<size ; i++){
		printf("Enter %d element : ",i+1);
		scanf("%d",&arr[i]);
	}
	printf("Array elements are :\n");
	for(int i = 0 ; i<size ; i++){
		printf("%d\n",arr[i]);
	}
}

