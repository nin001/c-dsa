// take 2d array from user and print the sum
#include<stdio.h>

void main(){
	int row,col;
	int sum = 0;
	printf("Enter number of rows : " );
	scanf("%d",&row);
	printf("Enter number of cols : " );
	scanf("%d",&col);
	int arr[row][col];
	printf("Enter 2d Array elements :\n");
	for(int i = 0 ; i<row ; i++){
		for(int j = 0; j<col ; j++){
			scanf("%d",&(*(*(arr+i)+j)));
			sum = sum + *(*(arr+i)+j);
		}
	}
	printf("Array sum is : %d\n",sum);
}
