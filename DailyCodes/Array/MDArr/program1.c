//2d array take size from user and print elements using pointers
#include<stdio.h>

void main(){
	int row,col;
	printf("Enter number of rows : ");
	scanf("%d",&row);
	printf("Enter number of col : ");
	scanf("%d",&col);
	int arr[row][col];
	printf("Enter Array elements :\n");
	for(int i = 0; i<row ; i++){
		for(int j = 0; j<col ; j++){
			scanf("%d",&(*(*(arr+i)+j)));
		}
	}

	for(int i = 0; i<row ; i++){
		for(int j = 0; j<col ; j++){
			printf("%d\t",*(*(arr+i)+j));
		}
		printf("\n");
	}
}
