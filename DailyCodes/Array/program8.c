//assingning array to an array
//
#include<stdio.h>

void main(){
	//hard coded 
	int arr1[3] = {1,2,3};
	int arr2[3];
	//arr2 = arr1;
	/*
	    here we have two array arr1 and arr2 and both are of same size so if we do arr2 = arr1 
	    we know that arr without [] is the address of the first element 
	    so this will lead us to error
erer: assignment to expression of array type 

this means that we are assigning costant to another constant just like 10 = 20 this is not possible
*/
	//correct way of assigning
	for(int i = 0 ; i<3 ; i++){
		arr2[i] = arr1[i];
	}
	printf("Elements in array 2 : ");
	for(int i = 0 ; i<3 ; i++){
		printf("%d ",arr2[i]);
	}
	printf("\n");
}
