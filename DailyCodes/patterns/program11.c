//print
//123
//45
//6
#include <stdio.h>

void main(){
	int row; 
	printf("Enter number of rows : ");
	scanf("%d",&row);
	int var = 1;
	for(int i = 1 ; i<=row ; i++){
		for(int j = i ; j<=row ; j++){
			printf("%d ",var);
			var++;
		}
		printf("\n");
	}
}
