//WAP to take one value in function and print value in main function
#include<stdio.h>
void fun(int* x){
	printf("in function ....\n");
	int val;
	printf("Enter value : ");
	scanf("%d",&val);
	*x = val;
	printf("End function.....\n");
}
void main(){
	printf("start main ...\n");
	int x;
	fun(&x);
	printf("Value : %d\n",x);
	printf("End main....\n");
}
