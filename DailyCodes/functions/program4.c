//Wap to array from user in function and print in main func
#include<stdio.h>

void array(int *arrptr , int size){
	int arr[size];
	printf("Enter array elements :\n");
	for(int i = 0 ; i<size ; i++){
		scanf("%d",&arr[i]);
		*(arrptr+i) = arr[i];
	}

}
void main(){
	int size;
	printf("Enter array size : ");
	scanf("%d",&size);
	int arr[size];
	array(arr,size);
	printf("Array elements are : ");
	for(int i = 0 ; i<size ; i++){
		printf("%d ",arr[i]);
	}
	printf("\n");
}
	
