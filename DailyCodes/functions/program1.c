/*
WAP to take value from user and printf it in another function
*/
#include<stdio.h>

void print(int);

void main(){
	int x;
	printf("Enter number : ");
	scanf("%d",&x);
	print(x);
}

void print(int x){
	printf("You entered : %d\n",x);
}	
