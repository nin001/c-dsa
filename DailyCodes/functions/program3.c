//take array from user in main and print array in another function
#include<stdio.h>

void printarr(int * arrptr,int size){
	printf("Array elements are : ");
	for(int i = 0 ; i<size ; i++){
		printf("%d ",*(arrptr+i));
	}printf("\n");
}

void main(){
	int size;
	printf("Enter size : ");
	scanf("%d",&size);
	int arr[size];printf("Enter array elements :\n");
	for(int i = 0 ; i<size ; i++)
		scanf("%d",&arr[i]);
	printarr(arr,size);	
}
