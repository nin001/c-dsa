// return mid element without using sorting
#include<stdio.h>

int mid(int arr[] , int size){
	int midCount = (0+size-1)/2;
	for(int i = 0 ; i<size ; i++){
		int count = 0;
		for(int j = 0 ; j<size ; j++){
			if(i!=j){
				if(arr[i]>arr[j])
					count++;
			}
		}
		if(count == midCount)
			return arr[i];
	}

}

void main(){
	int size;
	printf("Enter size : ");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array elements : ");
	for(int i = 0 ; i<size ; i++)
		scanf("%d",&arr[i]);

	int ret = mid(arr,size);
	printf("%d is mid\n",ret);
}
