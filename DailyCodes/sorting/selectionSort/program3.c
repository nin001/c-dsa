// array of N objects which 
// red - 0 
// white - 1
// blue - 2
#include<stdio.h>

void selectionSort(int arr[] , int size){
	for(int i = 0 ; i<size-1 ; i++){
		int minDex = i;
		for(int j = i+1 ; j<size ; j++){
			if(arr[minDex]>arr[j])
				minDex = j;
		}

		int temp = arr[i];
		arr[i] = arr[minDex];
		arr[minDex] = temp;
	}
}

void main(){
	int size;
	printf("Enter how many objects to enter : ");
	scanf("%d",&size);

	int arr[size];

	printf("Enter objects\nred - 0\nwhite - 1\nblue - 2\n");
	for(int i = 0 ; i<size ; i++){
		scanf("%d",&arr[i]);
		if(arr[i] != 1 && arr[i] != 2 && arr[i] != 0){
			printf("Enter right choice\n");
			i--;
		}
	}

	selectionSort(arr,size);
	for(int i = 0 ; i<size ; i++){
		printf("%d ",arr[i]);
	}
	printf("\n");
}
