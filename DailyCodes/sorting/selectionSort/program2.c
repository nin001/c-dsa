// factors sorting using selection sort
#include<stdio.h>

int factorCount(int num){

        int count = 0;
        for(int i = 1 ; i<num ; i++){
                if(num%i == 0)
                        count++;
        }

        return count;
}

void selectionSort(int arr[] , int size){
        for(int i = 0 ; i<size ; i++){
		int minDex = i;
                for(int j = i+1 ; j<size ; j++){
                        if(factorCount(arr[minDex])>factorCount(arr[j])){
                                    	minDex = j;
                        }else{
                                if(factorCount(arr[minDex]) == factorCount(arr[j])){
                                        if(arr[j]>arr[j+1]){
                                                minDex = j;
                                        }
                                }
                        }
                }

		int temp = arr[minDex];
		arr[minDex] = arr[i];
		arr[i] = temp;
        }
}

void main(){
        int size;
        printf("Enter size : ");
        scanf("%d",&size);

        int arr[size];
        printf("Enter elements\n");
        for(int i = 0 ; i<size ; i++)
                scanf("%d",&arr[i]);

        selectionSort(arr,size);

        printf("sorted array by factorCount\n");
        for(int i = 0 ; i<size ; i++)
                printf("%d ",arr[i]);
        printf("\n");

}

