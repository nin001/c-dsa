// selection sort
// it works on first index and the smallest number comes at first index and second smallest at second index
// same for rest
#include<stdio.h>

int selectionSort(int arr[] , int size){
	for(int i = 0 ; i<size-1 ; i++){
		int minDex = i;
		for(int j = i+1 ; j<size ; j++){
			if(arr[minDex] > arr[j])
				minDex = j;
		}

		int temp = arr[minDex];
		arr[minDex] = arr[i];
		arr[i] = temp;
	}
	return 0;
}

void main(){
	int size;
	printf("Enter size : ");
	scanf("%d",&size);

	int arr[size];

	printf("Enter unsorted elements\n");
	for(int i = 0 ; i<size ; i++)
		scanf("%d",&arr[i]);

	selectionSort(arr,size);
	
	printf("Sorted array\n");
	for(int i = 0 ; i<size ; i++)
		printf("%d ",arr[i]);
	printf("\n");

}
