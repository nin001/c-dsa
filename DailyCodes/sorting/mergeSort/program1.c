// works on dividing the unsorted array till found the last unit and then sorting it accordingly
#include<stdio.h>

int midSum = 0;

int  mergeSortRec(int arr[] , int start , int end){
	if(start<end){
		int mid = (start+end)/2;
		mergeSortRec(arr,start,mid);
		mergeSortRec(arr,mid+1,end);
		midSum += mid;
		return midSum;
	}
}

void main(){
	int arr[] = {7,1,4,-5,-3,10,8,9,3,2};

	int ret = mergeSortRec(arr,0,9);
	printf("MidSum = %d\n",midSum);

}
