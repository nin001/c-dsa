// Haore's algorithm

#include<stdio.h>

void swap(int *ptr1 , int *ptr2){
	int temp = *ptr1;
	*ptr1 = *ptr2;
	*ptr2 = temp;
}

int partition(int arr[] , int start , int end){
	int pivot = arr[start];

	int i = start-1;
	int j = end+1;

	while(1){
		do{
			i++;
		}while(arr[i]<pivot);

		do{
			j--;
		}while(arr[j]>pivot);

		if(i>=j)
			return j;
		swap(&arr[i],&arr[j]);
	}
}

void QuickHaore(int arr[] , int start , int end){
	if(start<end){
		int pivot = partition(arr,start,end);
		QuickHaore(arr,start,pivot);
		QuickHaore(arr,pivot+1,end);
	}
}


void main(){
	int size;
	printf("Enter size : ");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array elements\n");
	for(int i = 0 ; i<size ; i++)
		scanf("%d",&arr[i]);

	QuickHaore(arr,0,size-1);
	
	printf("Sorted Array\n");
	for(int i = 0 ; i<size ; i++)
		printf("%d ",*(arr+i));
	printf("\n");
}
