// Lomoto approch
// In this approh we select last element as the pivot element and then we put the pivot element at its index
// where on its left there are elements smaller than pivot and at right there are elements present which are 
// greater than pivot

#include<stdio.h>

void swap(int *ptr1 , int *ptr2){
	int temp = *ptr1;
	*ptr1 = *ptr2;
	*ptr2 = temp;
}

int partition(int arr[] , int start , int end){
	int pivot = arr[end];

	int itr = -1;
	
	for(int i = 0 ; i<end ; i++){
		if(arr[i]<pivot){
			swap(&arr[++itr],&arr[i]);
		}
	}

	swap(&arr[itr+1],&arr[end]);

	return itr+1;
}

void QuickLomoto(int arr[] , int start , int end){
	if(start<end){
		int pivot = partition(arr,start,end);
		QuickLomoto(arr,start,pivot-1);
		QuickLomoto(arr,pivot+1,end);
	}
}

void main(){
	int size;
	printf("Enter size : ");
	scanf("%d",&size);

	int arr[size];
	printf("Enter array elements\n");
	for(int i = 0 ; i<size ; i++)
		scanf("%d",&arr[i]);

	int start = 0 ;
	int end = size-1 ; 

	QuickLomoto(arr,start,end);
	
	printf("Sorted array\n");
	for(int i = 0 ; i<size ; i++)
		printf("%d ",*(arr+i));
	
	printf("\n");

}
