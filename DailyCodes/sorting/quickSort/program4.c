// naive approch with mid element as pivot
#include<stdio.h>

int position(int arr[] , int start , int end){
	int pivot = arr[(start+end)/2];

	int temp[end-start+1];
	int index = 0;

	for(int i = start ; i <= end ; i++){
		if(arr[i]<pivot){
			temp[index++] = arr[i];
		}
	}

	int pos = index+start;
	temp[index++] = pivot;
	for(int i = start ; i<= end ; i++){
		if(arr[i]>pivot){
			temp[index++] = arr[i];
		}
	}

	for(int i = start ; i <= end ; i++){
		arr[i] = temp[i-start];
	}

	return pos;
}

void QuickNaiveMid(int arr[] , int start , int end){
	if(start<end){
		int pivot = position(arr,start,end);

		QuickNaiveMid(arr,start,pivot-1);
		QuickNaiveMid(arr,pivot+1,end);

	}
}


void main(){
	int size;
	printf("Enter size : ");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array elements\n");
	for(int i = 0 ; i<size ; i++)
		scanf("%d",&arr[i]);

	QuickNaiveMid(arr,0,size-1);
	
	printf("sorted array\n");
	for(int i = 0 ; i<size ; i++){
		printf("%d ",arr[i]);
	}

	printf("\n");
}
