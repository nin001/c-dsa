// Naive approch

#include<stdio.h>

int partition(int arr[] , int start , int end){
	int pivot = arr[start];
	
	int temp[end-start+1];
	int index = 0;
	for(int i = start+1 ; i<=end ; i++){
		if(arr[i]<pivot)
			temp[index++] = arr[i];
	}

	int pos = index+start;
	temp[index++] = pivot;

	for(int i = start+1 ; i<=end ; i++){
		if(arr[i]>pivot)
			temp[index++] = arr[i];
	}

	for(int i = start ; i<=end ; i++){
		arr[i] = temp[i-start];
	}

	return pos;
}
		
void QuickNaive(int arr[] , int start , int end){
	if(start<end){
		int pivot = partition(arr,start,end);
		QuickNaive(arr,start,pivot-1);
		QuickNaive(arr,pivot+1,end);
	}
}


void main(){
	int size;
	printf("Enter size : ");
	scanf("%d",&size);

	int arr[size];
	printf("Enter array elements\n");
	for(int i = 0 ; i<size ; i++)
		scanf("%d",&arr[i]);

	QuickNaive(arr,0,size-1);
	
	printf("Sorted array\n");
	for(int i = 0 ; i<size ; i++)
		printf("%d ",arr[i]);
	printf("\n");
}
