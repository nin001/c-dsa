#include<stdio.h>

void countSort(int arr[] , int n , int pos){
        int countArr[10] = {0};

	for(int i = 0 ; i<n ; i++)
                countArr[(arr[i]/pos)%10]++;


        for(int i = 1 ; i<10 ; i++)
                countArr[i] += countArr[i-1];

        int output[n];
        for(int i = n-1 ; i>=0 ; i--){
                output[countArr[(arr[i]/pos)%10]-1] = arr[i];
                countArr[(arr[i]/pos)%10]--;
        }

        for(int i = 0 ; i<n ; i++)
                arr[i] = output[i];
}

void radixSort(int arr[] , int size){
	int max = arr[0];
	for(int i = 1 ; i<size ; i++)
		if(max<arr[i])
			max = arr[i];

	for(int pos = 1 ; max/pos > 0 ; pos *= 10)
		countSort(arr,size,pos);

}
void main(){
        int arr[] = {720,435,1235,920,5,8,22,115};

        int size = sizeof(arr)/sizeof(arr[0]);

        printf("Array : ");
        for(int i = 0 ; i<size ; i++)
                printf("%d ",arr[i]);
        printf("\n");

        radixSort(arr,size);

        printf("Sorted Array : ");
        for(int i = 0 ; i<size ; i++)
                printf("%d ",arr[i]);
        printf("\n");

}

