// insertion sort
// in insertion sort , we make two lists sorted and unsorted list
// sorted list is the first element and from unsorted list we insert 
// to its position in sorted list 
#include<stdio.h>

void insertionSort(int arr[] , int size){
	for(int i = 1 ; i<size ; i++){
		int store = arr[i];
		int j;
		for( j = i-1 ; j>=0 && store<arr[j] ; j--){
			arr[j+1] = arr[j];
		}

		arr[j+1] = store;
	}
}

void main(){
	int size;
	printf("Enter size : ");
	scanf("%d",&size);

	int arr[size];

	printf("Enter arrray elements\n");
	for(int i = 0 ; i<size ; i++)
		scanf("%d",&arr[i]);

	insertionSort(arr,size);

	printf("Sorted array\n");
	for(int i = 0 ; i<size ; i++)
		printf("%d ",arr[i]);
	printf("\n");

}
