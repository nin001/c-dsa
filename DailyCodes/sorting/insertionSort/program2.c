// using insertion sort 
// sort the character array according to the length if same then dont swap

#include<stdio.h>

int mystrlen(char *str1){
	int count = 0;
	while(*str1 != '\0'){
		count++;
		str1++;
	}
	return count;
}

void mystrcpy(char *dest , char *src){
	while(*src != '\0'){
		*dest = *src;
		src++;
		dest++;
	}
}

void insertionSort(int row , int col , char arr[row][col]){
	for(int i = 1 ; i < row ; i++){
		int store = mystrlen(arr[i]);
		int j;
		for(j = i-1 ; j>=0 && store<mystrlen(arr[j]) ; j--){
			mystrcpy(arr[j+1],arr[j]);			
		}

		mystrcpy(arr[j+1],arr[i]);
	}
}

void main(){
	char arr[7][7] = {"niraj","ram","mahesh","Ajay","akshuu","y","yy"};

	insertionSort(7,7,arr);

	for(int i = 0 ; i<7 ; i++){
		puts(arr[i]);
	}
	printf("\n");

}
