// sort array of strings according to the length of the string
#include<stdio.h>

int mystrlen(char *str){
	int len = 0;
	while(*str != '\0'){
		len++;
		str++;
	}
	return len;
}

void swap(char *ptr1 , char *ptr2){
	int lenChecked = 0;
	while(*ptr1 != '\0' && lenChecked <= mystrlen(ptr1)){
		char temp = *ptr1;
		*ptr1 = *ptr2;
		*ptr2 = temp;
		ptr1++;
		ptr2++;
		lenChecked++;
	}
}

void sort(int size , char ptr[][size]){
	for(int i = 0 ; i<size-1 ; i++){
		for(int j = 0 ; j<size-i-j ; j++){
			if(mystrlen(ptr[j])>mystrlen(ptr[j+1])){
				
			}else{
				if(mystrlen(ptr[j]) == mystrlen(ptr[j+1])){
					char *temp = ptr[j];
					int k = 0;
					while(*temp != '\0' && k<=mystrlen(ptr[j])){
						if(ptr[j][k] > ptr[j+1][k])
							swap(ptr[j] , ptr[j+1]);
						k++;
						temp++;
					}
				}
			}
		}
	}

}

void main(){
	char ch[][6] = {"Ashish" , "Kanha" , "Rahul" , "Raj", "Anuj" , "Shashi"};
	
	sort(6,ch);

	for(int i = 0 ; i<6 ; i++)
		printf("%s ",ch[i]);
	printf("\n");
}


// not working 
