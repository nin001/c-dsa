// using bubble sort sort the numbers array by the number of factors 
#include<stdio.h>

int factorCount(int num){

	int count = 0;
	for(int i = 1 ; i<num ; i++){
		if(num%i == 0)
			count++;
	}

	return count;
}

void bubbleSort(int arr[] , int size){
	for(int i = 0 ; i<size ; i++){
		for(int j = 0 ; j<size-1-i ; j++){
			if(factorCount(arr[j])>factorCount(arr[j+1])){
					int temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = temp;
			}else{
				if(factorCount(arr[j]) == factorCount(arr[j+1])){
					if(arr[j]>arr[j+1]){
					 	int temp = arr[j];
						arr[j] = arr[j+1];
						arr[j+1] = temp;
					}
				}
			}		
		}
	}
}

void main(){
	int size;
	printf("Enter size : ");
	scanf("%d",&size);

	int arr[size];
	printf("Enter elements\n");
	for(int i = 0 ; i<size ; i++)
		scanf("%d",&arr[i]);

	bubbleSort(arr,size);

	printf("sorted array by factorCount\n");
	for(int i = 0 ; i<size ; i++)
		printf("%d ",arr[i]);
	printf("\n");

}
