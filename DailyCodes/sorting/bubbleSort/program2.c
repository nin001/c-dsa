// bubble sort using single for loop
#include<stdio.h>

int bubbleSort(int arr[] , int size){
	int itr = 0;
	for(int i = 0 ; i<size-itr-1 ; i++){
		if(arr[i]>arr[i+1]){
			int temp = arr[i];
			arr[i] = arr[i+1];
			arr[i+1] = temp;
		}

		if(i == size-itr-2){
			i = 0;
			itr++;
		}

		if(itr == size-2)
			break;
	}
	return 0;
}

void main(){
	int size;
	printf("Enter size : ");
	scanf("%d",&size);

	int arr[size];

	printf("Enter unsorted array\n");
	for(int i = 0 ; i<size ; i++){
		scanf("%d",&arr[i]);
	}

	bubbleSort(arr,size);

	printf("Sorted array\n");
	for(int i = 0 ; i<size ; i++)
		printf("%d ",arr[i]);
	printf("\n");
}
