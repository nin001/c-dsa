// bubble sort is a sorting technique which works on the last index
// takes the largest element to the last index and second largest to the second last index
#include<stdio.h>

int bubbleSort(int arr[] , int size){
	for(int i = 0 ; i<size-1 ; i++){
		for(int j = 0 ; j<size-i-1 ; j++){
			if(arr[j]>arr[j+1]){
				int temp = arr[j];
				arr[j] = arr[j+1];
				arr[j+1] = temp;
			}
		}
	}
	return 0;
}

void main(){
	int size;
	printf("Enter size : ");
	scanf("%d",&size);

	int arr[size];

	printf("Enter unsorted array\n");
	for(int i = 0 ; i<size ; i++){
		scanf("%d",&arr[i]);
	}

	bubbleSort(arr,size);

	printf("Sorted array\n");
	for(int i = 0 ; i<size ; i++){
		printf("%d ",arr[i]);
	}
	printf("\n");
}

