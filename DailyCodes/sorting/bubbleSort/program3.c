// bubble sort in linked list
#include<stdio.h>
void* malloc(size_t);

typedef struct node{
	int data;
	struct node *next;
}node;

node *head = NULL;

node* createNode(){
	node *newNode = (node*)malloc(sizeof(node));

	printf("Enter data : ");
	scanf("%d",&newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){
	node *newNode = createNode();
	if(head == NULL)
		head = newNode;
	else{
		node *temp = head;
		while(temp->next != NULL)
			temp = temp->next;
		temp->next = newNode;
	}
}

int nodeCount(node *head){
	if(head == NULL)
		return 0;
	else{
		int count = 0;
		while(head!=NULL){
			count++;
			head = head->next;
		}
		return count;
	}
}

void bubbleSort(node *head){
	node *tempPtr = head;
	int count = nodeCount(head);
	int itr = 1;
	while(itr != count-1){
		if(tempPtr->data > tempPtr->next->data){
			int temp = tempPtr->data;
			tempPtr->data = tempPtr->next->data;
			tempPtr->next->data = temp;
		}

		tempPtr = tempPtr->next;

		if(tempPtr->next == NULL){
			tempPtr = head;
			itr++;
		}
	}
}

void display(){
	node *temp = head;
	while(temp->next != NULL){
		printf("%d->",temp->data);
		temp = temp->next;
	}
	printf("%d\n",temp->data);
}


void main(){
	int countNode;
	printf("Enter number of nodes : ");
	scanf("%d",&countNode);
	
	printf("Enter unsorted data\n");

	for(int i = 0 ; i<countNode ; i++)
		addNode();
	display();
	bubbleSort(head);
	display();
}
