// counting sort algorithm

#include<stdio.h>

void countSort(int *arr , int n){
	int max = arr[0];
	for(int i = 1 ; i<n ; i++)
		if(max<arr[i])
			max =  arr[i];
	
	int countArr[max+1];
	for(int i = 0 ; i<=max ; i++)
		countArr[i] = 0;


	for(int i = 0 ; i<n ; i++)
		countArr[arr[i]]++;
	

	for(int i = 1 ; i<=max ; i++)
		countArr[i] += countArr[i-1];
	
	int output[n];
	for(int i = n-1 ; i>=0 ; i--){
		output[countArr[arr[i]]-1] = arr[i];
		countArr[arr[i]]--;
	}

	for(int i = 0 ; i<n ; i++)
		arr[i] = output[i];
}


void main(){
	int arr[] = {3,7,2,1,8,2,5,2,7};

	int size = sizeof(arr)/sizeof(arr[0]);
	
	printf("Array : ");
	for(int i = 0 ; i<size ; i++)
		printf("%d ",arr[i]);
	printf("\n");
	
	countSort(arr,size);
	
	printf("Sorted Array : ");
	for(int i = 0 ; i<size ; i++)
		printf("%d ",arr[i]);
	printf("\n");
	
}
