//print odd square from the given range
//
#include <stdio.h>

void main(){
	int sloop,eloop;
	printf("Enter 1 value : ");
	scanf("%d",&sloop);
	printf("Enter 2 value : ");
	scanf("%d",&eloop);

	for(int i = sloop ; i<=eloop ; i++){
		if(i%2==1 && i>0){
			printf("Square of %d is %d\n",i,i*i);
		}
	}
}
