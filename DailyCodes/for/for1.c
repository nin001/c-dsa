//square of odd and cube of even
#include<stdio.h>

void main(){
	int sLoop,eLoop;
	printf("Enter first number : ");
	scanf("%d",&sLoop);
	printf("Enter second number : ");
	scanf("%d",&eLoop);

	for(int i = sLoop ; i<=eLoop ; i++){
		if(i%2==0){
			printf("%d cube = %d\n",i,((i*i*i)));
		}else if(i%2==1){
			printf("%d square = %d\n",i,(i*i));
		}
	}
}
