//print the numbers divisible by 5 between given range
#include<stdio.h>

void main(){
	int sLoop,eLoop;
	printf("Enter the first number :");
	scanf("%d",&sLoop);
	printf("Enter the second number :");
	scanf("%d",&eLoop);

	for(int i = sLoop ; i<=eLoop ; i++){
		if(i%5==0 && i>0){
			printf("%d is divisible by 5 \n",i);
		}
	}
}
