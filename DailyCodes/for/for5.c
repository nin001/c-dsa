//print no. of even in  given range
#include<stdio.h>

void main(){
	int sLoop,eLoop;
	int count = 0;
	printf("Enter the starting value : ");
	scanf("%d",&sLoop);
	printf("Enter the ending value : ");
	scanf("%d",&eLoop);
	for(int i = sLoop ; i<=eLoop ; i++){
		if(i%2==0 && i>0){
			count = count + 1;
		}
	}
	printf("Total even numbers in the given range : %d\n",count);	
}
