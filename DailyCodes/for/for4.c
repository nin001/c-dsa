//print reverse order in given range
#include <stdio.h>

void main(){
	int sLoop,eLoop;
	printf("Enter the starting number : ");
	scanf("%d",&sLoop);
	printf("Enter the ending number : ");
	scanf("%d",&eLoop);

	for(int i = eLoop ; i>=sLoop ; i--){
		printf("%d\n",i);
	}
}
