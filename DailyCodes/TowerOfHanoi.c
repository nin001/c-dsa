//Tower of hanoi code
//using recursion

#include<stdio.h>

void TOH(int n , char a , char b , char c){
	if(n>0){
		TOH(n-1,a,c,b);
		printf("%c->%c\n",a,c);
		TOH(n-1,b,a,c);
	}
}

void main(){
	printf("Enter Number : ");
	int num;
	scanf("%d",&num);

	char A='A' , B = 'B' , C = 'C';

	TOH(num,A,B,C);
}
