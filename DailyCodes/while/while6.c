/*print using while loop
1
4  9
16 25 36
*/
#include<stdio.h>

void main(){
	int x = 1;
	int i = 1;
	while(i<=3){
		int j = 1;
		while(j<=i){
			printf("%d ",x*x);
			x++;
			j++;
		}
		printf("\n");
		i++;
	}
}
	
