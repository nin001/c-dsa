// print even numbers between the range taken from the user
#include<stdio.h>

void main(){
	int st,end;
	printf("Enter first number : ");
	scanf("%d",&st);
	printf("Enter second number : ");
	scanf("%d",&end);

	while(st<=end){
		if(st%2==0){
			printf("%d is even\n",st);
		}
		st++;
	}
}
