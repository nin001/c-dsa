// use while loop for printing patterns
// ***
// ***
// ***
#include<stdio.h>

void main(){
	int i = 1;
	while(i<=3){
		int j = 1;
		while(j<=3){
			printf("* ");
			j++;
		}
		printf("\n");
		i++;
	}
}
