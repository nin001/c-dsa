// while syntax
// " initialization
//" while(condition){
// "	body of while 
//" 	increment/decrement
//" }"
#include<stdio.h>

void main(){
	int i = 0;
	while(i<=10){
		printf("iteration number %d\n",i);
		i++;
	}
}
