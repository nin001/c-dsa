#include <stdio.h>
void main(){
	int user_age;
	printf("Please Enter your age : ");
	scanf("%d",&user_age);			// This is the predefined function for accepting values in the variables
	printf("Your age is %d\n ",user_age);
	/*Scanf is the predefined function in c used to take input from the user it has two parameteres 
	 one is the format specifier which has format of the data or the datatype of the variable that we are
	 taking input and other is the address of the variable in which the taken value is to be stored
	 */
}
