/*Operators:
 operators are the symbols that are used to do certain operation on opperands
eg=> ans = a+b; here '+' is a operator and '=' is a assigning operator and a and b are operands on which operation is performing

There are certain types of operators in c 
-uanary
-binary
-terenary
 
unary - two types -> incriment '++' and decrement '--'
these two further have two types that are pre and post (inc/dec)

functionality of pre and post
int pre(int x){
	x = x+1;
	return x;
}
int post(int x){
	int temp;
	temp = x;
	x = x+1;
	return temp;
}
these two functions are same for decrement and incriment unary operators (in decrement just sign changes)

*/
#include <stdio.h>

void main(){
	int x = 8;  //initialisation 
	int ans1;	// declaration
	ans1 = ++x + ++x;
	printf("Value of x = %d ans value of ans1 = %d\n",x,ans1);
	//----------------------------------
	int y = 2;
	int ans2;
	ans2 = y++ + y++;
	printf("value of y = %d and value of ans2 = %d\n",y,ans2);
	//------------------------------------
	int z = 10;
	int z1 = 10;
	int ans33;
	int ans3;
	ans33 = z1++ + ++z1 + ++z1 + ++z1;
	ans3 = z++ + ++z;
	printf("value of z1 = %d and value of ans33 = %d\n",z1,ans33);
	printf("Value of z = %d and value of ans3 = %d\n",z,ans3);
	//-------------------------------------
	int a = 12;
	int b = 10;
	int ans4;
	ans4 = a-- + ++b;
	printf("vale of ans4 = %d\n",ans4);
	ans4 = --a + ++a;
	printf("Value of ans4 = %d\n",ans4);
	printf("Value of a and b is %d & %d\n",a,b);
	
}
