#include <stdio.h>

void func(){
	printf("In the user defined function...\n");
}

void main(){ 			//void main is also a user defined function which is recognised by the os this function is ran first by the OS and is also called as Entry point function
	printf("start main...\n");
	printf("In the main function...\n");
	printf("user defined function called...\n");
	func();  //Function called
	printf("End main ...\n");
}

