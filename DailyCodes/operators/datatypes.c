#include <stdio.h>

void main(){
	int age = 21;
	float salary = 500000.00;
	char fav_char = 'A'; 
	double gdp_ind = 2.62;
	//void is also a primitive data type in c;
	printf("%d\n", age);
	printf("%f\n",salary);
	printf("%c\n",fav_char);
	printf("%lf\n",gdp_ind);
	
}
	
	
	
	
	
	/*
	 Data types : data types are the containers which has defined size and defined data type to store:
	 There are three types in data types in c:
	 1. Primitive or user defined 
	 2. user defined data types 
	 3. Derived data types
	 Primitive data types are the data types which are defined already by c, there are five primitive data types in c :
	 int, float , char , double & void 
	 int - 4 bytes , float - 4 bytes ,char - 1 byte , double -8 byte , void - 1 byte   */
	
