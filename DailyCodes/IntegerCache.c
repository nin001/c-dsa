//Implementation of IntegerCaching in c 

#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

typedef struct Node{
	int *dataPtr;
	struct Node *next;
}node;

node *head = NULL;

bool isFirst(){
	if(head->next == NULL)
		return true;
	else
		return false;
}

void addInteger(int data){
	node *newNode = (node*)malloc(sizeof(node));

	if(head == NULL)
		head = newNode;
	else{
		node *temp = head;
		while(temp->next != NULL)
			temp = temp->next;
		temp->next = newNode;
	}
	newNode->next = NULL;

	if(isFirst()){
		newNode->dataPtr = (int*)malloc(sizeof(int));
		*(newNode->dataPtr) = data;
	}else{
		if(data>=-128 && data<128){
			node *temp = head;
			int flag = 0;
			while(temp->next != NULL){
				if(*(temp->dataPtr) == data){
					flag = 1;
					break;
				}
				temp = temp->next;
			}
			if(flag == 1){
				newNode->dataPtr = temp->dataPtr;
			}else{
				newNode->dataPtr = (int*)malloc(sizeof(int));
				*(newNode->dataPtr) = data;
			}
		}else{
			newNode->dataPtr = (int*)malloc(sizeof(int));
			*(newNode->dataPtr) = data;
		}
	}
}

void printInteger(){
	if(head == NULL){
		printf("Insert Integers first\n");
	}else{
		node *temp = head;
		while(temp != NULL){
			printf("%d -> %p\n",*(temp->dataPtr),temp->dataPtr);
			temp = temp->next;
		}

	}
}

int countNode(){
	if(head == NULL)
		return 0;
	else{
		node *temp = head;
		int count = 0;
		while(temp!=NULL){
			temp = temp->next;
			count++;
		}
		return count;
	}
}

void delIntegerAtPos(int pos){
	int count = countNode();
	if(pos <= 0 || pos > count)
		printf("invalid Position");
	else{
		if(pos == 1){
			node *temp = head;
			head = head->next;
			free(temp->dataPtr);
			free(temp);
		}else if(pos == count){
			node *temp = head;
			while(temp->next->next != NULL)
				temp = temp->next;
			free(temp->next->dataPtr);
			free(temp->next);
			temp->next = NULL;
		}else{
			node *temp = head;
			while(pos-2){
				temp = temp->next;
				pos--;
			}
			node *temp2 = temp->next;
			temp->next = temp2->next;
			free(temp2->dataPtr);
			free(temp2);
			temp2 = NULL;
		}
	}
}


void main(){
	char choice;

	do{
		printf("Integer Caching\n");
		printf("1.AddInteger\n");
		printf("2.PrintInteger\n");
		printf("3.DelIntegerAtPosition\n");

		printf("Enter choice : ");
		int ch;
		scanf("%d",&ch);

		switch(ch){
			case 1:{
				int data;
				printf("Enter data : ");
				scanf("%d",&data);
				addInteger(data);
			       }
			       break;
			case 2:
			       printInteger();
			       break;
			case 3:
			       {
				int pos;
				printf("Enter position :");
				scanf("%d",&pos);
				delIntegerAtPos(pos);
			       }
			       break;
			default:
			       printf("Invalid Choice\n");
		}

		getchar();
		printf("Do you want to continue(y/n): ");
		scanf("%c",&choice);
	}while(choice == 'Y' || choice == 'y');
}
