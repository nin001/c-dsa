// second last occurance
#include<stdio.h>

int secLastOcc(int arr[] , int size , int key){
	int index = -1;
	int temp = -1;
	for(int i = 0 ; i<size ; i++){
		if(arr[i] == key){
			index = temp;
			temp = i;
		}
	}

	return index;
}

void main(){
	int size;
	printf("Enter size : ");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array elements :\n");
	for(int i = 0 ; i < size ; i++)
		scanf("%d",(arr+i));

	int key;
	printf("Enter key : ");
	scanf("%d",&key);

	int ret = secLastOcc(arr,size,key);

	if(ret != -1){
		printf("Second last occurance of %d is at %d index\n",key,ret);
	}else{
		printf("havent occured two times\n");
	}

}
