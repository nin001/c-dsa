// last occurance
#include<stdio.h>

int lastOcc(int arr[],int size,int key){
	int var = -1;
	for(int i = 0 ; i<size ; i++){
		if(arr[i] == key)
			var = i;
	}
	return var;
}

void main(){
	int size;
	printf("Enter size of array : ");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array elements :\n");
	for(int i = 0 ; i<size ; i++)
		scanf("%d",(arr+i));

	int key;
	printf("Enter element to search last occurance : ");
	scanf("%d",&key);

	int ret = lastOcc(arr,size,key);

	if(ret != -1){
		printf("index %d is last occurance of %d\n",ret,key);
	}else{
		printf("%d not present in the array\n",key);
	}
}
