// first occurance in sorted array using binary search
#include<stdio.h>

int firstOcc(int arr[] , int size , int key){
	int start = 0;
	int end = size-1;
	int mid = (start+end)/2;
	int store = -1;
	while(start<=end){
		if(arr[mid] == key){
			store = mid;
			if(arr[mid-1] != key)
				return mid;
			else
				end = mid-1;

		}
		
		if(arr[mid] < key)
			start = mid+1;
		if(arr[mid] > key)
			end =mid-1;

		mid = (start+end)/2;
	}

	return store;
}

void main(){
	int size;
	printf("Enter size : ");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array elements :\n");
	for(int i = 0 ; i<size ; i++)
		scanf("%d",(arr+i));

	int key;
	printf("Enter key : ");
	scanf("%d",&key);

	int ret = firstOcc(arr,size,key);
	if(ret != -1)
		printf("First occurance of %d is %d\n",key,ret);
	else
		printf("Not present\n");

}
