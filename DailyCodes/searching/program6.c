// ceiling value 
#include<stdio.h>

int ceilingVal(int arr[] , int size , int key){
	int start = 0 ;
	int end = size-1;
	int mid = (start+end)/2;
	int var = -1;
	while(start<=end){
		if(arr[mid] == key){
			return arr[mid];
		}

		if(arr[mid]<key){
			start = mid+1;
		}
		if(arr[mid]>key){
			var = arr[mid];
			end = mid-1;
		}

		mid = (start+end)/2;
	}
	return var;
}

void main(){
	int arr[] = {5,7,14,21,28,35,70};

	printf("Cieling value = %d\n",ceilingVal(arr,7,29));

}
