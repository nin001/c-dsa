// sorted data searching techique - Binary search
// Binaray search is a searching technique where we divide the array and find the desired 
#include<stdio.h>

int binarySearch(int arr[] , int size , int key){
	int start = 0;
	int end = size-1;

	int mid = start+end/2;

	while(start<=end){
		if(arr[mid] == key)
			return mid;

		if(arr[mid]<key){
			start = mid+1;
		}else{
			end = mid-1;
		}
		mid = start+end/2;
	}

	return -1;
}

void main(){
	int size;
	printf("Enter size : ");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array elements :\n");
	for(int i = 0 ; i<size ; i++)
		scanf("%d",(arr+i));

	int key;
	printf("Enter key : ");
	scanf("%d",&key);

	int ret = binarySearch(arr,size,key);
	if(ret != -1)
		printf("Found at %d index\n",ret);
	else
		printf("Not found\n");
}

		
