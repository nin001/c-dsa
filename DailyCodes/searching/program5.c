//Binary search questions
//
// Sorted array given and a key given return the floor value present in the array
#include<stdio.h>

int floorVal(int arr[],int size,int key){
	int start = 0;
	int end = size-1;
	int mid;
	int var = -1;
	while(start<=end){
		mid = (start+end)/2;
		if(arr[mid] == key){
			return arr[mid];
		}
		if(arr[mid]<key){
			var = arr[mid];
			start = mid+1;
		}else{
			end = mid-1;
		}
	}
	
	return var;
}


void main(){
	int size;
	printf("Enter size : ");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array elements :\n");

	for(int i = 0 ; i<size ; i++)
		scanf("%d",&arr[i]);

	int key;
	printf("Enter key : ");
	scanf("%d",&key);

	int ret = floorVal(arr,size,key);
	if(ret != -1){
		printf("Floor = %d\n",ret);
	}else{
		printf("Floor not found\n");
	}

}
