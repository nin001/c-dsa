// linear searching 
// searches in linear manner compares with each data one by one
// used for unsorted data
#include<stdio.h>

int ifpresent(int *ptr,int size,int element){
	for(int i = 0 ; i<size ; i++){
		if(*(ptr+i) == element)
			return i;
	}
	return -1;
}

void main(){
	int size;
	printf("Enter size of the array : ");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array elements :\n");
	for(int i = 0 ; i<size ; i++)
		scanf("%d",(arr+i));

	int key;
	printf("Enter element to search : ");
	scanf("%d",&key);

	int ret = ifpresent(arr,size,key);
	if(ret != -1){
		printf("Element present at %d index\n",ret);
	}else{
		printf("Element not present\n");
	}

}
