/*WAP to check wheather the given input is leap year or not*/ 
#include<stdio.h>

void main(){
	int year;
	printf("Enter year : ");
	scanf("%d",&year);
	
	if(year%4==0 && year>0){
		printf("%d is a leap year \n",year);
	}else if(year%4!=0 && year>0){
		printf("%d is not a leap year \n ",year);
	}else{
		printf("wrong input\n");
	}
}
