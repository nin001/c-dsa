//taking number from 0-5 and print its spelling
//
#include<stdio.h>

void main(){
	int x;
	printf("Enter number : ");
	scanf("%d",&x);
	if(x>=0){
		
		switch(x){
			case 0:
				printf("Zero\n");
				break;
			case 1:
				printf("One\n");
				break;
			case 2:
				printf("Two\n");
				break;
			case 3:
				printf("Three\n");
				break;
			case 4:
				printf("Four\n");
				break;
			case 5:
				printf("Five\n");
				break;
			default:
				printf("Greater than five\n");
				break;
		}
	}else{
		printf("Negative number\n");
	}
}
