// fibonacci series
//
#include<stdio.h>

void main(){
	int inp;
	printf("Enter number :");
	scanf("%d",&inp);
	int num1 = 0;
	int num2 = 1;
	int num3 = num1+num2;
	printf("%d %d ",num1,num2);
	while((num3)<inp){
		printf("%d ",num3);
		num1=num2;
		num2=num3;
		num3=num1+num2;
	}
	printf("\n");
}
