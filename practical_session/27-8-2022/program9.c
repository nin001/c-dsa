//enter a number and print the product of the digits
//
#include<stdio.h>

void main(){
	int dig,rem,product = 1;
	printf("Enter a number :");
	scanf("%d",&dig);
	while(dig!=0){
		rem = dig%10;
		product = product*rem;
		dig = dig/10;
	}
	printf("Product of digits is %d\n",product);
}

