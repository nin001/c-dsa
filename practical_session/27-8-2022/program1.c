//take user input from 1-x add the numbers divisible by 3
//
#include<stdio.h>

void main(){
	int x ;
	int sum = 0;
	printf("Enter number : ");
	scanf("%d",&x);
	if(x>0){
		for(int i = 1 ; i<=x ; i++){
			if(i%3!=0){
				sum = sum + i;
			}
		}
	}else{
		for(int i = x ; i>=0 ; i--){
			if(i%3!=0){
				sum = sum + i;
			}
		}
	}
	printf("Sum of numbers divisible by 3 is %d\n",sum);

}
