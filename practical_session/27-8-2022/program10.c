// input number from user and print the addition of the digit
//
#include<stdio.h>

void main(){
	int x,sum=0,rem;
	printf("Enter number :");

	while(x!=0){
		rem = x%10;
		sum = sum+rem;
		x = x/10;
	}
	printf("Addition of digits is %d\n",sum);

}
