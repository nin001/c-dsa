// reverse doubly linked list

#include <stdio.h>
#include <stdlib.h>

struct node {
	struct node *prev;
	int data;
	struct node *next;
};

struct node *head = NULL;

struct node* createNode() {
	struct node *newNode = (struct node*)malloc(sizeof(struct node));

	printf("Enter data : ");
	scanf("%d",&newNode->data);

	newNode->prev = NULL;
	newNode->next = NULL;

	return newNode;
}

void addNode() {
	struct node *newNode = createNode();
	if(head == NULL) {
		head = newNode;
	}else {
		struct node *temp = head;
		while(temp->next != NULL) {
			temp = temp->next;
		}
		newNode->prev = temp;
		temp->next = newNode;
	}
}

void reverse1() {
	if(head == NULL)
		return;
	else {
		if(head->next == NULL && head->prev == NULL)
			return;
		struct node *temp = head;
		while(temp->next != NULL) {
			struct node *temp1 = temp->prev;
			temp->prev = temp->next;
			temp->next = temp1;
			temp = temp->prev;
		}
		struct node *temp1 = temp->prev;
		temp->prev = temp->next;
		temp->next = temp1;

		head = temp;
	}
}

void main() {

	int countNode;
	printf("Enter number of nodes : ");
	scanf("%d",&countNode);

	for(int i = 0 ; i<countNode ; i++) {
		addNode();
	}

	reverse1();

	//print
	struct node *temp = head;
	while(temp->next != NULL) {
		printf("|%d|=",temp->data);
		temp = temp->next;
	}
	printf("|%d|\n",temp->data);
}
