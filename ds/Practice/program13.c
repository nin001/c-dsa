// stack using linked list

#include <stdio.h>
#include <stdlib.h>

struct node {
	int data;
	struct node *next;
};

struct node *head = NULL;
struct node *top = NULL;
int size = 0;
int flag = 0;

int countNode() {
	if(head == NULL)
		return 0;
	else {
		int count = 0;
		struct node *temp = head;
		while(temp != NULL) {
			count++;
			temp = temp->next;
		}
		return count;
	}
}

int push() {
	if(countNode() == size)
		return -1;
	else {
		struct node *newNode = (struct node *)malloc(sizeof(struct node));

		printf("Enter data : ");
		scanf("%d",&newNode->data);

		newNode->next = NULL;

		if(head == NULL) {
			head = newNode;
		}else {

			struct node *temp = head;
			while(temp->next != NULL) {
				temp = temp->next;
			}
			temp->next = newNode;
		}

		top = newNode;

		return 0;
	}
}

int pop() {
	if(countNode() == 0) {
		flag = 0;
		return -1;
	}else {
		flag = 1;
		int data = top->data;
		if(head == top) {
			free(top);
			head = top = NULL;
		}else {
			struct node *temp = head;
			while(temp->next != top) {
				temp = temp->next;
			}
			free(temp->next);
			top = temp;
		}

		return data;
	}
}

int peek() {
	if(top == NULL) {
		flag = 0;
		return -1;
	}else {
		flag = 1;
		return top->data;
	}
}

void printStack() {
	if(top == NULL) {
		printf("Empty stack\n");
	}else {
		struct node *temp = head;
		while(temp != NULL) {
			printf("|%d|\n",temp->data);
			temp = temp->next;
		}
	}
}
		
		
void main() {

	printf("Enter size of stack : ");
	scanf("%d",&size);

	char ch;
	do {
		printf("1.push\n");
		printf("2.pop\n");
		printf("3.peek\n");
		printf("4.PrintStack\n");

		int choice;
		printf("Enter choice : ");
		scanf("%d",&choice);

		switch(choice) {
			case 1:
				{
					int ret = push();
					if(ret != -1) {
						printf("Successfully pushed\n");
					} else {
						printf("Stack overflow\n");
					}
				}
				break;
			case 2:
				{
					int ret = pop();
					if(flag == 1)
						printf("%d popped\n",ret);
					else
						printf("Stack underflow");
				}
				break;
			case 3:
				{
					int ret = peek();
					if(flag == 0)
						printf("Stack empty\n");
					else
						printf("%d at top\n",ret);

				}
				break;
			case 4:
				printStack();
				break;
			default:
				printf("Invalid choice\n");
		}

		getchar();
		printf("Do you want to continue (y/n) : ");
		scanf("%c",&ch);
	}while(ch == 'y' || ch == 'Y');


}	
