// implementing 2 stacks in one array

#include <stdio.h>

int size = 0;
int top1 = -1;
int top2 = -1;

int flag = 0;

int push1(int *arr) {
	if(top1 == size-1 || top1 == top2-1 )
		return -1;
	else {
		printf("Enter data : ");
		scanf("%d",(arr+(++top1)));
		return 0;
	}
}

int push2(int *arr) {
	if(top2 == 0 || top2 == top1+1)
		return -1;
	else {
		printf("Enter data : ");
		scanf("%d",(arr+(--top2)));

		return 0;
	}
}

int pop1(int *arr) {
	if(top1 == -1) {
		flag = 1;
		return -1;
	}else {
		flag = 0;
		int val = *(arr+top1);
		top1--;
		return val;
	}
}

int pop2(int *arr) {
	if(top2 == size) {
		flag = 1;
		return -1;
	}else {
		flag = 0;
		int val = *(arr+top2);
		top2++;
		return val;
	}
}

int peek1(int *arr) {
	if(top1 == -1) {
		flag = 1;
		return -1;
	}else {
		flag = 0;
		return *(arr+top1);
	}
}

int peek2(int *arr) {
	if(top2 == size) {
		flag = 1;
		return -1;
	}else {
		flag = 0;
		return *(arr+top2);
	}
}

void main() {

	printf("Enter size of array : ");
	scanf("%d",&size);
	
	top2 = size;

	int arr[size];

	char ch;

	do {
		printf("Operations of 2 stacks\n");
		printf("1.Push in stack 1\n");
		printf("2.Push in stack 2\n");
		printf("3.Pop stack 1\n");
		printf("4.Pop stack 2\n");
		printf("5.Peek stack 1\n");
		printf("6.Peek stack 2\n");
		
		int choice;
		printf("Enter choice : ");
		scanf("%d",&choice);

		switch(choice) {
			case 1:
				{
					int ret = push1(arr);
					if(ret != -1) 
						printf("Pushed Successfully in stack1\n");
					else
						printf("Stack1 overflow\n");
				}
				break;
			case 2:
				{
					int ret = push2(arr);
					if(ret != -1) 
						printf("Pushed Successfully in stack2\n");
					else	
						printf("Stack2 overflow\n");
				}
				break;

			case 3:
				{
					int ret = pop1(arr);
					if(flag == 0)
						printf("%d Popped from stack 1\n",ret);
					else
						printf("Stack1 underflow\n");
				}
				break;
			case 4:
				{
					int ret = pop2(arr);
					if(flag == 0)
						printf("%d popped from stack2\n",ret);
					else
						printf("Stack2 Underflow\n");
				}
				break;
			case 5:
				{
					int ret = peek1(arr);
					if(flag == 0)
						printf("%d at top in stack1",ret);
				}
				break;
			case 6:
				{
					int ret = peek2(arr);
					if(flag == 0)
						printf("%d at top in stack2",ret);
				}
				break;
			default:
				printf("Invalid choice\n");
		}

		getchar();
		printf("Do you want to contine(y/n) : ");
		scanf("%c",&ch);
	}while(ch == 'Y' || ch == 'y');
}
