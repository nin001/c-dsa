// print reverse linked list in c 

#include <stdio.h>
#include <stdlib.h>

struct node {
	int data;
	struct node *next;
};

struct node *head = NULL;

struct node* createNode() {
	struct node *newNode = (struct node*)malloc(sizeof(struct node));

	printf("Enter data : ");
	scanf("%d",&newNode->data);

	newNode->next = NULL;
}

void addNode() {
	struct node *newNode = createNode();

	if(head == NULL) {
		head = newNode;
	}else {
		struct node *temp = head;
		while(temp->next != NULL)
			temp = temp->next;
		temp->next = head;
	}
}

int countNode() {
	if(head == NULL)
		return 0;
	else {
		int count = 0;
		struct node *temp = head;
		while(temp != NULL){
			count++;
			temp = temp->next;
		}
		return count;
	}
}

void printReverse() {
	if(head == NULL)
		printf("Empty linked list");
	else {
// WRTIE CODE HERE //////////////////////////////////////////////////////////////////



void main() {
	int count;
	printf("Enter no of nodes : ");
	scanf("%d",&count);

	for(int i = 0 ; i<count ; i++) {
		addNode();
	}
}
