// structres in c
// structure is a user defined data type , in which we can
// store various type of datas under a single name
// Structures provide a storage space in which we can store
// data such as string which is character array in c , and 
// all other primitive data type

#include <stdio.h>
#include <stdlib.h>
struct Employee {
	int empId;
	char empName[10];
	float sal;
};

void main() {

	struct Employee *emp1 = (struct Employee*)malloc(sizeof(struct Employee));
	emp1->empId = 1001;
	
	emp1->sal = 1332000;

	printf("Name : %s\nEmployee Id : %d\nEmployee Sal : %f\n",emp1->empName,emp1->empId,emp1->sal);

}
