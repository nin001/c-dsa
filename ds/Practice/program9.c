// convert singly linked list to singly circular linked list

#include <stdio.h>
#include <stdlib.h>

struct node {
	int data;
	struct node *next;
};

struct node *head = NULL;

struct node* createNode() {
	struct node *newNode = (struct node*)malloc(sizeof(struct node));

	printf("Enter data : ");
	scanf("%d",&newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode() {
	struct node *newNode = createNode();

	if(head == NULL)
		head = newNode;
	else {
		struct node *temp = head;
		while(temp->next != NULL)
			temp = temp->next;
		temp->next = newNode;
	}
}

void convertToCircular() {
	struct node *temp = head;
	if(head == NULL)
		return;
	while(temp->next != NULL)
		temp = temp->next;
	temp->next = head;
}


void main() {
	int countNode;
	printf("Number of nodes in Linked list : ");
	scanf("%d",&countNode);

	for(int i = 0 ; i<countNode ; i++) {
		addNode();
	}

	convertToCircular();

	printf("Linked list converted to circular linked list\n");

	struct node *temp = head;

	while(temp->next != head) {
		printf("|%d->%p|->",temp->data,temp->next);
		temp = temp->next;
	}
	printf("|%d->%p|\n",temp->data,temp->next);

}
