// Problem statement : given number and linked list , find the occurance of
// number in linkedlist

#include <stdio.h>
#include <stdlib.h>

struct node {
	int data;
	struct node *next;
};

struct node *head = NULL;

struct node* createNode() {
	struct node *newNode = (struct node *)malloc(sizeof(struct node));

	printf("Enter data : ");
	scanf("%d",&newNode->data);

	newNode->next = NULL;
}


void addNode() {
	struct node *newNode = createNode();

	if(head == NULL) {
		head = newNode;
	}else {
		struct node *temp = head;
		while(temp->next != NULL)
			temp = temp->next;

		temp->next = newNode;
	}
}


void printLL() {
	if(head == NULL)
		printf("Empty LL");
	else {
		struct node *temp = head;
		while(temp->next != NULL) {
			printf("|%d|->",temp->data);
			temp = temp->next;
		}
		printf("|%d|\n",temp->data);
	}
}

int countNumber(int value) {
	if(head == NULL) {
		return -1;
	} else {
		int count = 0;
		struct node *temp = head;
		while(temp!=NULL) {
			if(temp->data == value)
				count++;
			temp = temp->next;
		}
		return count;
	}
}

void main() {
	int countNode;
	printf("Enter number of nodes to add : ");
	scanf("%d",&countNode);

	for(int i = 0 ; i<countNode ; i++) {
		addNode();
	}

	int val;
	printf("Enter number : ");
	scanf("%d",&val);

	int ret = countNumber(val);
	if(ret != -1)
		printf("Occurances of %d is %d\n",val,ret);

}
