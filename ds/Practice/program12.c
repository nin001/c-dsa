//Stack 
//
//a data structure which follows First in Last Out or Last in First out
//
//This is a data structure which follows implementation as stacking

// Implementation of stack using Array

#include <stdio.h>

int top = -1;
int flag = 0;

int push(int *stack , int size) {
	if(top == size-1)
		return -1;
	else {
		int data;
		printf("Enter data : ");
		scanf("%d",&data);

		*(stack+(++top)) = data;

		return 0;
	}
}

int pop(int *stack , int size) {
	if(top == -1) {
		flag = 0;
		return -1;
	}else {
		flag = 1;
		int data = *(stack+top);
		top--;
		return data;
	}
}

int peek(int *stack , int size) {
	if(top == -1) {
		flag = 0;
		return -1;
	}else {
		flag = 1;
		return *(stack+top);
	}
}


void printStack(int *stack) {
	if(top == -1) {
		printf("Stack Empty");
	} else {
		int tmp = top;
		while(tmp != -1) {
			printf("|%d|\n",*(stack+tmp));
			tmp--;
		}
	}
}

void main() {
	int size;
	printf("Enter size of stack : ");
	scanf("%d",&size);

	int stack[size];

	char ch;
	do {
		printf("Operations\n");
		printf("1.Push\n");
		printf("2.Pop\n");
		printf("3.Peek\n");
		printf("4.PrintStack\n");

		int choice;
		printf("Enter choice : ");
		scanf("%d",&choice);

		switch(choice) {
			case 1:
				{
					int ret = push(stack,size);
					if(ret != -1) {
						printf("Successfully pushed\n");
					}else {
						printf("Stack overflow\n");
					}
				}
				break;
			case 2:
				{
					int ret = pop(stack,size);
					if(ret!=-1 && flag == 1) {
						printf("%d popped\n",ret);
					}else {
						printf("Stack underflow\n");
					}
				}
				break;
			case 3:
				{
					int ret = peek(stack,size);
					if(ret != -1 && flag == 1) {
						printf("%d at top\n",ret);
					}else {
						printf("Stack is empty\n");
					}
				}
				break;
			case 4:
				printStack(stack);
				break;
			default:
				printf("Invalid choice");
		}

		getchar();
		printf("Do you want to continue (y/n) : ");
		scanf("%c",&ch);
	}while(ch == 'y' || ch == 'Y');
}
