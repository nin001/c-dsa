
#include <stdio.h>
#include <stdlib.h>

struct node {
	int data;
	struct node *next;
};
struct node *head = NULL;

struct node* createNode() {
	struct node *newNode = (struct node*)malloc(sizeof(struct node));

	printf("Enter data : ");
	scanf("%d",&newNode->data);

	newNode->next = NULL;
	return newNode;
}

void addNode() {
	struct node *newNode = createNode();
	if(head == NULL)
		head = newNode;
	else {
		struct node *temp = head;
		while(temp->next != NULL)
			temp = temp->next;
		temp->next = newNode;
	}
	newNode->next = NULL;
}


void addFirst() {
	struct node *newNode = createNode();
	if(head == NULL)
		head = newNode;
	else {
		newNode->next = head;
		head = newNode;
	}
}

void addLast() {
	addNode();
}

int countNode() {
	if(head == NULL)
		return 0;
	else {
		int count = 0;
		struct node *temp = head;
		while(temp != NULL){
			count++;
			temp = temp->next;
		}
		return count;
	}
}

void addAtPos(int pos) {
       int count = countNode();
       if(pos<0 || pos > count+1)
	       printf("Invalid position\n");
       else {
	       if(pos == 1)
		       addFirst();
	       if(pos == count+1)
		       addNode();
	       else {
		       struct node *newNode = createNode();
		       struct node *temp = head;
		       
		       while(pos-2) {
			       temp = temp->next;
			       pos--;
		       }
		       
		       newNode->next = temp->next;
		       temp->next = newNode;
	       }
       }
}

void deleteFirst() {
	if(head == NULL) {
		printf("Linked list empty\n");
	}else {
		if(head->next == NULL) {
			free(head);
			head = NULL;
		}else {
			struct node *temp = head;
			head = temp->next;
			free(temp);
		}
	}
}

void deleteLast() {
	if(head == NULL) {
		printf("Linked list empty\n");
	}else {
		if(head->next == NULL) {
			free(head);
			head = NULL;
		}else {
			struct node *temp = head;
			while(temp->next->next != NULL)
				temp = temp->next;
			free(temp->next);
			temp->next = NULL;
		}
	}
}

void deleteAtPos(int pos) {
	int count = countNode();
	if(pos<0 || pos>count)
		printf("Invalid position\n");
	else {
		if(pos == 1)
			deleteFirst();
		else if(pos == count)
			deleteLast();
		else {
			struct node *temp = head;
			while(pos-2) {
				temp = temp->next;
				pos--;
			}

			struct node *temp1 = temp->next;
			temp->next = temp1->next;
			free(temp1);
		}
	}
}

void printLL() {
	if(head == NULL)
		printf("LL EMPTY\n");
	else {
		struct node *temp = head;
		while(temp->next != NULL) {
			printf("|%d|->",temp->data);
			temp = temp->next;
		}
		printf("|%d|\n",temp->data);
	}
}

int secLastOcc(int num) {
	if(head == NULL)
		return -1;
	else {
		if(head->next == NULL)
			return -1;
		else {
			int occ = -1;
			int sec = -1;
			int pos = 0;
			struct node *temp = head;
			while(temp != NULL) {
				pos++;
				if(temp->data == num){
					sec = occ;
					occ = pos;
				}
				temp = temp->next;
			}
			
			return sec;
		}
	}
}

void main() {
	
	char ch;
	do {
		printf("singly linked list\n");
		printf("1.addNode\n");
		printf("2.addFirst\n");
		printf("3.addLast\n");
		printf("4.addAtPos\n");
		printf("5.deleteFirst\n");
		printf("6.deleteLast\n");
		printf("7.deleteAtPos\n");
		printf("8.printLL\n");
		printf("Choose any option : ");

		int choice;
		scanf("%d",&choice);

		switch(choice) {
			case 1:
				addNode();
				break;
			case 2:
				addFirst();
				break;
			case 3:
				addLast();
				break;
			case 4:{
				int pos;
				printf("Enter position : ");
				scanf("%d",&pos);
				addAtPos(pos);
			       }break;
			case 5:
			       deleteFirst();
			       break;
			case 6:
			       deleteLast();
			       break;
			case 7:
			       {
				int pos;
				printf("Enter position : ");
				scanf("%d",&pos);
				deleteAtPos(pos);
			       }
			       break;
			case 8:
			       printLL();
			       break;
			default:
			       printf("invalid choice\n");
		}

		printf("Do you want to continue (y/n) : ");
		getchar();
		scanf("%c",&ch);
	}while(ch == 'y' || ch == 'Y');

	int num;
	printf("Enter num to find sec last occ in linked list : ");
	scanf("%d",&num);

	int ret = secLastOcc(num);
	if(ret != -1)
		printf("Sec last occ : %d\n",ret);

}
