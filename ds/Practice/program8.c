// Circular linked list number of nodes

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct node {
	int data;
	struct node *next;
};

struct node *head = NULL;

struct node* createNode() {
	struct node *newNode = (struct node*)malloc(sizeof(struct node));
	
	printf("Enter data : ");
	scanf("%d",&newNode->data);

	newNode->next = NULL;
}

void addNode() {
	struct node* newNode = createNode();

	if(head == NULL) {
		head = newNode;
		newNode->next = head;
	}else {
		struct node* temp = head;
		while(temp->next != head) 
			temp = temp->next;

		temp->next = newNode;
		newNode->next = head;
	}
}

int count() {
	if(head == NULL)
		return 0;
	else {
		int count = 0;
		struct node *temp = head;
		while(temp->next != head) {
			count++;
			temp = temp->next;
		}

		return ++count;
	}
}


void main() {
	int countNode;
	printf("Enter count : ");
	scanf("%d",&countNode);

	for(int i = 0 ; i<countNode ; i++) {
		addNode();
	}

	printf("Number of nodes in cirular linked list %d\n",count());

}
