//Linked list
//Singly linked list : 
 

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	char name[20];
	int id;
	struct Node *next;
}node;

node *head = NULL;

//create node function
node* createNode(){
	getchar();
	node *newNode = (node*)malloc(sizeof(node));
	printf("Enter Name : ");
	char ch;
	int i = 0;
	while((ch = getchar())!='\n'){
		(*newNode).name[i] = ch;
		i++;
	}
	printf("Enter id : ");
	scanf("%d",&newNode->id);	
	newNode->next = NULL;
}

//addnode function
void addNode(){
	node *newNode = createNode();
	if(head == NULL)
		head = newNode;
	else{
		node *temp = head;
		while(temp->next != NULL)
			temp = temp->next;
		temp->next = newNode;
	}
}

//addFirst
void addFirst(){
	node *newNode = createNode();
	if(head == NULL)
		head = newNode;
	else{
		newNode->next = head;
		head = newNode;
	}
}

//addLast
void addLast(){
	addNode();
}

//Count total number of nodes 
int count(){
	node *temp = head;
	int count = 0;
	while(temp!=NULL){
		count++;
		temp = temp->next;
	}return count;
}
//add Node at position
void addPos(int pos){
	int tot = count();
	if(pos<=(tot+1) && pos>0){
		if(pos == 1){
			addFirst();
		}else{
			node *newNode = createNode();
			node *temp = head;
			while(pos-2){
				pos--;
				temp = temp->next;
			}
			newNode->next = temp->next;
			temp->next = newNode;
		}
	}else{
		printf("Invalid Position\n");
	}
}


//display();
void display(){
	node *temp = head;
	while(temp!=NULL){
		printf("| %s %d |->",temp->name,temp->id);
		temp = temp->next;
	}printf("NULL\n");
}

void main(){
	printf("WELCOME TO LINKED LIST MENU\n");
	char choice;
	do{
		printf("--------------------\n");
		printf("| 1.addnode        |\n");
		printf("| 2.addFirst       |\n");
		printf("| 3.addLast        |\n");
		printf("| 4.addpos         |\n");
		printf("| 5.display        |\n");
		printf("--------------------\n");

		int ch;
		printf("\nEnter choice : ");
		scanf("%d",&ch);
		switch(ch){
			case 1:
				addNode();
				break;
			case 2:
				addFirst();
                                break;
			case 3:
				addLast();
                                break;
			case 4:
				{
				int pos;
				printf("Enter Position want to insert Node : ");
				scanf("%d",&pos);
				addPos(pos);
				}
                                break;
			case 5:
				display();
                                break;
			default:
				printf("Invalid choide BSDK\n");
		}
		
		getchar();
		printf("Do you want to continue (y/n): ");
		scanf("%c",&choice);
	}while(choice == 'Y' || choice == 'y');
	printf("Dhanyavad\n");


}
