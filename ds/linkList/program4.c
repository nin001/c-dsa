// linkedList 
// Linear data structure which has nodes which are connected through self referenctial structures 


//Accessing Nodes and filling values 
//making access function to display the code
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct Employee{
	int empid;
	char eName[20];
	float sal;
	struct Employee *next;
}Emp;

void access(Emp* ptr){
	printf("Employee name : %s\n",ptr->eName);
	printf("Employee id : %d\n",ptr->empid);
	printf("Employee sal : %f\n",ptr->sal);
}

void insert(Emp* ptr){
	ptr->empid = 1;
	strcpy(ptr->eName,"Niraj");
	ptr->sal = 30.00;
}
void main(){
	Emp *emp1 = (Emp*)malloc(sizeof(Emp));
	Emp *emp2 = (Emp*)malloc(sizeof(Emp));
	Emp *emp3 = (Emp*)malloc(sizeof(Emp));

	Emp *head = emp1;

	insert(emp1);
	emp1->next = emp2;
	

	insert(emp2);
	emp2->next = emp3;

	insert(emp3);
	emp3->next = NULL;

	access(emp1);
	access(emp2);
	access(emp3);
}
