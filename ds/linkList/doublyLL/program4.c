// Doubly linked list 
// reverse linked list using Inplace reverse

#include<stdio.h>
#include<stdlib.h>

typedef struct node{
	struct node *prev;
	int data;
	struct node *next;
}node;

node *head = NULL;

node* createNode(){
	node *newNode = (node*)malloc(sizeof(node));
	newNode->prev = NULL;
	
	printf("Enter data : ");
	scanf("%d",&newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){
	node *newNode = createNode();
	if(head == NULL){
		head = newNode;
	}else{
		node *temp = head;
		while(temp->next != NULL)
			temp = temp->next;
		temp->next = newNode;
		newNode->prev = temp;
	}
}

void inplaceRev(){
	if(head == NULL){
		printf("LINKED LIST IS EMPTY CANNOT REVERSE\n");
	}else{
		node *temp = NULL;
		while(head != NULL){
			temp = head->prev;
			head->prev = head->next;
			head->next = temp;
			head = head->prev;
		}
		head = temp->prev;
	}
}

void display(){
	if(head == NULL){
		printf("EMPTY LINKED LIST\n");
	}else{
		node *temp = head;
		while(temp->next != NULL){
			printf("|%d|->",temp->data);
			temp = temp->next;
		}printf("|%d|\n",temp->data);

	}
}

void main(){
	int ncount;
	printf("Enter number of nodes : ");
	scanf("%d",&ncount);

	for(int i = 0 ; i<ncount ; i++)
		addNode();

	display();
	inplaceRev();
	display();

}
