// problem statement
// write a prog that counts the number of times the number has occured in the linkedlist
#include<stdio.h>
#include<stdlib.h>

typedef struct node{
	int data;
	struct node *next;
}node;

node *head = NULL;
 
node* createNode(){
	node *newNode = (node*)malloc(sizeof(node));
	
	printf("Enter data : ");
	scanf("%d",&newNode->data);

	newNode->next = NULL;
}

void addNode(){
	node *newNode = createNode();
	if(head == NULL)
		head = newNode;
	else{
		node *temp = head;
		while(temp->next != NULL)
			temp = temp->next;
		temp->next = newNode;
	}
}

int display(){
	if(head == NULL){
		printf("Linked list is Empty\n");
		return -1;
	}else{
		node *temp = head;
		while(temp->next != NULL){
			printf("%d>",temp->data);
			temp = temp->next;
		}printf("%d\n",temp->data);
		
		return 0;
	}
}

int rep(int num){
	if(head == NULL)
		return -1;
	else{
		if(head->next == NULL){
			if(num == head->data)
				return 1;
			return 0;
		}else{
			int count = 0;
			node *temp = head;
			while(temp != NULL){
				if(temp->data == num)
					count++;
				temp = temp->next;
			}
			return count;
		}
	}
}

void main(){
	int nodeCount;
	printf("Enter number of nodes : ");
	scanf("%d",&nodeCount);

	for(int i = 0 ; i<nodeCount ; i++)
		addNode();

	printf("Linked list : ");
	display();

	int num;
	printf("Enter number : ");
	scanf("%d",&num);
	printf("%d ",num);
	num = rep(num);
	if(num != -1)
		printf("occured %d times\n",num);
	else
		printf("Linked list Empty\n");

}
