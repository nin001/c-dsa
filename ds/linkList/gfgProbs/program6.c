// program 6
// Exchange first and last nodes in circular linkedlist
#include<stdio.h>
#include<stdlib.h>

typedef struct node{
	int data;
	struct node *next;
}node;

node *head = NULL;

node* createNode(){
	node *newNode = (node*)malloc(sizeof(node));

	printf("Enter data : ");
	scanf("%d",&newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){
	node *newNode = createNode();
	if(head == NULL){
		head = newNode;
		newNode->next = head;
	}else{
		node *temp = head;
		while(temp->next != head)
			temp = temp->next;
		temp->next = newNode;
		newNode->next = head;
	}
}

void printLL(){
	if(head == NULL)
		printf("EMPTY\n");
	else{
		node *temp = head;
		while(temp->next != head){
			printf("|%d|->",temp->data);
			temp = temp->next;
		}printf("|%d|\n",temp->data);
	}
}
/*
int exchange_FL(){
	if(head == NULL)
		return -1;
	else{
		if(head->next == head)
			return head->data;
		else{
			int tempFirst = head->data;
			node *temp = head;
			while(temp->next != head){
				temp = temp->next;
			}

			int data = temp->data;
			temp->data = tempFirst;
			head->data = data;
		}
		return 0;
	}
}
*/
int exchange_FL(){
	if(head == NULL)
		return -1;
	else{
		if(head->next == head)
			return head->data;
		else{
			node *tempFirst = head;
			node *temp = head;
			while(temp->next->next != head){
				temp = temp->next;
			}
			
			temp->next->next = tempFirst->next;
			head = temp->next;
			temp->next = tempFirst;
			tempFirst->next = head;

		}
		return 0;
	}
}



void main(){
	int countNode;
	printf("Enter number of nodes : ");
	scanf("%d",&countNode);

	for(int i = 0 ; i<countNode; i++)
		addNode();

	printLL();
	int ret = exchange_FL();
	if(ret == -1 || ret != 0 )
		printf("Invalid\n");
	else
		printf("Successful\n");
	printLL();
}
