// problem statement
// convert singly linkedlist into a circular linkedlist
#include<stdio.h>
#include<stdlib.h>

typedef struct node{
	int data;
	struct node *next;
}node;

node *head = NULL;

node* createNode(){
	node *newNode = (node*)malloc(sizeof(node));

	printf("Enter data : ");
	scanf("%d",&newNode->data);

	newNode->next = NULL;
}

void addNode(){
	node *newNode = createNode();
	if(head == NULL)
		head = newNode;
	else{
		node *temp = head;
		while(temp->next != NULL)
			temp = temp->next;
		temp->next = newNode;
	}
}

void displayCLL(){
	node *temp = head;
	while(temp->next != head){
		printf("%d>",temp->data);
		temp = temp->next;
	}printf("%d\n",temp->data);
}

void llConverter(){
	if(head == NULL)
		printf("String already empty\n");
	else{
		node *temp = head;
		while(temp->next != NULL)
			temp = temp->next;
		temp->next = head;
		displayCLL();
	}
}



void main(){
	int size;
	printf("Enter linked list size : ");
	scanf("%d",&size);

	for(int i = 0 ; i<size ; i++)
		addNode();

	printf("Converting singly linkedlist to circular linkedlist..\n");
	llConverter();
	
}
