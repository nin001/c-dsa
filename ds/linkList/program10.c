//Linked list
//Singly linked list : 
 

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	char name[20];
	int id;
	struct Node *next;
}node;

node *head = NULL;

//create node function
node* createNode(){
	getchar();
	node *newNode = (node*)malloc(sizeof(node));
	printf("Enter Name : ");
	char ch;
	int i = 0;
	while((ch = getchar())!='\n'){
		(*newNode).name[i] = ch;
		i++;
	}
	printf("Enter id : ");
	scanf("%d",&newNode->id);	
	newNode->next = NULL;
}

//addnode function
void addNode(){
	node *newNode = createNode();
	if(head == NULL)
		head = newNode;
	else{
		node *temp = head;
		while(temp->next != NULL)
			temp = temp->next;
		temp->next = newNode;
	}
}

//addFirst
void addFirst(){
	node *newNode = createNode();
	if(head == NULL)
		head = newNode;
	else{
		newNode->next = head;
		head = newNode;
	}
}

//addLast
void addLast(){
	addNode();
}

//Count total number of nodes 
int countNode(){
	node *temp = head;
	int count = 0;
	while(temp!=NULL){
		count++;
		temp = temp->next;
	}return count;
}
//add Node at position
int addPos(int pos){
	int tot = countNode();
	if(pos <= 0 || pos>(tot+1)){
		printf("Invalid position\n");
		return -1;
	}else{
		if(pos==1){
			addFirst();
		}else if(pos == tot+1){
			addLast();
		}else{
			node *newNode = createNode();
			node *temp = head;
			while(pos-2){
				temp = temp->next;
			}
			newNode->next = temp->next;
			temp->next = newNode;
		}
		return 0;
	}
}

//delete first
void deleteFirst(){
	if(head != NULL){
		node *temp = head;
		if(head->next == NULL){
			head = NULL;
			free(temp);
		}else{
			head = temp->next;
			free(temp);
		}
	}else{
		printf("Linked List already Empty\n");
	}
}

//delete Last
void deleteLast(){
	if(head != NULL){
		node *temp = head;
		if(head->next == NULL){
			head = NULL;
			free(temp);
		}else{
			while(temp->next->next != NULL)
				temp = temp->next;
			free(temp->next);
			temp->next = NULL;
		}
	}else{
		printf("Linked list already Empty\n");
	}
}

//delete at Position
void deleteAtPos(int pos){
	int count = countNode();
	if(pos<=0 || pos>count){
		printf("INVALID POSITION\n");
	}else{
		if(pos == 1)
			deleteFirst();
		else if(pos == count)
			deleteLast();
		else{
			node *temp = head;
			while(pos-2)
				temp = temp->next;
			node *ptr = temp->next;
			temp->next = temp->next->next;
			free(ptr);
		}
	}
}

	
//display();
void display(){
	node *temp = head;
	while(temp!=NULL){
		printf("| %s %d |->",temp->name,temp->id);
		temp = temp->next;
	}printf("NULL\n");
}

void main(){
	printf("WELCOME TO LINKED LIST MENU\n");
	char choice;
	do{
		printf("--------------------\n");
		printf("| 1.addnode        |\n");
		printf("| 2.addFirst       |\n");
		printf("| 3.addLast        |\n");
		printf("| 4.addpos         |\n");
		printf("| 5.display        |\n");
		printf("| 6.deleteFirst    |\n");
		printf("| 7.deleteLast     |\n");
		printf("| 8.deleteAtPos    |\n");
		printf("--------------------\n");

		int ch;
		printf("\nEnter choice : ");
		scanf("%d",&ch);
		switch(ch){
			case 1:
				addNode();
				break;
			case 2:
				addFirst();
                                break;
			case 3:
				addLast();
                                break;
			case 4:
				{
				int pos;
				printf("Enter Position want to insert Node : ");
				scanf("%d",&pos);
				addPos(pos);
				}
                                break;
			case 5:
				display();
                                break;
			case 6:
                                deleteFirst();
                                break;
			case 7:
                                deleteLast();
                                break;
			case 8:
				{
				int pos;
				printf("Enter position want to delete : ");
				scanf("%d",&pos);
				deleteAtPos(pos);
				}
                                break;

			default:
				printf("Invalid choide BSDK\n");
		}
		
		getchar();
		printf("Do you want to continue (y/n): ");
		scanf("%c",&choice);
	}while(choice == 'Y' || choice == 'y');
	printf("Dhanyavad\n");


}
