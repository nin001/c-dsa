// Linked list practice
// singly linked list

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node * next;
}Node;

Node *head = NULL;

Node* createNode(){
	Node *newNode = (Node*)malloc(sizeof(Node));

	printf("Enter data : ");
	scanf("%d",&newNode->data);

	newNode->next = NULL;

	return newNode;
}

void AddNode(){
	Node *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{
		Node* temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
	}
}

void AddLast(){
	AddNode();
}

void AddFirst(){
	Node *newNode = createNode();
	if(head == NULL){
		head = newNode;
	}else{
		Node *temp = head;
		head = newNode;
		newNode->next = temp;
	}
}

int countNode(){
	if(head == NULL)
		return 0;
	else{
		int count = 0;
		Node *temp = head;
		while(temp != NULL){
			count++;
			temp = temp->next;
		}
		return count;
	}
}

void AddAtPos(int pos){
	int count = countNode();
	if(pos<=0 || pos>count+1){
		printf("Invalid Position\n");
	}else{
		if(pos == 1)
			AddFirst();
		else if(pos == count+1)
			AddLast();
		else{
			Node *newNode = createNode();
			Node *temp = head;
			while(pos-2){
				temp = temp->next;
				pos--;
			}
			newNode->next = temp->next;
			temp->next = newNode;
		}
	}
}

void DelNode(){
	if(head == NULL){
		printf("Invalid Operation LL empty\n");
	}else{
		if(head->next == NULL){
			free(head);
			head = NULL;
		}else{
			Node *temp = head;
			while(temp->next->next != NULL){
				temp = temp->next;
			}
			free(temp->next);
			temp->next = NULL;
		}
	}
}

void DelFirst(){
	if(head == NULL){
		printf("Invalid Operation LL Empty\n");
	}else{
		if(head->next == NULL){
			free(head);
			head = NULL;
		}else{
			Node *temp = head;
			head = head->next;

			free(temp);
		}
	}
}

void DelLast(){
	DelNode();
}

void DelAtPos(int pos){
	int count = countNode();

	if(pos<=0 || pos>count){
		printf("Invalid position\n");
	}else{
		if(pos == 1)
			DelFirst();
		else if(pos == count)
			DelLast();
		else{
			Node *temp = head;
			while(pos-2){
				temp = temp->next;
				pos--;
			}
			Node *temp1 = temp->next;
			temp->next = temp1->next;
			free(temp1);
		}
	}
}
			
void printLL(){
	if(head == NULL){
		printf("EMPTY LL\n");
	}else{
		Node *temp = head;
		while(temp->next != NULL){
			printf("|%d|->",temp->data);
			temp = temp->next;
		}
		printf("|%d|\n",temp->data);
	}
}

void main(){
	char choice;
	do{
		printf("1.AddNode\n");
		printf("2.AddLast\n");
		printf("3.AddFirst\n");
		printf("4.AddAtPos\n");
		printf("5.DelNode\n");
		printf("6.DelLast\n");
		printf("7.DelFirst\n");
		printf("8.DelAtPos\n");
		printf("9.DisplayLL\n");
		printf("Enter choice : ");

		int ch;
		scanf("%d",&ch);

		switch(ch){
			case 1:
				AddNode();
				printLL();
				break;
			case 2:
				AddLast();
				printLL();
				break;
			case 3:
				AddFirst();
				printLL();
				break;

			case 4:{
				int pos;
				printf("Enter position : ");
				scanf("%d",&pos);
				AddAtPos(pos);
				printLL();
			       }
				break;
			case 5:
				DelNode();
				printLL();
				break;
			case 6:
				DelLast();
				printLL();
				break;
			case 7:
				DelFirst();
				printLL();
				break;
			case 8:{
				int pos;
				printf("Enter position : ");
				scanf("%d",&pos);
				DelAtPos(pos);
				printLL();
			       }
				break;
			case 9:
				printLL();
				break;
			default:
				printf("Invalid Choice");
		}

		printf("Do you want to contine (y/n) :");
		getchar();
		scanf("%c",&choice);

	}while(choice == 'Y' || choice == 'y');
}	

		
