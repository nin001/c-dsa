// Linked list 
// Prob statement given by sir
// implementation of prority queue using linkedlist

#include<stdio.h>
#include<stdlib.h>

typedef struct node{
	int data;
	int prio;
	struct node *next;
}node;

node *head = NULL;
int flag = 0;

node* createNode(){
	node *newNode = (node*)malloc(sizeof(node));
	int ch = 1;
	do{
		printf("Enter Priority : ");
		scanf("%d",&newNode->prio);
		if(newNode->prio<0 || newNode->prio >5)
			printf("Invalid priority\n");
		else
			ch = 0;
	}while(ch);

	printf("Enter data : ");
	scanf("%d",&newNode->data);

	newNode->next = NULL;

	return newNode;

}

int enqueue(){
	node *newNode = createNode();
	if(head == NULL){
		head = newNode;
	}else{
		node *temp = head;
		node *prev = NULL;
		while(temp->next != NULL){
			if(newNode->prio>=temp->prio)
				break;
			prev = temp;
			temp = temp->next;
		}
		if(prev == NULL){
			head = newNode;
			newNode->next = temp;
		}else{
			prev->next = newNode;
			newNode->next = temp;
		}
	}
	return 0;
}

int dequeue(){
	if(head == NULL){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		node *temp = head;
		int val = temp->data;
		head = temp->next;
		free(temp);

		return val;
	}
}

void printQ(){
	if(head == NULL)
		printf("Q is empty\n");
	else{
		node *temp = head;
		while(temp->next != NULL){
			printf("|%d|->",temp->data);
			temp = temp->next;
		}
		printf("|%d|\n",temp->data);

	}
}


void main(){
	int nodeCount;
	printf("Enter Node count : ");
	scanf("%d",&nodeCount);

	for(int i = 0 ; i<nodeCount ; i++)
		enqueue();

	printQ();

	int ret = dequeue();
	if(flag == 0)
		printf("Underflow\n");
	else
		printf("%d dequeued\n",ret);



}
