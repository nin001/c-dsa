// Singly linkedlist reverse using inplace reverse

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
}node;

node *head = NULL;

node* createNode(){
	node *newNode = (node*)malloc(sizeof(node));
	printf("Enter data : ");
	scanf("%d",&newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){
	node *newNode = createNode();
	if(head == NULL)
		head = newNode;
	else{
		node *temp = head;
		while(temp->next != NULL)
			temp = temp->next;
		temp->next = newNode;
	}
}

int rev(){
	if(head == NULL)
		return -1;
	else{
		node *temp1 = NULL;
		node *temp2 = NULL;

		while(head != NULL){
			temp2 = head->next;
			head->next = temp1;
			temp1 = head;
			head = temp2;
		}
		head = temp1;
	}
}

void display(){
	if(head == NULL)
		printf("LINKED LIST IS EMPTY\n");
	else{
		node *temp = head;
		while(temp->next != NULL){
			printf("|%d|->",temp->data);
			temp = temp->next;
		}printf("|%d|\n",temp->data);
		
	}
}


void main(){
	int countNode;
	printf("Enter node coount : ");
	scanf("%d",&countNode);

	for(int i = 0 ; i<countNode ; i++)
		addNode();

	display();
	rev();
	display();
}
