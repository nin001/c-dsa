//Problem given by sir
//return the position of the node where number taken from user has occured second last

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
}node;

node *head = NULL;

node* createNode(){
	node *newNode = (node*)malloc(sizeof(node));
	printf("Enter data : ");
	scanf("%d",&newNode->data);
	newNode->next = NULL;
	return newNode;
}

void addNode(){
	node *newNode = createNode();
	if(head == NULL)
		head = newNode;
	else{
		node *temp = head;
		while(temp->next != NULL)
			temp = temp->next;
		temp->next = newNode;
	}
}

void display(){
	node *temp = head;
	while(temp != NULL){
		printf("| %d |->",temp->data);
		temp = temp->next;
	}printf("NULL\n");
}
int countNode(){
	int count = 0;
	node *ptr = head;
	while(ptr!=NULL){
		ptr = ptr->next;
		count++;
	}return count;
}


int OccCheck(int num){
	if(head == NULL || head->next == NULL){
		printf("Not enough nodes \n");
		return -1;
	}else{
		node *temp = head;
		int arr[countNode()];
		int i = 0;
		int pos = 1;
		int count = 0;
		while(temp!=NULL){
		       if(temp->data == num){
			       arr[i] = pos;
			       i++;
			       count++;
		       }
		       temp = temp->next;
		       pos++;
		}
		if(head == NULL){
			printf("Np nodes present\n");
			return -1;
		}else if(head->next == NULL){
			printf("Only one Node present.. No repetition possible\n");
			return -1;
		}else if(count == 1){
			printf("%d occur only once\n",num);
			return -1;
		}else if(count == 0){
			printf("%d doesnt exist in nodes\n",num);
			return -1;
		}else{
			printf("Second last occurance of %d is at position : %d\n",num,arr[i-2]);
			return arr[i-1];
		}
	}
}



void main(){
	int n;
	printf("How many nodes you want to enter : ");
	scanf("%d",&n);
	for(int i = 0 ; i<n ; i++){
		addNode();
	}
	display();
	
	int num;
	printf("Enter Number To Check Its Second Occurance : ");
	scanf("%d",&num);
	OccCheck(num);
}
	
		


















