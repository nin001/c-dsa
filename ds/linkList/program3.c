// linkedList 
// Linear data structure which has nodes which are connected through self referenctial structures 


//Accessing Nodes and filling values 
//making access function to display the code
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct Employee{
	int empid;
	char eName[20];
	float sal;
	struct Employee *next;
}Emp;

void access(Emp* ptr){
	printf("Employee name : %s\n",ptr->eName);
	printf("Employee id : %d\n",ptr->empid);
	printf("Employee sal : %f\n",ptr->sal);
}

void main(){
	Emp *emp1 = (Emp*)malloc(sizeof(Emp));
	Emp *emp2 = (Emp*)malloc(sizeof(Emp));
	Emp *emp3 = (Emp*)malloc(sizeof(Emp));

	Emp *head = emp1;

	emp1->empid = 1;
	strcpy(emp1->eName,"Niraj");
	emp1->sal = 60.0;
	emp1->next = emp2;
	
	emp2->empid = 2;
	strcpy(emp2->eName,"raj");
	emp2->sal = 61.0;
	emp2->next = emp3;

	emp3->empid = 3;
	strcpy(emp3->eName,"Nin");
	emp3->sal = 65.0;
	emp3->next = NULL;

	access(emp1);
	access(emp2);
	access(emp3);
}
