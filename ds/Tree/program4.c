// level order traversal using linked list

#include <stdio.h>
#include <stdlib.h>
#include<stdbool.h>

struct TreeNode {
	struct TreeNode *left;
	int data;
	struct TreeNode *right;
};

struct Queue {
	struct TreeNode *TreePtr;
	struct Queue *next;
};

struct Queue *front = NULL;
struct Queue *rear = NULL;

bool isEmpty();

void enQueue(struct TreeNode *root) {
	struct Queue *temp = (struct Queue *)malloc(sizeof(struct Queue));
	temp->TreePtr = root;
	temp->next = NULL;

	if(isEmpty()){
		front = rear = temp;
	}else{
		rear->next = temp;
		rear = temp;
	}
}

struct TreeNode *deQueue(struct TreeNode *root) {
	if(isEmpty()) {
		printf("Queue is empty\n");
	}else{
		struct Queue *temp = front;
		struct TreeNode *itmPtr = temp->TreePtr;
		if(front == rear)
			front = rear = NULL;
		else{
			front = temp->next;
		}
		free(temp);
		return itmPtr;
	}
}

bool isEmpty(){
	if(front == NULL && rear == NULL)
		return true;
	else
		return false;
}

void levelOrderUsingQueue(struct TreeNode *root) {
	if(root == NULL) {
		printf("Tree is empty\n");
	}else{
		struct TreeNode *temp = root;
		enQueue(temp);
		while(!isEmpty()) {
			temp = deQueue(temp);
			printf("%d ",temp->data);
			if(temp->left != NULL)
				enQueue(temp->left);
			if(temp->right != NULL)
				enQueue(temp->right);
		}
	
	}
				
}

void main() {
	struct TreeNode *root = (struct TreeNode*)malloc(sizeof(struct TreeNode));

	root->data = 1;
	root->left = (struct TreeNode *)malloc(sizeof(struct TreeNode));
	root->left->data = 2;

	root->right = (struct TreeNode *)malloc(sizeof(struct TreeNode));
	root->right->data = 3;

	root->left->left = (struct TreeNode *)malloc(sizeof(struct TreeNode));
	root->left->left->data = 4;

	root->left->right = (struct TreeNode *)malloc(sizeof(struct TreeNode));
	root->left->right->data = 5;

	root->right->left = (struct TreeNode *)malloc(sizeof(struct TreeNode));
	root->right->left->data = 6;

	root->right->right = (struct TreeNode *)malloc(sizeof(struct TreeNode));
	root->right->right->data = 7;

	levelOrderUsingQueue(root);
	printf("\n");
}
	
