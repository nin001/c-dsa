// implimenting queue in linked list

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
}node;

node *head = NULL;
int size;
int qSize = 0;
int flag = 0;

int enQ(){
	if(qSize == size)
		return -1;
	else{
		node *newnode = (node*)malloc(sizeof(node));
		printf("Enter data : ");
		scanf("%d",&newnode->data);
		if(head == NULL){
			head = newnode;
		}else{
			node *temp = head;
			while(temp->next != NULL)
				temp = temp->next;
			temp->next = newnode;
		}
		qSize++;
		return 0;
	}
}

int dQ(){
	if(qSize == 0){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		int retV = head->data;
		node* temp = head;
		head = head->next;
		free(temp);
		qSize--;
		return retV;
	}
}

int front(){
	if(qSize == 0){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		return head->data;
	}
}

void PrintQ(){
	if(head == NULL)
		printf("Queue Empty\n");
	else{
		node *temp = head;
		while(temp->next != NULL){
			printf("|%d|",temp->data);
			temp = temp->next;
		}
		printf("|%d|\n",temp->data);
	}
}

void main(){
	printf("Enter size of Queue : ");
	scanf("%d",&size);

	char choice;
	do{
		printf("1.Enqueu\n");
		printf("2.Dequeu\n");
		printf("3.Front\n");
		printf("4.PrintQ\n");
		printf("Enter choice : ");
		
		int ch;
		scanf("%d",&ch);

		switch(ch){
			case 1:{
				int ret = enQ();
				if(ret == -1)
					printf("Queue Overflow\n");
				else
					printf("Enqueued Successfully\n");
			       }
			       break;
			case 2:{
				int ret = dQ();
				if(flag == 0)
					printf("Queue UnderFlow\n");
				else
					printf("%d dequeued\n",ret);
			       }
			       break;
			case 3:{
				int ret = front();
				if(flag == 0)
					printf("Queue is empty\n");
				else
					printf("Front = %d\n",ret);
			       }
			       break;
			case 4:
			       PrintQ();
			       break;
			default:
			       printf("Invalid Choice\n");
		}

		getchar();
		printf("Do You want to continue\n");
		scanf("%c",&choice);
	}while(choice == 'Y' || choice == 'y');
}
			       
