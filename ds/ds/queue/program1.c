// Queue
// It is data structure which works on FIFO (first in first out)
// there are 4 types of queue
// 1. Linear 
// 2. Circular
// 3. Priority
// 4. DeQue

// Implementation of queue with help of array
#include<stdio.h>

int size;
int front = -1;
int rear = -1;
int flag = 0;

int enqueue(int que[]){
	if(rear == size-1)
		return -1;
	else{
		if(front == -1)
			front++;
		rear++;

		printf("Enter data : ");
		scanf("%d",&que[rear]);

		return 0;
	}
}

int dequeue(int que[]){
	if(front == -1){
		flag = 0;
		return -1;
	}else{
		int val = que[front];
		flag = 1;
		if(front == rear){
			rear = -1;
			front = -1;
		}else{
			front++;
		}

		return val;
	}
}

int frontt(int que[]){
	if(front == -1){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		return que[front];
	}
}

void printQ(int que[]){
	if(front == -1)
		printf("Queue is Empty\n");
	else{
		for(int i = front ; i<=rear ; i++)
			printf("|%d| ",que[i]);
		printf("\n");
	}
}

void main(){
	printf("Enter size of Queue : ");
	scanf("%d",&size);

	int queue[size];

	char ch;
	do{
		printf("Choose an option\n");
		printf("1.Enqueue\n");
		printf("2.Dequeue\n");
		printf("3.Front\n");
		printf("4.PrintQ\n");

		int choice;
		printf("Enter your choice : ");
		scanf("%d",&choice);

		switch(choice){
			case 1 :{
				int ret = enqueue(queue);
				if(ret == -1)
					printf("Queue overflow\n");
				}
				break;
			case 2 :{
				int ret = dequeue(queue);
				if(flag == 0)
					printf("Queue underflow\n");
				else
					printf("%d dequeued\n",ret);
				}
				break;
			case 3 :{
				int ft = frontt(queue);
				if(flag == 0)
					printf("Queue is Empty\n");
				else
					printf("%d at front\n",ft);
				}
				break;

			case 4 :
				printQ(queue);
				break;

			default :
				printf("Invalid choice\n");
		}
		getchar();
		printf("Do you want to continue(y/n) : ");
		scanf("%c",&ch);
	}while(ch == 'y' || ch == 'Y');
	printf("Dhanyawad\n");
}
