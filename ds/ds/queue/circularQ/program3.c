// circular Q practice 
// using array

#include<stdio.h>

int size;
int front = -1;
int rear = -1;
int flag = 0;

int enQ(int Q[]){
	if((front == 0 && rear == size-1) || (front - rear == 1)){
		return -1;
	}else{
		if(front == -1)
			front++;
		else
			if(rear == size-1 && front != 0)
				rear = -1;
		rear++;
		printf("Enter data : ");
		scanf("%d",(Q+rear));
		return 0;
	}
}

int dQ(int Q[]){
	if(front == -1){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		int ret = *(Q+front);
		if(front == rear){
			front = -1;
			rear = -1;
		}else{
			if(front == size-1 && rear<front)
				front = -1;
			front++;
		}
		return ret;
	}
}

int frontVal(int Q[]){
	if(front == -1){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		return *(Q+front);
	}
}

void PrintQ(int Q[]){
	if(front == -1){
		printf("Queue is empty\n");
	}else{
		int itr = front;
		while(itr != rear){
			if(itr == size)
				itr = 0; 
			printf("|%d|",*(Q+itr));
			itr++;
		}
		printf("|%d|\n",*(Q+itr));
	}
}

void main(){
        printf("Enter size of Queue : ");
        scanf("%d",&size);
	
	int Q[size];

        char choice;
        do{
                printf("1.Enqueu\n");
                printf("2.Dequeu\n");
                printf("3.Front\n");
                printf("4.PrintQ\n");
                printf("Enter choice : ");

                int ch;
                scanf("%d",&ch);

                switch(ch){
                        case 1:{
                                int ret = enQ(Q);
                                if(ret == -1)
                                        printf("Queue Overflow\n");
                                else
                                        printf("Enqueued Successfully\n");
                               }
                               break;
                        case 2:{
                                int ret = dQ(Q);
                                if(flag == 0)
                                        printf("Queue UnderFlow\n");
                                else
                                        printf("%d dequeued\n",ret);
                               }
                               break;
                        case 3:{
                                int ret = frontVal(Q);
                                if(flag == 0)
                                        printf("Queue is empty\n");
                                else
                                        printf("Front = %d\n",ret);
                               }
                               break;
                        case 4:
                               PrintQ(Q);
                               break;
                        default:
                               printf("Invalid Choice\n");
                }

                getchar();
                printf("Do You want to continue\n");
                scanf("%c",&choice);
        }while(choice == 'Y' || choice == 'y');
}

