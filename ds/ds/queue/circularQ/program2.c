// Implementation of queue in linked list

#include<stdio.h>
#include<stdlib.h>

typedef struct Queue{
	int data;
	struct Queue *next;
}que;

que *front = NULL;
que *rear = NULL;
int size;
int count = 0;
int flag = 0;

int enqueue(){
	if(count == size)
		return -1;
	else{
		count++;
		que *node = (que*)malloc(sizeof(que));
		printf("Enter data : ");
		scanf("%d",&node->data);
		node->next = NULL;
		if(front == NULL){
			front = node;
			rear = node;
			node->next = front;
		}else{
			rear->next = node;
			rear = node;
			rear->next = front;
		}
		return 0;
	}
}

int dequeue(){
	if(count == 0){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		count--;
		int val = front->data;
		if(front == rear){
			free(front);
			front = NULL;
			rear = NULL;
		}else{
			que *temp = front;
			front = front->next;
			rear->next = front;
			free(temp);
		}
		return val;
	}
}

int frontt(){
	if(front == NULL){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		return front->data;
	}
}
		

void printQ(){
	if(front == NULL)
		printf("Queue is Empty\n");
	else{
		que *temp = front;
		while(temp->next != front){
			printf("|%d| ",temp->data);
			temp = temp->next;
		}printf("|%d|\n",temp->data);

	}
}

void main(){
        printf("Enter size of Queue : ");
        scanf("%d",&size);

        char ch;
        do{
                printf("Choose an option\n");
                printf("1.Enqueue\n");
                printf("2.Dequeue\n");
                printf("3.Front\n");
                printf("4.PrintQ\n");

                int choice;
                printf("Enter your choice : ");
                scanf("%d",&choice);

                switch(choice){
                        case 1 :{
                                int ret = enqueue();
                                if(ret == -1)
                                        printf("Queue overflow\n");
                                }
                                break;
                        case 2 :{
                                int ret = dequeue();
                                if(flag == 0)
                                        printf("Queue underflow\n");
                                else
                                        printf("%d dequeued\n",ret);
                                }
                                break;
                        case 3 :{
                                int ft = frontt();
                                if(flag == 0)
                                        printf("Queue is Empty\n");
                                else
                                        printf("%d at front\n",ft);
                                }
                                break;

                        case 4 :
                                printQ();
                                break;

                        default :
                                printf("Invalid choice\n");
                }
                getchar();
                printf("Do you want to continue(y/n) : ");
                scanf("%c",&ch);
        }while(ch == 'y' || ch == 'Y');
        printf("Dhanyawad\n");
}

