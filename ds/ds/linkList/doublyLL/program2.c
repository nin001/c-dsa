//Doubly linked list:
//Real time example

#include<stdio.h>
void* malloc(size_t);
void free(void *);

typedef struct Node{
	struct Node *prev;
	int id;
	char name[20];
	float avg;
	int course;
	struct Node *next;
}Node;

Node *head = NULL;

Node* createNode(){
	Node *newNode = (Node*)malloc(sizeof(Node));
	newNode->prev = NULL;

	printf("Enter id : ");
	scanf("%d",&newNode->id);
	printf("Enter Name : ");
	char ch;
	int i = 0;
	while((ch = getchar()) != '\n'){
		(*newNode).name[i] = ch;
		i++;
	}

	printf("Enter avg performance : ");
	scanf("%f",&newNode->avg);
	printf("Enter number of courses : ");
	scanf("%d",&newNode->course);

	newNode->next = NULL;

	return newNode;
}

void addNode(){
	Node *newNode = createNode();
	if(head == NULL)
		head = newNode;
	else{
		Node *temp = head;
		while(temp->next != NULL)
			temp = temp->next;
		temp->next = newNode;
		newNode->prev = temp;
	}
}

int countNode(){
	int count = 0;
	Node *temp = head;
	while(temp != NULL){
		count++;
		temp = temp->next;
	}return count;
}

void addFirst(){
	if(head == NULL){
		addNode();
	}else{
		Node *newNode = createNode();
		newNode->next = head;
		head->prev = newNode;
		head = newNode;
	}
}

int addAtPos(int pos){
        int count = countNode();
        if(pos<=0 || pos>count+1){
                printf("Invalid Position\n");
                return -1;
        }else{
                if(pos == 1)
			addFirst();
		else if(pos == count+1){
			addNode();
		}else{
			Node *newNode = createNode();
			Node *temp = head;
			while(pos-2){
				pos--;
				temp = temp->next;
			}
			newNode->next = temp->next;
			temp->next->prev = newNode;
			temp->next = newNode;
			newNode->prev = temp;
		}return 0;
	}
}

int deleteFirst(){
	if(head == NULL){
		printf("Already empty\n");
		return -1;
	}else{
		head = head->next;
		free(head->prev);
		head->prev = NULL;
		return 0;
	}
}

int deleteLast(){
	if(head == NULL){
		printf("Doubly linked list already empty\n");
		return -1;
	}else{
		if(head->next == NULL){
			deleteFirst();
		}else{
			Node *temp = head;
			while(temp->next->next != NULL)
				temp = temp->next;
			free(temp->next);
			temp->next = NULL;
		}return 0;
	}
}

int deleteLastNew(){
	if (head == NULL){
		printf("Doubly linked list is already empty\n");
		return -1;
	}else{
		if(head->next == NULL){
			deleteFirst();
		}else{
			Node *temp = head;
			while(temp->next != NULL)
				temp = temp->next;
			temp->prev->next = NULL;
			free(temp);
		}
		return 0;
	}
}

int deleteAtPos(int pos){
	int count = countNode();
	if(pos<=0 || pos>count){
		printf("Invalid Position\n");
		return -1;
	}else{
		if(pos == count)
			deleteLast();
		else if(pos == 1)
			deleteFirst();
		else{
			Node *temp = head;
			while(pos-2){
				pos--;
				temp = temp->next;
			}
			temp->next = temp->next->next;
			free(temp->next->prev);
			temp->next->prev = temp;
		}
	}
}
		

void display(){
	Node *temp = head;
	while(temp->next!=NULL){
		printf("|%d %s %f %d|->",temp->id,temp->name,temp->avg,temp->course);
		temp = temp->next;
	}printf("|%d %s %f %d|\n",temp->id,temp->name,temp->avg,temp->course);
}


void main(){
	printf("Welcome to Doubly Linked List Menu\n");
	char choice;
	do{
		printf("-------------------\n");
		printf("|1.AddNode        |\n");
		printf("|2.AddFirst       |\n");
		printf("|3.AddAtPos       |\n");
		printf("|4.DeleteFirst    |\n");
		printf("|5.DeleteLast     |\n");
		printf("|6.DeleteAtPos    |\n");
		printf("|7.Display        |\n");
		printf("-------------------\n");

		int ch;
		printf("Enter Choice : ");
		scanf("%d",&ch);


		switch(ch){
			case 1:
				addNode();
				break;
			case 2:
				addFirst();
				break;
			case 3:{
				int pos;
				printf("Enter position To Add : ");
				scanf("%d",&pos);
				addAtPos(pos);
			       }
				break;
			case 4:
				deleteFirst();
				break;
			case 5:
				deleteLastNew();
				break;
			case 6:
				{
				int pos;
				printf("Enter position to delete : ");
				scanf("%d",&pos);
				deleteAtPos(pos);
				}
				break;
			case 7:
				display();
				break;
			default:
				printf("Invalid Choice\n");
		}

		getchar();
		printf("Do You Want To Continue (y/n) : ");
		scanf("%c",&choice);
	}while(choice == 'Y' || choice == 'y');
	
	printf("Dhanyawad\n");

}
