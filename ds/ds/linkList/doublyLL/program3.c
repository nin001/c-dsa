// Doubly linked list 
// reverse with swap(data)
#include<stdio.h>
#include<stdlib.h>

typedef struct node{
	struct node *prev;
	int data;
	struct node *next;
}node;

node *head = NULL;

node* createNode(){
	node *newNode = (node*)malloc(sizeof(node));

	newNode->prev = NULL;

	printf("Enter data : ");
	scanf("%d",&newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){
	node *newNode = createNode();
	if(head == NULL){
		head = newNode;
	}else{
		node *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
		newNode->prev = temp;
	}


}


int display(){
	if(head == NULL){
		printf("Linked list is Empty\n");
		return -1;
	}else{
		node *temp = head;
		while(temp->next != NULL){
			printf("|%d|->",temp->data);
			temp = temp->next;
		}printf("|%d|\n",temp->data);

		return 0;
	}
}

int countNode(){
	if(head == NULL)
		return 0;
	else if(head->next == NULL)
		return 1;
	else{
		node *temp = head;
		int count = 0;
		while(temp != NULL){
			count++;
			temp = temp->next;
		}
		return count;
	}
}


void revLL(){
	if(head == NULL){
		printf("Empty linked list\n");
	}else{
		if(head->next == NULL){
			printf("Only one node present\n");
			display();
		}else{
			node *temp1 = head;
			node *temp2 = head;
			while(temp2->next != NULL)
				temp2 = temp2->next;

			int count = countNode()/2;
			while(count){
				int data = temp2->data;
				temp2->data = temp1->data;
				temp1->data = data;

				temp1 = temp1->next;
				temp2 = temp2->prev;

				count--;
			}

			printf("String reversed : ");
			display();
		}
	}
}

			

void main(){
	int countNode;
	printf("Enter number of nodes : ");
	scanf("%d",&countNode);

	for(int i = 0 ; i<countNode ; i++)
		addNode();
	
	display();

	revLL();

	display();
}
