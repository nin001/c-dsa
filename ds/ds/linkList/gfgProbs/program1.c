// Problem statement 
// Easy : 
// print the middle of a given linked list
#include<stdio.h>
#include<stdlib.h>

typedef struct node{
	int data;
	struct node *next;
}node;

node *head = NULL;

node* createNode(){
	node *newNode = (node*)malloc(sizeof(node));
	printf("Enter data : ");
	scanf("%d",&newNode->data);

	newNode->next = NULL;
}

void addNode(){
	node *newNode = createNode();
	if(head == NULL)
		head = newNode;
	else{
		node *temp = head;
		while(temp->next != NULL)
			temp = temp->next;
		temp->next = newNode;
	}
}

void display(){
	if(head == NULL)
		printf("Linked list is Empty\n");
	else{
		node *temp = head;
		while(temp->next != NULL){
			printf("%d>",temp->data);
			temp = temp->next;
		}printf("%d\n",temp->data);

	}
}

int countNode(){
	if(head == NULL)
		return 0;
	else{
		if(head->next == NULL)
			return 1;
		else{
			int count = 0;
			node *temp = head;
			while(temp != NULL){
				count++;
				temp = temp->next;
			}
			return count;
		}
	}
}

int midVal(){
	if(head == NULL){
		return -1;
	}else{
		if(head->next == NULL)
			return head->data;
		else{
			int count = countNode()/2;
			node *temp = head;
			while(count){
				count--;
				temp = temp->next;
			}
			if(countNode()/2 == 0)
				return temp->next->data;
			else
				return temp->data;
		}
	}
	
}

void main(){
	int nodeCount;
	printf("Enter number of nodes : ");
	scanf("%d",&nodeCount);

	for(int i = 0 ; i<nodeCount ; i++)
		addNode();

	printf("Linked list : ");
	display();
	int ret = midVal();
	printf("%d\n",ret);
	

}
