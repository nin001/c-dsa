// problem statement 
// check if linked list is circular or not
#include<stdio.h>
#include<stdlib.h>

typedef struct node{
	int data;
	struct node *next;
}node;

node *head = NULL;

node* createNode(){
	node *newNode = (node*)malloc(sizeof(node));

	printf("Enter data : ");
	scanf("%d",&newNode->data);

	newNode->next = NULL;
	
	return newNode;
}

int isCircular(){
	if(head == NULL)
		return 1;
	else{
		node *temp = head;
		while(temp->next != head && temp->next != NULL)
			temp = temp->next;
		return (temp->next == head);
	}
}

int display(){
	node *temp = head;
	while(temp->next != head){
		printf("%d>",temp->data);
		temp = temp->next;
	}printf("%d\n",temp->data);
}

void main(){
	head = createNode();
	head->next = createNode();
	head->next->next = createNode();
	head->next->next->next = head;   // NULL instead of head will create the linked list as non Circular
display();
	if(isCircular())
		printf("Yes\n");
	else
		printf("No\n");

}
