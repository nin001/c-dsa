// problem statement
// count nodes of a circular linked list;

#include<stdio.h>
#include<stdlib.h>

typedef struct node{
	int data;
	struct node *next;
}node;

node *head = NULL;

node* createNode(){
	node *newNode = (node*)malloc(sizeof(node));

	printf("Enter data : ");
	scanf("%d",&newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){
	node *newNode = createNode();
	if(head == NULL){
		head = newNode;
		newNode->next = head;
	}else{
		node *temp = head;
		while(temp->next != head)
			temp = temp->next;
		temp->next = newNode;
		newNode->next = head;
	}
}

int countNode(){
	if(head == NULL)
		return 0;
	else{
		if(head->next == head)
			return 1;
		else{
			int count = 0;
			node *temp = head;
			while(temp->next != head){
				count++;
				temp = temp->next;
			}count++;
			return count;
		}
	}
}

void main(){
	addNode();
	addNode();
	addNode();
	addNode();
	addNode();
	addNode();

	printf("Nodes in circular linkedlist is %d\n",countNode());

}
