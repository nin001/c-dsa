// reverse a doubly linkedlist 
#include<stdio.h>
#include<stdlib.h>

typedef struct node{
	struct node *prev;
	int data;
	struct node *next;
}node;

node *head = NULL;

node* createNode(){
	node *newNode = (node*)malloc(sizeof(node));

	newNode->prev = NULL;
	
	printf("Enter data : ");
	scanf("%d",&newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){
	node *newNode = createNode();
	if(head == NULL){
		head = newNode;
	}else{
		node *temp = head;
		while(temp->next != NULL)
			temp = temp->next;
		temp->next = newNode;
		newNode->prev = temp;
	}
}

void printLL(){
	if(head == NULL)
		printf("EMPTY\n");
	else{
		node *temp = head;
		while(temp != NULL){
			printf("%d ",temp->data);
			temp = temp->next;
		}
		printf("\n");
	}
}

int rev(){
	if(head == NULL)
		return -1;
	else{
		node *temp = NULL;
		while(head->next != NULL){
			head->prev = head->next;
			head->next = temp;
			head = head->prev;
			temp = head->prev;
		}
		head->prev = head->next;
		head->next = temp;

		return 0;
	}
}
			


void main(){
	int nodeCount;
	printf("Enter number of nodes : ");
	scanf("%d",&nodeCount);

	for(int i = 0 ; i<nodeCount ; i++)
		addNode();

	printLL();

	int ret = rev();
	if(ret == 0){
		printf("Reversed linkedlist : ");
		printLL();
	}else
		printf("Invalid\n");
}
	

