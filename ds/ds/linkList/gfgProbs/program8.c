/*
Given a linked list and a number k. You have to reverse first part of linked list with k nodes and the second part with n-k nodes.
Input: 1 -> 2 -> 3 -> 4 -> 5
k = 2
Output: 2 -> 1 -> 5 -> 4 -> 3
Explanation: As k = 2 , so the first part 2
nodes: 1 -> 2 and the second part with 3 nodes:
3 -> 4 -> 5. Now after reversing the first part: 
2 -> 1 and the second part: 5 -> 4 -> 3.
So the output is: 2 -> 1 -> 5 -> 4 -> 3
*/

#include<stdio.h>

typedef struct node{
	struct node *prev;
	int data;
	struct node *next;
}

node *head = NULL;

void addNode(){
	node *newNode = (node*)malloc(sizeof(node));


	printf("Enter data : ");
	scanf("%d",&newNode->data);

	newNode->next = NULL;

	if(head == NULL){
		
		head->next = newNode;
	else{
		node *temp = head;
		while(temp->next != NULL)
			temp = temp->next;
		temp->next = newNode;
	}
}

int totalNode(node **head){
	if(*head == NULL){
		return 0;
	}else if(*head->next == NULL){
		return 1;
	}else{
		node *temp = *head;
		int count = 0;
		while(temp != NULL){
			count++;
			temp = temp->next;
		}

		return count;
	}
}

void reverse(node **head,int key){
	if(key<0 || key > totalNode(head) ){
		printf("invalid key\n");
	}else{
		if(key == 1){

}
void main(){
	int nodeCount;
	printf("Enter node count : ");
	scanf("%d",&nodeCount);

	for(int i = 0 ; i<nodeCount ; i++){
		addNode();
	}

	int key;
	printf("Enter key : ");
	scanf("%d",&key);

	reverse(head,key);

}
