// linkedList 
// Linear data structure which has nodes which are connected through self referenctial structures 


//Accessing Nodes and filling values 
//making access function to display the code
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct Employee{
	int empid;
	char eName[20];
	float sal;
	struct Employee *next;
}Emp;
Emp *head = NULL;
void access(){
	Emp *temp = head;
	while(temp!=NULL){
		printf("|%s ",temp->eName);
		printf("%d ",temp->empid);
		printf("%f|",temp->sal);
		temp = temp->next;
	}
}

void addNode(){
	Emp* newNode = (Emp*)malloc(sizeof(Emp));
	printf("Enter employee name :");
	fgets(newNode->eName,15,stdin);

	printf("Enter employee id : ");
	scanf("%d",&newNode->empid);

	printf("Enter employee salary : ");
	scanf("%f",&newNode->sal);
	getchar();

	if(head == NULL){
		head = newNode;
	}else{
		Emp *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
	}
}

void main(){
	int n;
	printf("How many nodes you want to insert : ");
	scanf("%d",&n);
	getchar();
	for(int i = 0 ; i<n ; i++){
		addNode();
	}
	printf("Nodes inserted are :\n");
	access();
}
