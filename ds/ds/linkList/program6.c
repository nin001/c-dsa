// Linked list
// Hard coded inserting and accessing values through head pointer
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct Movies{
	char mName[20];
	int count;
	float imdb;
	struct Movies *next;
}mov;

void main(){
	mov *head = NULL;
	mov *m1 = (mov*)malloc(sizeof(mov));
	mov *m2 = (mov*)malloc(sizeof(mov));
	mov *m3 = (mov*)malloc(sizeof(mov));
	
	//connecting list through head pointer
	head = m1;
	head->next = m2;
	head->next->next = m3;
	head->next->next->next = NULL;

	//inserting values
	
	strcpy(head->mName,"chup");
	head->count = 4;
	head->imdb = 9.8;

	strcpy(head->next->mName,"PRDP");
	head->next->count = 0;
	head->next->imdb = 2.4;

	strcpy(head->next->next->mName,"ABCD");
	head->next->next->count = 4;
	head->next->next->imdb = 7.8;

	//accessing values
	printf("Movie name : %s\n",head->mName);	
	printf("Count : %d\n",head->count);
	printf("IMDB rating : %f\n\n",head->imdb);
	
	printf("Movie name : %s\n",head->next->mName);	
	printf("Count : %d\n",head->next->count);
	printf("IMDB rating : %f\n\n",head->next->imdb);
	
	printf("Movie name : %s\n",head->next->next->mName);	
	printf("Count : %d\n",head->next->next->count);
	printf("IMDB rating : %f\n\n",head->next->next->imdb);
} 
