//problem statement
// make 2 linked list using one function for both (pass head as a parameter)
#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
}node;

node *head1 = NULL;
node *head2 = NULL;

node* createNode(){
	node *newNode = (node*)malloc(sizeof(node));
	printf("Enter data : ");
	scanf("%d",&newNode->data);

	newNode->next = NULL;
	return newNode;
}

void addNode(node **head){
	node *newNode = createNode();
	if(*head == NULL){
		*head = newNode;
	}else{
		node *temp = *head;
		while(temp->next != NULL)
			temp = temp->next;
		temp->next = newNode;
	}

}

int display(node *head){

	if(head == NULL){
		printf("LINKED LIST IS EMPTY\n");
		return -1;
	}else{
		node *temp = head;
		while(temp->next != NULL){
			printf("|%d|->",temp->data);
			temp = temp->next;
		}printf("|%d|\n",temp->data);

		return 0;
	}
}


void main(){
	int countNode;
	printf("Enter number of nodes to enter : Linked list 1\n");
	scanf("%d",&countNode);

	for(int i = 0 ; i<countNode ; i++)
		addNode(&head1);

	printf("Enter number of nodes to enter : Linked list 2\n");
	scanf("%d",&countNode);

	for(int i = 0 ; i<countNode ; i++)
		addNode(&head2);

	printf("LINKED LIST 1\n");
	display(head1);
	printf("LINKED LIST 2\n");
	display(head2);

}
