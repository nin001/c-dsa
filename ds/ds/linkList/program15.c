// problem stat
// make two linked list and concatinate first to another
//Approch 2

#include<stdio.h>
#include<stdlib.h>

typedef struct node{
	int data;
	struct node *next;
}node;

node *head1 = NULL;
node *head2 = NULL;

node* createNode(){
	node *newNode = (node*)malloc(sizeof(node));
	printf("Enter data : ");
	scanf("%d",&newNode->data);
	newNode->next = NULL;

	return newNode;
}

node* addNode(node *head){
	node *newNode = createNode();
	if(head == NULL)
		head = newNode;
	else{
		node *temp = head;
		while(temp->next != NULL)
			temp = temp->next;
		temp->next = newNode;
	}
	return head;
}

void concat(){
	if(head2 == NULL || head1 == NULL){
		printf("INVALID CALL\n");
	}else{
		node *temp = head1;
		while(temp->next != NULL)
			temp = temp->next;
		temp->next = head2;
	}
}
	
int display(node *head){
	if(head == NULL){
		printf("LINKED LIST IS EMPTY\n");
		return -1;
	}else{
		node *temp = head;
		while(temp->next != NULL){
			printf("|%d|->",temp->data);
			temp = temp->next;
		}printf("|%d|\n",temp->data);

		return 0;
	}
}


void main(){
	int nodeCount;
	printf("Number of nodes in linked list 1 :\n");
	scanf("%d",&nodeCount);

	for(int i = 0 ; i<nodeCount ; i++)
		head1 = addNode(head1);
	display(head1);

	printf("Number of nodes in linked list 2 :\n");
	scanf("%d",&nodeCount);

	for(int i = 0 ; i<nodeCount ; i++)
		head2 = addNode(head2);

	display(head2);
	
	concat();

	display(head1);
}
