// problem stat
// add firstn nodes in linked list


#include<stdio.h>
#include<stdlib.h>

typedef struct node{
	int data;
	struct node *next;
}node;

node *head1 = NULL;
node *head2 = NULL;

node* createNode(){
	node *newNode = (node*)malloc(sizeof(node));
	printf("Enter data : ");
	scanf("%d",&newNode->data);
	newNode->next = NULL;

	return newNode;
}

node* addNode(node *head){
	node *newNode = createNode();
	if(head == NULL)
		head = newNode;
	else{
		node *temp = head;
		while(temp->next != NULL)
			temp = temp->next;
		temp->next = newNode;
	}
	return head;
}

int countNode(node *head){
	if(head == NULL)
		return 0;
	else if(head->next == NULL)
		return 1;
	else{
		node *temp = head;
		int count = 0;
		while(temp != NULL){
			count++;
			temp = temp->next;
		}

		return count;
	}
}

void concatFirst(int num){
	int count = countNode(head2);
	if(num>count || num<=0){
		printf("Invalid number\n");
	}else{
		node *temp1 = head1;
		while(temp1->next != NULL)
			temp1 = temp1->next;
		
		temp1->next = head2;
		node *temp2 = head2;
		while(num != 1){
			num--;
			temp2 = temp2->next;
		}

		temp2->next = NULL;

	}
}
	
int display(node *head){
	if(head == NULL){
		printf("LINKED LIST IS EMPTY\n");
		return -1;
	}else{
		node *temp = head;
		while(temp->next != NULL){
			printf("|%d|->",temp->data);
			temp = temp->next;
		}printf("|%d|\n",temp->data);

		return 0;
	}
}


void main(){
	int nodeCount;
	printf("Number of nodes in linked list 1 :\n");
	scanf("%d",&nodeCount);

	for(int i = 0 ; i<nodeCount ; i++)
		head1 = addNode(head1);
	display(head1);

	printf("Number of nodes in linked list 2 :\n");
	scanf("%d",&nodeCount);

	for(int i = 0 ; i<nodeCount ; i++)
		head2 = addNode(head2);

	int num;
	printf("Enter number of nodes to concat : ");
	scanf("%d",&num);

	concatFirst(num);
	display(head1);
	
}
