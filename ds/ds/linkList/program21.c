// Linked list practice
// Singly linkedlist

#include<stdio.h>
#include<stdlib.h>

typedef struct node{
	int data;
	struct node *next;
}node;

node *head = NULL;

node *createNode(){
	node * newNode = (node*)malloc(sizeof(node));

	printf("Enter data : ");
	scanf("%d",&newNode->data);
	newNode->next = NULL;
	return newNode;
}

void addNode(){
	node *newNode = createNode();
	if(head == NULL)
		head = newNode;
	else{
		node *temp = head;
		while(temp->next != NULL)
			temp = temp->next;
		temp->next = newNode;
	}
}

void addLast(){
	addNode();
}

void addFirst(){
	node *newNode = createNode();
	if(head == NULL)
		head = newNode;
	else{
		node *temp = head;
		head = newNode;
		newNode->next = temp;
	}
}

int countNode(){
	if(head == NULL)
		return 0;
	else{
		int count = 0;
		node *temp = head;
		while(temp != NULL){
			count++;
			temp = temp->next;
		}
		return count;
	}
}

void addAtPos(int pos){
	if(pos <= 0 || pos > (countNode()+1))
		printf("Invalid Position\n");
	else{
		if(pos == 1)
			addFirst();
		else if(pos == countNode()+1)
			addNode();
		else{
			node *newNode = createNode();
			node *temp = head;
			while(pos-2){
				temp = temp->next;
				pos--;
			}

			newNode->next = temp->next;
			temp->next = newNode;
		}
	}
}

void deleteNode(){
	if(head == NULL)
		printf("Linkedlist is already empty\n");
	else{
		if(head->next == NULL){
			free(head);
			head = NULL;
		}else{
			node *temp = head;
			while(temp->next->next != NULL)
				temp = temp->next;
			free(temp->next);
			temp->next = NULL;
		}
	}
}

void deleteLast(){
	deleteNode();
}

void deleteFist(){
	if(head == NULL){
		printf("Linkedlist is already empty\n");
	}else{
		if(head->next == NULL){
			free(head);
			head = NULL;
		}else{
			node *temp = head->next;
			free(head);
			head = temp->next;
		}
	}
}

void deleteAtPos(int pos){
	if(pos < 0 || pos > countNode())
		printf("Invalid position\n");
	else{
		if(pos == 1)
			deleteFist();
		else if(pos == countNode())
			deleteNode();
		else{
			node *temp = head;
			while(pos-2){
				temp = temp->next;
				pos--;
			}
			node *ptr = temp->next;
			temp->next = temp->next->next;
			free(ptr);
		}
	}
}

void display(){
	if(head == NULL)
		printf("EMPTY\n");
	else{
		node *temp = head;
		while(temp->next != NULL){
			printf("|%d|->",temp->data);
			temp = temp->next;
		}printf("|%d|\n",temp->data);
	}
}

void main(){
	char ch;
	do{
		printf("Enter choice :\n");
		printf("1.AddNode\n");
		printf("2.AddLast\n");
		printf("3.AddAtPos\n");
		printf("4.DeleteNode\n");
		printf("5.DeleteLast\n");
		printf("6.DeleteAtPos\n");
		printf("7.Display\n");

		int choice;
		printf("Enter choice : ");
		scanf("%d",&choice);

		switch(choice){
			case 1:
				addNode();
				break;
			case 2:
				addLast();
				break;
			case 3:
				{
				int pos;
				printf("Enter position : ");
				scanf("%d",&pos);
				addAtPos(pos);
				}
				break;
			case 4:
				deleteNode();
				break;
			case 5:
				deleteLast();
				break;
			case 6:
				{
				int pos;
				printf("Enter position : ");
				scanf("%d",&pos);
				deleteAtPos(pos);
				}
				break;
			case 7:
				display();
				break;
			default:
				printf("Invalid choice\n");
		}

		getchar();
		printf("Do u want to continue : ");
		scanf("%c",&ch);
	}while(ch == 'Y' || ch == 'y');

}

