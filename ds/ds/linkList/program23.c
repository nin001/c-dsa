// Linked list practice
// Doubly linked list

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	struct Node *prev;
	int data;
	struct Node *next;
}node;

node *head = NULL;

node* createNode(){
	node *newNode = (node*)malloc(sizeof(node));

	newNode->prev = NULL;
	
	printf("Enter data : ");
	scanf("%d",&newNode->data);

	newNode->next = NULL;

	return newNode;
}

void AddNode(){
	node *newNode = createNode();
	if(head == NULL){
		head = newNode;
	}else{
		node *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		newNode->prev = temp;
		temp->next = newNode;
	}
}

void AddFirst(){
	node *newNode = createNode();
	if(head == NULL)
		head = newNode;
	else{
		newNode->next = head;
		head->prev = newNode;
		head = newNode;
	}
}

void AddLast(){
	AddNode();
}

int countNode(){
	if(head == NULL)
		return 0;
	else{
		int count = 0;
		node *temp = head;
		while(temp!=NULL){
			temp = temp->next;
			count++;
		}return count;
	}
}

void AddAtPos(int pos){
	int count = countNode();
	if(pos<=0 || pos>count+1)
		printf("Invalid position\n");
	else{
		if(pos == 1)
			AddFirst();
		else if(pos == count+1)
			AddLast();
		else{
			node *newNode = createNode();
			node *temp = head;
			while(pos-2){
				temp = temp->next;
				pos--;
			}
			newNode->next = temp->next;
			temp->next->prev = newNode;
			temp->next = newNode;
			newNode->prev = temp;
		}
	}
}

void DelNode(){
	if(head == NULL){
		printf("Invalid Operation EMPTY LL\n");
	}else{
		if(head->next == NULL){
			free(head);
			head = NULL;
		}else{
			node *temp = head;
			while(temp->next->next != NULL){
				temp = temp->next;
			}

			free(temp->next);
			temp->next = NULL;
		}
	}
}

void DelFirst(){
	if(head == NULL){
		printf("Invalid Operation EMPTY LL\n");
	}else{
		if(head->next == NULL){
			free(head);
			head = NULL;
		}else{
			head = head->next;
			free(head->prev);
			head->prev = NULL;
		}
	}
}

void DelLast(){
	DelNode();
}

void DelAtPos(int pos){
	int count = countNode();
	if(pos<=0 || pos>count)
		printf("Invalid position\n");
	else{
		if(pos == 1)
			DelFirst();
		else if(pos == count)
			DelLast();
		else{
			node *temp = head;
			while(pos-2){
				temp = temp->next;
				pos--;
			}
			temp->next->prev = temp;
			node *temp1 = temp->next;
			temp->next = temp->next->next;
			free(temp1);
		}
	}
}

void printLL(){
	if(head == NULL){
		printf("EMPTY LL\n");
	}else{
		node *temp = head;
		while(temp->next != NULL){
			printf("|%d|-",temp->data);
			temp = temp->next;
		}
		printf("|%d|\n",temp->data);
	}
}

void main(){
	char choice;
	do{
		printf("1.AddNode\n");
		printf("2.AddLast\n");
		printf("3.AddFirst\n");
		printf("4.AddAtPos\n");
		printf("5.DelNode\n");
		printf("6.DelLast\n");
		printf("7.DelFirst\n");
		printf("8.DelAtPos\n");
		printf("9.DisplayLL\n");
		printf("Enter choice : ");

		int ch;
		scanf("%d",&ch);

		switch(ch){
			case 1:
				AddNode();
				printLL();
				break;
			case 2:
				AddLast();
				printLL();
				break;
			case 3:
				AddFirst();
				printLL();
				break;

			case 4:{
				int pos;
				printf("Enter position : ");
				scanf("%d",&pos);
				AddAtPos(pos);
				printLL();
			       }
				break;
			case 5:
				DelNode();
				printLL();
				break;
			case 6:
				DelLast();
				printLL();
				break;
			case 7:
				DelFirst();
				printLL();
				break;
			case 8:{
				int pos;
				printf("Enter position : ");
				scanf("%d",&pos);
				DelAtPos(pos);
				printLL();
			       }
				break;
			case 9:
				printLL();
				break;
			default:
				printf("Invalid Choice");
		}

		printf("Do you want to contine (y/n) :");
		getchar();
		scanf("%c",&choice);

	}while(choice == 'Y' || choice == 'y');
}	

		
