//Linked list
//Singly linked list : 
//In this each node has only the address of its next node 
//it is very similar or pure example of self referential structure
#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
}node;

node *head = NULL;

//create node function
node* createNode(){
	node *newNode = (node*)malloc(sizeof(node));
	printf("Enter data : ");
	scanf("%d",&newNode->data);
	newNode->next = NULL;
	return newNode;
}

//addnode function
void addNode(){
	node *newNode = createNode();
	if(head == NULL)
		head = newNode;
	else{
		node *temp = head;
		while(temp->next != NULL)
			temp = temp->next;
		temp->next = newNode;
	}
}

//addFirst
void addFirst(){
	node *newNode = createNode();
	if(head == NULL)
		head = newNode;
	else{
		newNode->next = head;
		head = newNode;
	}
}

//addLast
void addLast(){
	addNode();
}

//add Node at position
void addPos(int pos){
	node *newNode = createNode();
	node *temp = head;
	while(pos-2){
		pos--;
		temp = temp->next;
	}
	newNode->next = temp->next;
	temp->next = newNode;
}

//Count total number of nodes 
int count(){
	node *temp = head;
	int count = 0;
	while(temp!=NULL){
		count++;
		temp = temp->next;
	}return count;
}

//display();
void display(){
	node *temp = head;
	while(temp!=NULL){
		printf("| %d |->",temp->data);
		temp = temp->next;
	}printf("NULL\n");
}

void main(){
	int n;
	printf("How many nodes you want to enter\n");
	scanf("%d",&n);
	for(int i = 0 ; i<n ; i++){
		addNode();
	}
	printf("Total number of nodes : %d\n",count());
	printf("Add node in first :\n");
	addFirst();
	display();
	int pos;
	printf("Which position you want to add Node : ");
	scanf("%d",&pos);
	addPos(pos);
	display();
}
