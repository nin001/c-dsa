// inplace reverse in Singly circular linkedlist
//
#include<stdio.h>
#include<stdlib.h>

typedef struct node{
	int data;
	struct node *next;
}node;

node *head = NULL;

node* createNode(){
	node *new = (node*)malloc(sizeof(node));
	printf("Enter data : ");
	scanf("%d",&new->data);

	new->next = NULL;

	return new;
}

void addNode(){
	node *new = createNode();
	if(head == NULL){
		head = new;
		new->next = head;
	}else{
		node *temp = head;
		while(temp->next != head)
			temp = temp->next;
		temp->next = new;
		new->next = head;
	}
}

void display(){
	if(head == NULL)
		printf("Singly circular linkedlist is Empty\n");
	else{
		node *temp = head;
		while(temp->next != head){
			printf("|%d|->",temp->data);
			temp = temp->next;
		}printf("|%d|\n",temp->data);

	}
}

int rev(){
	if(head == NULL)
		return -1;
	else{
		node *temphead = head;
		node *temp1 = NULL;
		node *temp2 = NULL;
		while(temp2 != head){
			temp2 = temphead->next;
			temphead->next = temp1;
			temp1 = temphead;
			temphead = temp2;
		}
		head->next = temp1;
		head = temp1;
	}
}
		
void main(){
	int countNode;
	printf("Enter number of nodes : ");
	scanf("%d",&countNode);

	for(int i = 0 ; i<countNode ; i++)
		addNode();

	display();
	rev();
	display();

}
