// Linked list practice
// Doubly circular linked list

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	struct Node *prev;
	int data;
	struct Node *next;
}node;

node *head = NULL;

node* createNode(){
	node *newNode = (node*)malloc(sizeof(node));
	newNode->prev = NULL;

	printf("Enter data : ");
	scanf("%d",&newNode->data);

	newNode->next = NULL;
	
	return newNode;
}

void AddNode(){
	node *newNode = createNode();
	if(head == NULL){
		head = newNode;
		newNode->prev = newNode;
		newNode->next = newNode;
	}else{
		head->prev->next = newNode;
		newNode->prev = head->prev;
		newNode->next = head;
		head->prev = newNode;
	}
}

void AddLast(){
	AddNode();
}

void AddFirst(){
	node *newNode = createNode();
	if(head == NULL){
		head = newNode;
		newNode->next = newNode;
		newNode->prev = newNode;
	}else{
		newNode->next = head;
		head->prev->next = newNode;
		newNode->prev = head->prev;
		head->prev = newNode;
		
		head = newNode;
	}
}

int countNode(){
	if(head == NULL)
		return 0;
	else{
		node *temp = head;
		int count = 0;
		while(temp->next != head){
			count++;
			temp = temp->next;
		}
		return count+1;
	}
}

void AddAtPos(int pos){
	int count = countNode();
	if(pos<=0 || pos>count+1)
		printf("Invalid Position\n");
	else{
		if(pos == 1)
			AddFirst();
		else if(pos == count+1)
			AddLast();
		else{
			node *newNode = createNode();
			node *temp = head;
			while(pos-2){
				temp = temp->next;
				pos--;
			}

			newNode->next = temp->next;
			temp->next->prev = newNode;
			newNode->prev = temp;
			temp->next = newNode;
		}
	}
}

void DelNode(){
	if(head == NULL)
		printf("Invalid Operation\n");
	else{
		if(head->next == head){
			free(head);
			head = NULL;
		}else{
			head->prev = head->prev->prev;
			free(head->prev->next);
			head->prev->next = head;
		}
	}
}

void DelLast(){
	DelNode();
}

void DelFirst(){
	if(head == NULL)
		printf("Invalid Operation\n");
	else{
		if(head->next == head){
			free(head);
			head = NULL;
		}else{
			node *temp = head;
			head->prev->next = head->next;
			head->next->prev = head->prev;
			head = head->next;
			free(temp);
		}
	}
}

void DelAtPos(int pos){
	int count = countNode();
	if(pos<=0 || pos>count)
		printf("Invalid Position\n");
	else{
		if(pos == 1)
			DelFirst();
		else if(pos == count)
			DelLast();
		else{
			node *temp = head;
			while(pos-2){
				temp = temp->next;
				pos--;
			}
			
			temp->next = temp->next->next;
			free(temp->next->prev);
			temp->next->prev = temp;
		}
	}
}

void printLL(){
	if(head == NULL)
		printf("EMPTY LL\n");
	else{
		node *temp = head;
		while(temp->next != head){
			printf("|%d|-",temp->data);
			temp = temp->next;
		}
		printf("|%d|\n",temp->data);
	}
}

	
void main(){
	char choice;
	do{
		printf("1.AddNode\n");
		printf("2.AddLast\n");
		printf("3.AddFirst\n");
		printf("4.AddAtPos\n");
		printf("5.DelNode\n");
		printf("6.DelLast\n");
		printf("7.DelFirst\n");
		printf("8.DelAtPos\n");
		printf("9.DisplayLL\n");
		printf("Enter choice : ");

		int ch;
		scanf("%d",&ch);

		switch(ch){
			case 1:
				AddNode();
				printLL();
				break;
			case 2:
				AddLast();
				printLL();
				break;
			case 3:
				AddFirst();
				printLL();
				break;

			case 4:{
				int pos;
				printf("Enter position : ");
				scanf("%d",&pos);
				AddAtPos(pos);
				printLL();
			       }
				break;
			case 5:
				DelNode();
				printLL();
				break;
			case 6:
				DelLast();
				printLL();
				break;
			case 7:
				DelFirst();
				printLL();
				break;
			case 8:{
				int pos;
				printf("Enter position : ");
				scanf("%d",&pos);
				DelAtPos(pos);
				printLL();
			       }
				break;
			case 9:
				printLL();
				break;
			default:
				printf("Invalid Choice");
		}

		printf("Do you want to contine (y/n) :");
		getchar();
		scanf("%c",&choice);

	}while(choice == 'Y' || choice == 'y');
}	

		
