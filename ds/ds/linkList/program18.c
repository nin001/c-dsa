// singly linked list :
// linked list reverse with help of swap(data);
#include<stdio.h>
#include<stdlib.h>

typedef struct node{
	int data;
	struct node *next;
}node;

node *head = NULL;

node* createNode(){
	node *newNode = (node*)malloc(sizeof(node));

	printf("Enter data : ");
	scanf("%d",&newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){
	node *newnode = createNode();
	if(head == NULL)
		head = newnode;
	else{
		node *temp = head;
		while(temp->next != NULL)
			temp = temp->next;
		temp->next = newnode;
	}
}

int countNode(){
	if(head == NULL)
		return 0;
	else{
		if(head->next == NULL)
			return 1;
		else{
			int count = 0;
			node *temp = head;
			while(temp != NULL){
				count++;
				temp = temp->next;
			}
			return count;
		}
	}
}

int swap(){
	if(head == NULL)
		return -1;
	else{
		if(head->next == NULL)
			return 0;
		else{
			node *temp1 = head;
			int count = countNode();
			printf("%d\n:",count);
			int data;
			while(count/2){
				node *temp2 = head;
				int itr = count;
				while(itr-1){
					itr--;
					temp2 = temp2->next;
				}

				data = temp2->data;
				temp2->data = temp1->data;
				temp1->data = data;

				temp1 = temp1->next;
				count--;
			}
		}
		return 1;
	}
}

void display(){
	if(head == NULL)
		printf("Linked list is Empty\n");
	else{
		node *temp = head;
		while(temp->next != NULL){
			printf("|%d|->",temp->data);
			temp = temp->next;
		}
		printf("|%d|\n",temp->data);
	}
}

void main(){
	int countNode;
	printf("Enter number of nodes : ");
	scanf("%d",&countNode);

	for(int i = 0 ; i<countNode ; i++)
		addNode();

	display();
	swap();
	printf("Reversed linked list\n");
	display();

}
