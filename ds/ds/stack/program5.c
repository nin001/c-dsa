// Implementing stack using linked list
// taking stack size from user and handling size error(stack overFlow)
#include<stdio.h>
#include<stdlib.h>

typedef struct Stack{
        int data;
        struct Stack *next;
}stack;

int size;
stack *head = NULL;
stack *top = NULL;
int flag = 0;
int sizeCounter = 0;

stack* createNode(){
        stack *newNode = (stack*)malloc(sizeof(stack));

        printf("Enter data : ");
        scanf("%d",&newNode->data);
        newNode->next = NULL;

        return newNode;
}

int countNode(){
	if(head == NULL)
		return 0;
	else{
		if(head->next == NULL)
			return 1;
		else{
			int count = 0;
			stack *temp = head;
			while(temp->next != NULL){
				temp = temp->next;
				count++;
			}return count;
		}
	}
}

int push(){
	if(sizeCounter == size){
		return -1;
	}else{
		stack *newNode = createNode();
		if(head == NULL){
			head = newNode;
			top = newNode;
		}else{
			stack *temp = head;
			while(temp->next != NULL)
				temp = temp->next;
			temp->next = newNode;
			top = temp;
		}
		sizeCounter++;
		return 0;
	}
}

int pop(){
	if(top == NULL){
		flag = 1;
		return -1;
	}else{
		int data;
		if(top->next == NULL){
			data = top->data;
			free(top);
			top = NULL;
			head = NULL;
			return data;
		}else{
			data = top->next->data;
			free(top->next);
			top->next = NULL;
			stack *temp = head;
			while(temp->next->next != NULL)
				temp = temp->next;
			top = temp;
			flag = 0;
			return data;
		}
	}
}

int peek(){
	if(top == NULL)
		flag = 1;
	else{
		if(top->next == NULL)
			return top->data;
		else
			return top->next->data;
	}
}

void main(){
	printf("Enter size of Stack : ");
	scanf("%d",&size);

	char ch;
        do{
                printf("1.Push\n");
                printf("2.Pop\n");
                printf("3.Peek\n");

                int choice;
                printf("Enter choice : ");
                scanf("%d",&choice);

                switch(choice){
                        case 1:
				{
                                int ret = push();
				if(ret != 0)
					printf("Stack Overflow..\n");
				}
				break;
                        case 2:
                                {
                                int ret = pop();
                                if(flag == 1)
                                        printf("Stack Underflow..\n");
                                else
                                        printf("%d popped\n",ret);
                                }
                                break;
                        case 3:
                                {
                                int ret = peek();
                                if(flag == 1)
                                        printf("Stack is Empty\n");
                                else
                                        printf("%d peeked present at top\n",ret);
                                }
                                break;

                        default:
                                printf("INVALID CHOICE\n");
                }

                getchar();
                printf("Do You Want To Continue(y/n) : ");
                scanf("%c",&ch);
        }while(ch == 'y' || ch == 'Y');
}
