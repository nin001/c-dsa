// implementing stack using linked list (with additional functions);

#include<stdio.h>
#include<stdlib.h>

typedef struct node{
	int data;
	struct node *next;
}node;

node *head = NULL;
int size = 0;
node *top = NULL;
int flag = 0;

node* createNode(){
	node *newNode = (node*)malloc(sizeof(node));

	printf("Enter data : ");
	scanf("%d",&newNode->data);

	newNode->next = NULL;

	return newNode;
}

int countNode(){
	if(head == NULL)
		return 0;
	else{
		if(head->next == NULL)
			return 1;
		else{
			int count = 0;
			node *temp = head;
			while(temp != NULL){
				count++;
				temp = temp->next;
			}
			return count;
		}
	}
}

int push(){
	if(countNode() == size)
		return -1;
	else{
		node *newNode = createNode();
		if(head == NULL){
			head = newNode;
			top = newNode;
		}else{
			node *temp = head;
			while(temp->next != NULL)
				temp = temp->next;
			temp->next = newNode;
			top = newNode;
		}
		return 0;
	}
}

int pop(){
	if(top == NULL){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		int data = top->data;
		if(head->next == NULL){
			free(top);
			top = NULL;
			head = NULL;
		}else{
			node *temp = head;
			while(temp->next->next != NULL)
				temp = temp->next;
			top = temp;
			free(top->next);
			top->next = NULL;
		}
		return data;
	}
}

int peek(){
	if(top == NULL){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		return top->data;
	}
}	
		

void main(){
        printf("Enter size of Array : ");
        scanf("%d",&size);

	char ch;
        do{
                printf("1.Push\n");
                printf("2.Pop\n");
                printf("3.Peek\n");

                int choice;
                printf("Enter choice : ");
                scanf("%d",&choice);

                switch(choice){
                        case 1:
                                {
                                int ret = push();
                                if(ret != 0)
                                        printf("Stack Overflow..\n");
                                }
                                break;
                        case 2:
                                {
                                int ret = pop();
                                if(flag == 0)
                                        printf("Stack Underflow..\n");
                                else
                                        printf("%d popped\n",ret);
                                }
                                break;
                        case 3:
                                {
                                int ret = peek();
                                if(flag == 0)
                                        printf("Stack is Empty\n");
                                else
                                        printf("%d peeked present at top\n",ret);
                                }
                                break;

                        default:
                                printf("INVALID CHOICE\n");
                }

                getchar();
                printf("Do You Want To Continue(y/n) : ");
                scanf("%c",&ch);
        }while(ch == 'y' || ch == 'Y');
}

