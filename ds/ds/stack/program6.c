// Implementing stack using doubly linked list

#include<stdio.h>
#include<stdlib.h>

typedef struct stack{
	struct stack *prev;
	int data;
	struct stack *next;
}stack;

int size;
stack *head = NULL;
stack *top = NULL;
int flag = 0;
int counter;

stack* createNode(){
	stack *newNode = (stack*)malloc(sizeof(stack));
	newNode->prev = NULL;
	
	printf("Enter data : ");
	scanf("%d",&newNode->data);

	newNode->next = NULL;
	return newNode;
}

int countNode(){
	if(head == NULL)
		return 0;
	else if(head->next == NULL)
		return 1;
	else{
		int count = 0;
		stack *temp = head;
		while(temp != NULL){
			count++;
			temp = temp->next;
		}
		return count;
	}
}

int push(){
	if(countNode() == size){
		flag = 0;
		return -1;
	}else{
		stack *newNode = createNode();
		flag = 1;
		if(head == NULL){
			head = newNode;
			top = newNode;
		}else{
			stack *temp = head;
			while(temp->next != NULL)
				temp = temp->next;
			temp->next = newNode;
			newNode->prev = temp;
			top = newNode;
		}
		return 0;
	}
}

int pop(){
	if(countNode() == 0){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		int data;
		if(top->next == NULL){
			data = top->data;
			free(top);
			top = NULL;
			head = NULL;
		}else{
			data = top->data;
			top = top->prev;
			free(top->next);
			top->next = NULL;
		}
		return data;
	}
}

int peek(){
	if(top == NULL){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		return top->data;
	}
}	
	

void main(){
	printf("Enter size of Stack : ");
        scanf("%d",&size);

        char ch;
        do{
                printf("1.Push\n");
                printf("2.Pop\n");
                printf("3.Peek\n");

                int choice;
                printf("Enter choice : ");
                scanf("%d",&choice);

                switch(choice){
                        case 1:
                                {
                                int ret = push();
                                if(ret != 0)
                                        printf("Stack Overflow..\n");
                                }
                                break;
                        case 2:
                                {
                                int ret = pop();
                                if(flag == 0)
                                        printf("Stack Underflow..\n");
                                else
                                        printf("%d popped\n",ret);
                                }
                                break;
                        case 3:
                                {
                                int ret = peek();
                                if(flag == 0)
                                        printf("Stack is Empty\n");
                                else
                                        printf("%d peeked present at top\n",ret);
                                }
                                break;
                        default:
                                printf("INVALID CHOICE\n");
                }

                getchar();
                printf("Do You Want To Continue(y/n) : ");
                scanf("%c",&ch);
        }while(ch == 'y' || ch == 'Y');
}

