// Stack
// stack is a data structure(way) which has some rules
// it is a LIFO based last in first out 
// That means insertion and deletion (push,pop) both should be from one place only 
// Example CD stack
// there is a pointer type which points the topmost element in stack which has its location 


// basic stack implementation with help of array
#include<stdio.h>

int *aptr = NULL;
int top = -1;

int push(int num){
	*(aptr+(++top)) = num;
	return num;
}


void main(){
	int size;
	printf("Enter array size : ");
	scanf("%d",&size);

	int arr[size];
	
	aptr = arr;

	int num;
	printf("Enter number to push : ");
	scanf("%d",&num);
	push(num);

	for(int i = 0 ; i<=top ; i++)
		printf("|%d|\n",arr[i]);

}
	
