// Implementing 2 stacks in one array

#include<stdio.h>

int size;
int top1 = -1;
int top2 = -1;
int flag = 0;

int push(int arr[]){
	if(flag == 0){
		if(top2-top1<1)
			return -1;
		else{
			
			printf("Enter data : ");
			scanf("%d",&arr[top1++]);
			return 0;
		}
	}else{
		if(top2-top1<1)
			return -1;
		else{
		
			printf("Enter data : ");
			scanf("%d",&arr[--top2]);
			return 0;
		}
	}
}

int pop(int arr[]){
	if(flag == 0){
		if(top1 == -1){
			flag = 0;
			return -1;
		}else{
			flag = 1;
			int val = arr[top1];
			if(top1 == top2){
				top1 = -1;
				top2 = -1;
			}
			top1--;

			return val;
		}
	}else{
		if(top2 == -1){
			flag = 0;
			return -1;
		}else{
			flag = 1;
			int val = arr[top2];
			if(top1 == top2){
				top1 = -1;
				top2 = -1;
			}
			top2++;

			return val;
		}
	}
}	
				
int peek(int arr[]){
	if(flag == 0){
		if(top1 == -1){
			flag = 0;
			return -1;
		}else{
			flag = 1;
			return arr[top1];
		}
	}else{
		if(top2 == size){
			flag = 0;
			return -1;
		}else{
			flag = 1;
			return arr[top2];
		}
	}
}


void main(){
	printf("Enter size of an array : ");
	scanf("%d",&size);

	int arr[size];

	top1 = 0;
	top2 = size;

	char ch;
	do{
		printf("Select one of the choices\n");
		printf("1.push1\n");
		printf("2.push2\n");
		printf("3.pop1\n");
		printf("4.pop2\n");
		printf("5.peek1\n");
		printf("6.peek2\n");

		int choice;
		printf("Enter Your Choice : ");
		scanf("%d",&choice);

		switch(choice){
			case 1 :{
				flag = 0;
				int ret = push(arr);
				if(ret == -1)
					printf("Stack overflow\n");
				}
				break;
			case 2 :{
				flag = 1;
				int ret = push(arr);
				if(ret == -1)
					printf("Stack overflow\n");
				}
				break;

			case 3 :{
				flag = 0;
				int ret = pop(arr);
				if(flag == 0)
					printf("Stack underflow\n");
				else
					printf("%d popped\n",ret);
				}
				break;
			case 4 :{
				flag = 1;
				int ret = pop(arr);
				if(flag == 0)
					printf("Stack underflow\n");
				else
					printf("%d popped\n",ret);
				}
				break;

			case 5 :{
				flag = 0;
				int ret = peek(arr);
				if(flag == 1)
					printf("%d peeked\n",ret);
				else
					printf("Stack is Empty\n");
				}
				break;
			case 6 :{
				flag = 1;
				int ret = peek(arr);
				if(flag == 1)
					printf("%d peeked\n",ret);
				else
					printf("Stack is Empty\n");
				}
				break;
				
			default : 
				printf("Invalid choice\n");
		}

		getchar();
		printf("Do you want to continue(y/n) : ");
		scanf("%c",&ch);
	}while(ch == 'y' || ch == 'Y');
	

}
