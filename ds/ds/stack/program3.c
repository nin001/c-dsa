// Stack implementation using array
// Resolving previous test cases (code failed when stack elements were inserted as -1 and also failed for last elements)
#include<stdio.h>

int size;
int *sptr = NULL;
int top = -1;
int flag = 0;

int push(int num){

	if(top == (size-1)){
		printf("Stack Overflow...\n");
		return -1;
	}else{
		*(sptr+(++top)) = num;
		return 0;
	}
}	

int pop(){
	if(top == -1){
		flag = 1;
		return -1;
	}else{
		flag = 0;
		return *(sptr+(top--));
	}
}

int peek(){
	if(top == -1){
		flag = 1;
		return -1;
	}else{
		return *(sptr+top);
	}
}

void main(){
	printf("Enter Stack size : ");
	scanf("%d",&size);

	int stack[size];
	sptr = stack;

	char ch;
	do{
		printf("1.Push\n");
		printf("2.Pop\n");
		printf("3.Peek\n");

		int choice;
		printf("Enter Choice : ");
		scanf("%d",&choice);

		switch(choice){
			case 1:
				{
				int num;
				printf("Enter number : ");
				scanf("%d",&num);
				push(num);
				//printf("%d\n",top);
				}

				break;
			case 2:
				{
				int ret = pop();
				if(flag == 1)
					printf("Stack Underflow..\n");
				else
					printf("%d popped\n",ret);
				}
				break;
			case 3:
				{
				int data = peek();
				if(flag == 1)
					printf("Stack is Empty\n");
				else
					printf("%d Peeked present at top\n",data);
				}
				break;
			default:
				printf("INVALID CHOICE\n");
		}

		getchar();
		printf("Do you want to continue (y/n) : ");
		scanf("%c",&ch);
	}while(ch == 'y' || ch == 'Y');
	printf("Dhanyawad\n");

}


