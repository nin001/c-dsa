// Stack implementation using array
// taking user defined values to perform push operation 

#include<stdio.h>

int size;
int *aptr = NULL;
int top = -1;

int push(int num){
	if(top<(size-1)){
		top++;
		*(aptr+(top)) = num;
		return num;
	}else{
		return -1;
	}
}

void main(){
	printf("Enter size of stack : ");
	scanf("%d",&size);
	
	int arr[size];
	aptr = arr;


	int p;
	int val;
	printf("Push function initiated :\n");
	printf("Enter How many push calls ; ");
	scanf("%d",&p);
	for(int i = 0 ; i<p ; i++){
		int num;
		printf("Enter number to Push : ");
		scanf("%d",&num);
		val = push(num);
	
		if(val == -1){
			printf("Stack Overflow error ... Aborting \n");
			break;
		}
	}

	for(int i = top ; i>=0 ; i--){
		printf("|%d|\n",arr[i]);
		
	}

}
	
