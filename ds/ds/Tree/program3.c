// Given inorder and post order traversal of a btree
// construct a btree using traversal

#include<stdio.h>
#include<stdlib.h>

struct TreeNode {
	int data;
	struct TreeNode *left;
	struct TreeNode *right;
};

struct TreeNode* createTreeUsingTraversal(int in[] , int post[] , int inStart , int inEnd , int postStart , int postEnd){
	if(inStart > inEnd){
		return NULL;
	}

	int rootData = post[postEnd];
	struct TreeNode *root = (struct TreeNode*)malloc(sizeof(struct TreeNode));

	int index;
	for(index = inStart ; index <= inEnd ; index++)
		if(in[index] == rootData)
			break;

	int lLen = index-inStart;

	printf("%d\n",rootData);
	root->left = createTreeUsingTraversal(in,post,inStart,index-1,postStart,postStart+lLen-1);
	root->right = createTreeUsingTraversal(in,post,index+1,inEnd,postStart+lLen,postEnd-1);

	return root;

}

void postOrder(struct TreeNode *root) {
	if(root == NULL)
		return;

	postOrder(root->left);
	postOrder(root->right);
	printf("%d ",root->data);
}

void inOrder(struct TreeNode *root) {
	if(root == NULL)
		return;

	postOrder(root->left);
	printf("%d ",root->data);
	postOrder(root->right);
}

void main() {
	int post[] = {5,4,2,6,8,7,3,1};
	int in[] = {2,5,4,1,6,3,8,7};

	int inStart = 0  ,inEnd = 6;
	int postStart = 0 , postEnd = 6;
	struct TreeNode *root = createTreeUsingTraversal(in,post,inStart,inEnd,postStart,postEnd);
	
	postOrder(root);
	printf("\n");
	inOrder(root);
	printf("\n");

}
