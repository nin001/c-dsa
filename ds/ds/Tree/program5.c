// Total number of nodes in tree

#include <stdio.h>
#include <stdlib.h>

struct TreeNode{
        int data;
        struct node *left;
        struct node *right;
};

struct TreeNode* insert(int level){
        struct TreeNode *newNode = (struct TreeNode*)malloc(sizeof(struct TreeNode));
        printf("Enter data : ");
        scanf("%d",&newNode->data);

        char ch;
        level++;
        getchar();
        printf("Do you want to add left child at level %d\n",level);
        scanf("%c",&ch);

        if(ch == 'y' || ch == 'Y'){
                newNode->left = insert(level);
        }else{
                newNode->left = NULL;
        }

        getchar();
        printf("Do you want to add right child at level %d\n",level);
        scanf("%c",&ch);

        if(ch == 'y' || ch == 'Y'){
                newNode->right = insert(level);
        }else{
                newNode->right = NULL;
        }

        return newNode;
}

void preOrder(struct TreeNode *root){
        if(root == NULL)
                return;

        printf("%d ",root->data);
        preOrder(root->left);
        preOrder(root->right);
}

void inOrder(struct TreeNode *root){
        if(root == NULL)
                return;
        inOrder(root->left);
        printf("%d",root->data);
        inOrder(root->right);
}

void postOrder(struct TreeNode *root){
        if(root == NULL)
                return;
        postOrder(root->left);
        postOrder(root->right);
        printf("%d",root->data);
}

// Total number of nodes in tree 

int totalNodeBT(struct TreeNode *root) {
	if(root == NULL)
		return 0;

	int leftSubtree = totalNodeBT(root->left);
	int rightSubtree = totalNodeBT(root->right);

	return leftSubtree + rightSubtree + 1;
}

void main(){
        struct TreeNode *root = (struct TreeNode*)malloc(sizeof(struct TreeNode));

        printf("Enter Binary Tree root node : ");
        scanf("%d",&root->data);

        printf("Binary Tree is rooted with %d\n",root->data);

        char ch;
        printf("Do you want to insert left node (y/n) :");
        getchar();
        scanf("%c",&ch);

        if(ch == 'y' || ch == 'Y'){
                root->left = insert(0);
        }else{
                root->left = NULL;
        }

        printf("Do you want to insert right node (y/n) :");
        getchar();
        scanf("%c",&ch);

        if(ch == 'y' || ch == 'Y'){
                root->right = insert(0);
        }else{
                root->right = NULL;
        }
        char choice;
        do{
                printf("1.In-order B - Tree\n");
                printf("2.Pre-order B - Tree\n");
                printf("3.Post-order B - Tree\n");

                int ch;
                printf("Enter choice : ");
                scanf("%d",&ch);

                switch(ch){
                        case 1:
                                inOrder(root);
                                break;
                        case 2:
                                preOrder(root);
                                break;
                        case 3:
                                postOrder(root);
                                break;
                        default:
                                printf("Invalid choice\n");
                }

                printf("Continue ?\n");
                getchar();
                scanf("%c",&choice);
        }while(choice == 'Y' || choice == 'y');

	printf("Total number of nodes in tree : %d\n",totalNodeBT(root));
}

