// Binary tree code

#include<stdio.h>
#include<stdlib.h>

typedef struct node{
	int data;
	struct node *left;
	struct node *right;
}Tree;

Tree* insert(int level){
	Tree *newNode = (Tree*)malloc(sizeof(Tree));
	printf("Enter data : ");
	scanf("%d",&newNode->data);

	char ch;
	level++;
	getchar();
	printf("Do you want to add left child at level %d\n",level);
	scanf("%c",&ch);

	if(ch == 'y' || ch == 'Y'){
		newNode->left = insert(level);
	}else{
		newNode->left = NULL;
	}
	
	getchar();
	printf("Do you want to add right child at level %d\n",level);
	scanf("%c",&ch);

	if(ch == 'y' || ch == 'Y'){
		newNode->right = insert(level);
	}else{
		newNode->right = NULL;
	}

	return newNode;
}

void preOrder(Tree *root){
	if(root == NULL)
		return;

	printf("%d ",root->data);
	preOrder(root->left);
	preOrder(root->right);
}

void inOrder(Tree *root){
	if(root == NULL)
		return;
	inOrder(root->left);
	printf("%d",root->data);
	inOrder(root->right);
}

void postOrder(Tree *root){
	if(root == NULL)
		return;
	postOrder(root->left);
	postOrder(root->right);
	printf("%d",root->data);
}

void main(){
	Tree *root = (Tree*)malloc(sizeof(Tree));

	printf("Enter Binary Tree root node : ");
	scanf("%d",&root->data);

	printf("Binary Tree is rooted with %d\n",root->data);

	char ch;
	printf("Do you want to insert left node (y/n) :");
	getchar();
	scanf("%c",&ch);

	if(ch == 'y' || ch == 'Y'){
		root->left = insert(0);
	}else{
		root->left = NULL;
	}

	printf("Do you want to insert right node (y/n) :");
	getchar();
	scanf("%c",&ch);

	if(ch == 'y' || ch == 'Y'){
		root->right = insert(0);
	}else{
		root->right = NULL;
	}
	char choice;
	do{
		printf("1.In-order B - Tree\n");
		printf("2.Pre-order B - Tree\n");
		printf("3.Post-order B - Tree\n");

		int ch;
		printf("Enter choice : ");
		scanf("%d",&ch);

		switch(ch){
			case 1:
				inOrder(root);
				break;
			case 2:
				preOrder(root);
				break;
			case 3:
				postOrder(root);
				break;
			default:
				printf("Invalid choice\n");
		}

		printf("Continue ?\n");
		getchar();
		scanf("%c",&choice);
	}while(choice == 'Y' || choice == 'y');
}
