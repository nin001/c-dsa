// given inorder traversal and preorder traversal of a btree 
// construct tree using these two

#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

struct TreeNode {
	int data;
	struct TreeNode *left;
	struct TreeNode *right;
};

//Author = Niraj Rajendra Randhir
//Description - Function takes input a inorder traversal and preorder traversal 
//array and start and end index , Function works on recursion (multiple) , using inorder
//and preorder traversal it finds the root(subtree/tree) and returns in its left and right
//as per the array
//
//in - inOrder[] , preOrder[] , start(index) , end(index)
//out - struct TreeNode *(pointer to a node);
//
//data - 11/5/2023


int preIndex = 0;

struct TreeNode* createTreeUsingTraversal(int in[] , int pre[] , int start , int end ){
	if(start<=end){
		struct TreeNode *newnode = (struct TreeNode*)malloc(sizeof(struct TreeNode));

		newnode->data = pre[preIndex];

		int index = -1;
		for(int i = start ; i<=end ; i++){
			if(in[i] == pre[preIndex]){
				index = i;
				break;
			}
		}

		preIndex++;

		newnode->left = createTreeUsingTraversal(in,pre,start,index-1);
		newnode->right = createTreeUsingTraversal(in,pre,index+1,end);
		
		return newnode;
	}else{
		return NULL;
	}
}

void inOrder(struct TreeNode *root) {
	if(root == NULL)
		return;

	inOrder(root->left);
	printf("%d ",root->data);
	inOrder(root->right);
}

void preOrder(struct TreeNode *root) {
	if(root == NULL) {
		return;
	}

	printf("%d ",root->data);
	preOrder(root->left);
	preOrder(root->right);
}

void main(){
	printf("Welcome to binary tree\n");

	int in[] = {2,5,4,1,6,3,8,7};
	int pre[] = {1,2,4,5,3,6,7,8};


	int start = 0;
	int end = sizeof(in)/sizeof(in[0]);

	struct TreeNode *root = createTreeUsingTraversal(in,pre,start,end-1);
	inOrder(root);
	printf("\n");
	preOrder(root);
	printf("\n");
}
