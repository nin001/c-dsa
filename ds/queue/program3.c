// Implementing queue in stack 
#include<stdio.h>

int size;
int top = -1;
int front = -1;
int rear = -1;
int flag = 0;

int mkQue(int arr[],int que[]){
	int temptop = top;
//	printf("%d\n",temptop);
	int i = 0;
	while(temptop != -1){
		que[i] = arr[temptop];
		i++;
		temptop--;
	}
	front = top;
	rear = 0;
}

int push(int arr[],int que[]){
	if(top == size-1)
		return -1;
	else{
		printf("Enter data : ");
		scanf("%d",&arr[++top]);
		mkQue(arr,que);
		return 0;
	}
}

int pop(int arr[]){
	if(front == -1){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		int val = arr[front];
		if(front == rear){
			front = -1;
			rear = -1;
		}else{
			front--;
		}
		return val;
	}
}

int frontt(int arr[]){
	if(front == -1){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		return arr[front];
	}
}

void printQ(int arr[]){
	if(front == -1)
		printf("Que is empty\n");
	else{
	//	printf("%d\n",front);
		for(int i = front ; i>=0 ; i--)
			printf("%d ",arr[i]);
		printf("\n");
	}
}
		
void main(){
        printf("Enter size of Queue : ");
        scanf("%d",&size);

	int arr[size];
        int queue[size];

        char ch;
        do{
                printf("Choose an option\n");
                printf("1.Enqueue\n");
                printf("2.Dequeue\n");
                printf("3.Front\n");
                printf("4.PrintQ\n");

                int choice;
                printf("Enter your choice : ");
                scanf("%d",&choice);

                switch(choice){
                        case 1 :{
                                int ret = push(arr,queue);
                                if(ret == -1)
                                        printf("Queue overflow\n");
                                }
                                break;
                        case 2 :{
                                int ret = pop(queue);
                                if(flag == 0)
                                        printf("Queue underflow\n");
                                else
                                        printf("%d dequeued\n",ret);
                                }
                                break;
                        case 3 :{
                                int ft = frontt(queue);
                                if(flag == 0)
                                        printf("Queue is Empty\n");
                                else
                                        printf("%d at front\n",ft);
                                }
                                break;

                        case 4 :
                                printQ(queue);
                                break;

                        default :
                                printf("Invalid choice\n");
                }
                getchar();
                printf("Do you want to continue(y/n) : ");
                scanf("%c",&ch);
        }while(ch == 'y' || ch == 'Y');
        printf("Dhanyawad\n");
}

