// practice 
// Queue
// Operations 
// Enqueue - Addition of element in queue
// Dequeue - Removal of element from queue
// printQ - elements in queue print

#include<stdio.h>
int size;
int front = -1;
int rear = -1;
int flag = 0;

int EnQ(int Q[]){
	if(rear == size-1)
		return -1;
	else{
		if(front == -1)
			front++;
		printf("Enter data : ");
		scanf("%d",(Q+(++rear)));
	}
}

int DQ(int Q[]){
	if(front == -1){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		int val = *(Q+front);
		if(front == rear){
			front = -1;
			rear = -1;
		}else{
			front++;
		}
		return val;
	}
}

int Front(int Q[]){
	if(front == -1){
		flag = 0;
		return -1;
	}else{
		return *(Q+front);
	}
}

void PrintQ(int Q[]){
	if(front == -1 && rear == -1){
		printf("Queue is Empty\n");
	}else{
		for(int i = front ; i<=rear ; i++)
			printf("|%d|",*(Q+i));
		printf("\n");
	}
}

void main(){
	printf("Enter size of Queue : ");
	scanf("%d",&size);

	int Que[size];

	char choice;
	do{
		printf("Queue menu\n");
		printf("1.Enque\n");
		printf("2.Deque\n");
		printf("3.Front\n");
		printf("4.PrintQ\n");

		int ch;
		printf("Enter Choice : ");
		scanf("%d",&ch);

		switch(ch){
			case 1:{
				int ret = EnQ(Que);
				if(ret == -1)
					printf("Queue OverFlow\n");
				else{
					printf("Enqueued successfully\n");
					PrintQ(Que);
				}
			       }
				break;
			case 2:{
				int ret = DQ(Que);
				if(flag == 0)
					printf("Queue Underflow\n");
				else
					printf("Dequeued : %d\n",ret);
				}
				break;
			case 3:{
				int ret = Front(Que);
				if(flag == 0){
					printf("Queue is Empty\n");
				}else{
					printf("Front : %d\n",ret);
				}
			       }
				break;
			case 4:
				PrintQ(Que);
				break;
			default:
				printf("Ivalid choice\n");
		}

		getchar();
		printf("Do You Want to Continue(y/n)\n");
		scanf("%c",&choice);
	}while(choice == 'Y' || choice == 'y');
}
