// Circular queue is a queue where while implementing in array after dequeue operation no blocks are left useless 
// after rear reaching last element (size-1) and front is at position 3 
// If we perform enqueue rear will jump to 0 index as it is in a circular format

// implementing circular queue in array
#include<stdio.h>

int size;
int front = -1;
int rear = -1;
int flag = 0;

int enqueue(int que[]){
	if((rear == size-1 && front == 0) || (rear == front-1))
		return -1;
	else{
		if(front == -1)
			front++;
		if(rear == size-1)
			rear = -1;
		printf("Enter data : ");
		scanf("%d",&que[++rear]);

		return 0;
	}
}

int dequeue(int que[]){
	if(front == -1){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		int val = que[front];
		if(rear == front){
			front = -1;
			rear = -1;
		}else{
			if(front == size-1)
				front = -1;
			front++;
		}
		return val;
	}
}	

int frontt(int que[]){
	if(front == -1){
		flag = 0;
		return -1;
	}else{
		return que[front];
	}
}

void printQ(int que[]){
	if(front == -1){
		printf("Queue is Empty\n");
	}else{
		if(front <= rear){
			for(int i = front ; i<= rear ; i++)
				printf("|%d| ",que[i]);
			printf("\n");
		}else{
			int itr = front;
			printf("%d\n",itr);
			while(itr != rear){
				if(itr == size-1){
					printf("|%d| ",que[itr]);
					itr = 0;
				}else{
					printf("|%d| ",que[itr]);
					itr++;
				}
			}
			printf("|%d| ",que[itr]);
			printf("\n");
		}
	}
}

void main(){
        printf("Enter size of Queue : ");
        scanf("%d",&size);

        int queue[size];

        char ch;
        do{
                printf("Choose an option\n");
                printf("1.Enqueue\n");
                printf("2.Dequeue\n");
                printf("3.Front\n");
                printf("4.PrintQ\n");

                int choice;
                printf("Enter your choice : ");
                scanf("%d",&choice);

                switch(choice){
                        case 1 :{
                                int ret = enqueue(queue);
                                if(ret == -1)
                                        printf("Queue overflow\n");
                                }
                                break;
                        case 2 :{
                                int ret = dequeue(queue);
                                if(flag == 0)
                                        printf("Queue underflow\n");
                                else
                                        printf("%d dequeued\n",ret);
                                }
                                break;
                        case 3 :{
                                int ft = frontt(queue);
                                if(flag == 0)
                                        printf("Queue is Empty\n");
                                else
                                        printf("%d at front\n",ft);
                                }
                                break;

                        case 4 :
                                printQ(queue);
                                break;

                        default :
                                printf("Invalid choice\n");
                }
                getchar();
                printf("Do you want to continue(y/n) : ");
                scanf("%c",&ch);
        }while(ch == 'y' || ch == 'Y');
        printf("Dhanyawad\n");
}

