// Implementing stack using linked list
// Using linked list without top 
// using basic functions 
#include<stdio.h>
#include<stdlib.h>

typedef struct Stack{
	int data;
	struct Stack *next;
}stack;

stack *head = NULL;
int flag = 0;

stack* createNode(){
	stack *newNode = (stack*)malloc(sizeof(stack));

	printf("Enter data : ");
	scanf("%d",&newNode->data);
	newNode->next = NULL;

	return newNode;
}

int push(){
	stack *newNode = createNode();
	if(head == NULL)
		head = newNode;
	else{
		stack *temp = head;
		while(temp->next != NULL)
			temp = temp->next;
		temp->next = newNode;
		
	}
	return 0;
}

int pop(){
	if(head == NULL){
		flag = 1;
		return -1;
	}else{
		int data;
		flag = 0;
		if(head->next == NULL){
			data = head->data;
			free(head);
			head = NULL;
			return data;
		}else{
			stack *temp = head;
			while(temp->next->next != NULL)
				temp = temp->next;
			data = temp->next->data;
			free(temp->next);
			temp->next = NULL;
			return data;
		}
		
	}
}

int peek(){
	if(head == NULL){
		flag = 1;
		return -1;
	}else{
		flag = 0;
		stack *temp = head;
		while(temp->next != NULL)
			temp = temp->next;
		return temp->data;
	}
}

void main(){
	
	char ch;
	do{
		printf("1.Push\n");
		printf("2.Pop\n");
		printf("3.Peek\n");

		int choice;
		printf("Enter choice : ");
		scanf("%d",&choice);

		switch(choice){
			case 1:
				push();	
				break;
			case 2:
				{
				int ret = pop();
				if(flag == 1)
					printf("Stack Underflow..\n");
				else
					printf("%d popped\n",ret);
				}
				break;
			case 3:
				{
				int ret = peek();
				if(flag == 1)
					printf("Stack is Empty\n");
				else
					printf("%d peeked present at top\n",ret);
				}
				break;

			default:
				printf("INVALID CHOICE\n");
		}

		getchar();
		printf("Do You Want To Continue(y/n) : ");
		scanf("%c",&ch);
	}while(ch == 'y' || ch == 'Y');
	printf("Dhanyawad\n");
}
