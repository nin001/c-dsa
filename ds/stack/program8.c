// Implementing stack using doubly circular linked list 
#include<stdio.h>
#include<stdlib.h>

typedef struct stack{
	struct stack *prev;
	int data;
	struct stack *next;
}stack;

stack *head = NULL;
stack *top = NULL;
int size;
int flag = 0;

stack* createNode(){
        stack *newNode = (stack*)malloc(sizeof(stack));
        newNode->prev = NULL;

        printf("Enter data : ");
        scanf("%d",&newNode->data);

        newNode->next = NULL;
        return newNode;
}

int countNode(){
        if(head == NULL)
                return 0;
        else if(head->next == NULL)
                return 1;
        else{
                int count = 0;
                stack *temp = head;
                while(temp->next != head){
			count++;
                        temp = temp->next;
		}count++;
                return count;
        }
}

int push(){
	if(countNode() == size){
		return -1;
	}else{
		stack *newNode = createNode();
		if(head == NULL){
			head = newNode;
			head->prev = head;
			head->next = head;
			top = head;
		}else{
			stack *temp = head;
			while(temp->next != head)
				temp = temp->next;
			temp->next = newNode;
			newNode->prev = temp;
			newNode->next = head;
			head->prev = newNode;
			top = newNode;
		}
		return 0;
	}
}

int pop(){
	if(countNode() == 0){
		flag = 0;
		return -1;
	}else{
		
		flag = 1;
		int data;
		if(top->next == head){
			data = top->data;
			free(top);
			top = NULL;
			head = NULL;
		}else{
			flag = 1;
			data = top->data;
			top->next->prev = top->prev;
			top->prev->next = head;
			free(top);
			top = head->prev;
		}
		return data;
	}
}

int peek(){
	if(top == NULL){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		return top->data;
	}
}

void main(){
        printf("Enter size of Stack : ");
        scanf("%d",&size);

        char ch;
        do{
                printf("1.Push\n");
                printf("2.Pop\n");
                printf("3.Peek\n");

                int choice;
                printf("Enter choice : ");
                scanf("%d",&choice);

                switch(choice){
                        case 1:
                                {
                                int ret = push();
                                if(ret != 0)
                                        printf("Stack Overflow..\n");
                                }
                                break;
                        case 2:
                                {
                                int ret = pop();
                                if(flag == 0)
                                        printf("Stack Underflow..\n");
                                else
                                        printf("%d popped\n",ret);
                                }
                                break;
                        case 3:
                                {
                                int ret = peek();
                                if(flag == 0)
                                        printf("Stack is Empty\n");
                                else
                                        printf("%d peeked present at top\n",ret);
                                }
                                break;
                        default:
                }

                getchar();
                printf("Do You Want To Continue(y/n) : ");
                scanf("%c",&ch);
        }while(ch == 'y' || ch == 'Y');
}

